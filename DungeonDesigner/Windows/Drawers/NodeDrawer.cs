﻿using DungeonDesigner.Src.Managers;
using DungeonDesigner.Src.Path.Render;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Windows.Drawers {
    class NodeDrawer : BasicDrawer {

        int minWidth = 10;
        int minHeight = 200;

        public NodeDrawer() : base(GlobalData.NODE_RENDERER) {

        }

        protected override void Initialize() {

            if(!Initialized) {
                base.Initialize();
            }
        }

        protected override void drawerUpdate() {
            _renderer.MainCamera.Position = GraphicPath.Rect.Center;

            minWidth = Math.Max((int)GraphicPath.Rect.Width, Parent.Parent.Width);
            minHeight = Math.Max((int)GraphicPath.Rect.Height, Parent.Parent.Height);

            //Width = minWidth;
            //Height = minHeight;
            /*if(Width < 800) {
                Width = 1500;
            }*/
            if(minWidth != Width) {
                Width = minWidth;
            }
            if(minHeight != Height) {
                Height = minHeight;
            }

        }
    }
}

