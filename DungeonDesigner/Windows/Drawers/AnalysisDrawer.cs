﻿using DungeonDesigner.Engine2D;
using DungeonDesigner.Engine2D.DebugSrc;
using DungeonDesigner.Engine2D.Logic;
using DungeonDesigner.Engine2D.Renderer;
using DungeonDesigner.Engine2D.UI;
using DungeonDesigner.Src.Controllers;
using DungeonDesigner.Src.Managers;
using DungeonDesigner.Src.Other;
using DungeonDesigner.Src.Rooms;
using Microsoft.Xna.Framework;
using MonoGame.Forms.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Windows.Drawers {
    class AnalysisDrawer : BasicDrawer {

        public AnalysisDrawer() : base(GlobalData.ANALYSIS_RENDERER) {

        }
        
        protected override void Initialize() {

            if(Initialized) return;
            base.Initialize();

            new CameraController(_renderName, _renderer.MainCamera, _input);

            new Grid(GlobalData.ANALYSIS_RENDERER, 1, 1);
            
        }

        protected override void drawerUpdate() {
         
        }

        /* protected override void Update(GameTime gameTime) {

             if(Project.isCreated && Focused) {
                 Debug.log(GraphicsDevice.Viewport.Width);

                 _input.update();
                 _objectUpdate.update();

                 _renderer.updateCamera();

                 _input.lateUpdate();


                 _objectUpdate.lateUpdate();
                 base.Update(gameTime);
             }

         }*/
    }
}
