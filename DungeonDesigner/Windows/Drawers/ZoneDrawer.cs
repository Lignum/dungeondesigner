﻿using DungeonDesigner.Engine2D;
using DungeonDesigner.Engine2D.DebugSrc;
using DungeonDesigner.Engine2D.Logic;
using DungeonDesigner.Engine2D.Managers;
using DungeonDesigner.Engine2D.Renderer;
using DungeonDesigner.Engine2D.UI;
using DungeonDesigner.Src.Controllers;
using DungeonDesigner.Src.Managers;
using DungeonDesigner.Src.Rooms;
using DungeonDesigner.Src.UI;
using DungeonDesigner.Windows;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Forms.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DungeonDesigner.Windows.Drawers {

    class ZoneDrawer : BasicDrawer {
        // Initialize(), Update() and Draw() 
        //GraphicsDeviceManager graphics;
        public ContentManager Content { get; set; }

        

        public ZoneDrawer() : base(GlobalData.ZONE_RENDERER) {
            
        }

        protected override void Initialize() {
            base.Initialize();

            Content = Editor.Content;           
            Content.RootDirectory = "Content";
            LoadContent();

#if DEBUG
            Project.load("C:/Users/Xenok/Desktop/Basura/debugMultyPath.dgd");
            MainWindow.getInstance().enableElements(true);
#endif
        }
        

        protected void LoadContent() {

            ResourceMng.loadResources(Content);

            //RenderSystem.Init(GraphicsDevice);
            //_renderer = RenderMng.addRenderer(GlobalData.ZONE_RENDERER, GraphicsDevice);
            //_input = new Input(GlobalData.ZONE_RENDERER);
            CursorItem.createInstance(_input);

            //_objectUpdate = ObjectUpdateMng.createObjectUpdate(GlobalData.ZONE_RENDERER);

            Controller.init();

            new CameraController(GlobalData.ZONE_RENDERER, _renderer.MainCamera, _input);
            DebugRenderer.Init(GlobalData.ZONE_RENDERER, GraphicsDevice);

            /*
            UIRenderer.Init(GraphicsDevice, new Point(1000,800));
            UIRenderer.onWindowResize();

            UISystem.Init(Content);
            */
        }

        protected override void drawerUpdate() {

            Controller.update();
        }

        /*protected override void Update(GameTime gameTime) {

            Time.update(gameTime);
            if(Project.isCreated && _focused) {

                Viewport port = GraphicsDevice.Viewport;
                port.Width = Width;
                GraphicsDevice.Viewport = port;
                Debug.log(GraphicsDevice.Viewport.Width);

                _input.update();
                _objectUpdate.update();
                Controller.update();
                UIEventSystem.update();

                _renderer.updateCamera();

                _input.lateUpdate();

                _objectUpdate.lateUpdate();

                base.Update(gameTime);
            }

        }*/
    }
}
