﻿using DungeonDesigner.Engine2D;
using DungeonDesigner.Engine2D.DebugSrc;
using DungeonDesigner.Engine2D.Logic;
using DungeonDesigner.Engine2D.Renderer;
using DungeonDesigner.Engine2D.UI;
using DungeonDesigner.Src.Controllers;
using DungeonDesigner.Src.Rooms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Forms.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Windows.Drawers {
    abstract class BasicDrawer : MonoGameControl {

        public bool Initialized { get; private set; } = false;

        protected string _renderName;

        protected RenderSystem _renderer;

        protected Input _input;
        protected ObjectUpdate _objectUpdate;

        protected bool _focused;
        protected bool _resized;

        Viewport _port;

        public BasicDrawer (string renderName) {
            _renderName = renderName;

            GotFocus += new System.EventHandler((object sender, EventArgs e) => setFocus(true));
            LostFocus += new System.EventHandler((object sender, EventArgs e) => setFocus(false));
        }

        protected override void Initialize() {
            base.Initialize();

            if(!Initialized) {
                Initialized = true;

                _renderer = RenderMng.addRenderer(_renderName, GraphicsDevice);
                _input = new Input(_renderName);

                _objectUpdate = ObjectUpdateMng.createObjectUpdate(_renderName);
            }
        }

        private void setFocus(bool focus) {
            
            _focused = focus;
            
            if(_focused) {
                _input.resetScrollWheel();
            }
        }

        public void OnResize() {
            //UIRenderer.onWindowResize(Size.Width, Size.Height);
            _resized = true;
            updateFrame();
        }


        protected override void Update(GameTime gameTime) {
            if(Project.isCreated && _focused) {

                Time.update(gameTime);
                updateFrame();

            }
            base.Update(gameTime);
        }

        private void updateFrame() {
            //Debug.log($"Update - {_input.ScrollWheelValue}")

            _port = GraphicsDevice.Viewport;
            _port.Width = Width;
            _port.Height = Height;
            GraphicsDevice.Viewport = _port;

            _input.update();
            _objectUpdate.update();

            drawerUpdate();

            _renderer.updateCamera();

            _input.lateUpdate();

            _objectUpdate.lateUpdate();
        }

        protected abstract void drawerUpdate();

        protected override void Draw() {
            if(!Project.isCreated) {
                GraphicsDevice.Clear(Color.Gray);
            } else {
                GraphicsDevice.Clear(Color.Black);

                if(_resized) {
                    _renderer.updateCamera();
                    _resized = false;
                }

                _renderer.renderAll();
#if DEBUG
                DebugRenderer.renderAll();
#endif
                //UIRenderer.render();
            }
            //base.Draw();
        }
    }
}
