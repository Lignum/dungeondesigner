﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DungeonDesigner.Src.Rooms;
using DungeonDesigner.Src.Path;
using DungeonDesigner.Src.Path.States;
using DungeonDesigner.Windows.Elements;
using DungeonDesigner.Src.Controllers;
using DungeonDesigner.Windows.Analysis;

namespace DungeonDesigner.Windows {
    public partial class AnalyzeTableControl : UserControl {

        public AnalyzeTableControl() {
            InitializeComponent();
            
            PathSolver.addOnCalculationEnd(createPathBtns);
        }

        private void button1_Click(object sender, EventArgs e) {

            //_analysisWindow.ShowDialog();
            
            /*
            Controller.setTool(Controller.Tool.None);
            Project.getInstance().getZone().solveZone();
            ToolsBtnCtr.onToolChange(null);
            */
        }


        void createPathBtns(LogicPath path) {

            clearPathBtns();

            for(int i = 0; i < path.EndNodes.Count; i++) {
                PathBtn pathBtn = new PathBtn(i, path.EndNodes[i]);
                pathsTable.Controls.Add(pathBtn, 0, pathsTable.Controls.Count);
                //pathsTable.Controls[pathsTable.Controls.Count - 1].Height = 10;
            }

            if(path.EndNodes.Count > 0) {
                (pathsTable.Controls[0] as PathBtn).performClick();
            }

        }

        public void clearPathBtns() {
            pathsTable.Controls.Clear();
            PathBtn.clearBtns();
        }
       

        private void panel2_Paint(object sender, PaintEventArgs e) {

        }
    }
}
