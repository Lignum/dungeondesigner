﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DungeonDesigner.Src.Controllers;
using DungeonDesigner.Src.Items;

namespace DungeonDesigner.Windows {
    public partial class ItemControl : UserControl {
        
        private static ItemControl INSTANCE;
        private const string DEFAULT_BLOCK = "-None-";

        static BindingList<KeyValuePair<string, ItemDataBase>> allItems;

        Item _selectedItem;
        

        public ItemControl() {
            InitializeComponent();
            if(INSTANCE != null) {
                Debug.log("ItemControl already initialiced");
            }
            INSTANCE = this;

            allItems = new BindingList<KeyValuePair<string, ItemDataBase>>();
            initComboBlockData(null);

            cbBlock.DataSource = allItems;
            cbBlock.ValueMember = "Key";
            cbBlock.DisplayMember = "Key";

            this.Enabled = false;
        }

        public static ItemControl getInstance() {
            if(INSTANCE == null) {
                INSTANCE = new ItemControl();
            }
            return INSTANCE;
        }

        public void setItem(Item item) {
            _selectedItem = item;

            this.Enabled = _selectedItem != null;

            if(item != null) {

                ItemDataBase data = item.getItemData();

                lblName.Text = data.ItemName;

                if(_selectedItem.PathPoint.UnlockItem != null) {
                    cbBlock.SelectedValue = _selectedItem.PathPoint.UnlockItem;
                } else {
                    cbBlock.SelectedValue = DEFAULT_BLOCK;
                }
            } else {

                lblName.Text = null;
                cbBlock.SelectedValue = "";
            }
        }

        public void initComboBlockData(Dictionary<string, ItemDataBase> _zoneItems) {

            allItems.Clear();
            allItems.Add(new KeyValuePair<string, ItemDataBase>(DEFAULT_BLOCK, null));

            if(_zoneItems != null) {
                foreach(var item in _zoneItems) {
                    allItems.Add(new KeyValuePair<string, ItemDataBase>(item.Value.ItemName, item.Value));
                }
            }
        }
        public void addComboBlockData(ItemDataBase zoneItem){            
            allItems.Add(new KeyValuePair<string, ItemDataBase>(zoneItem.ItemName, zoneItem));
        }

        private void cbBlock_SelectedIndexChanged(object sender, EventArgs e) {
            if(_selectedItem != null) {
                String block = cbBlock.SelectedValue as string;
                if(block != DEFAULT_BLOCK) {
                    _selectedItem.PathPoint.UnlockItem = block;
                } else {
                    _selectedItem.PathPoint.UnlockItem = null;
                }
            }
        }
    }
}
