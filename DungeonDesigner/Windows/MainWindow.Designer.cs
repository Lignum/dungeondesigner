﻿namespace DungeonDesigner.Windows {
    partial class MainWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.ItemMenuContainer = new System.Windows.Forms.SplitContainer();
            this.ItemsContainer = new System.Windows.Forms.SplitContainer();
            this.ItemsTopContainer = new System.Windows.Forms.SplitContainer();
            this.lblItems = new System.Windows.Forms.Label();
            this.btnCreateItem = new System.Windows.Forms.Button();
            this.ItemListPnl = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.BaseSplitContainer = new System.Windows.Forms.SplitContainer();
            this.ToolPlayerSplitContainer = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.TopMenuContainer = new System.Windows.Forms.SplitContainer();
            this.txtGridSizeY = new System.Windows.Forms.NumericUpDown();
            this.chbGridSnap = new System.Windows.Forms.CheckBox();
            this.txtGridSizeX = new System.Windows.Forms.NumericUpDown();
            this.lblGrid = new System.Windows.Forms.Label();
            this.analyzeBtn = new System.Windows.Forms.Button();
            this.btnItemStart = new DungeonDesigner.Windows.Elements.ToolsBtn.ToolBtnControl();
            this.btnItemEnd = new DungeonDesigner.Windows.Elements.ToolsBtn.ToolBtnControl();
            this.btnToolItemSelector = new DungeonDesigner.Windows.Elements.ToolsBtn.ToolBtnControl();
            this.btnItemDoor = new DungeonDesigner.Windows.Elements.ToolsBtn.ToolBtnControl();
            this.btnToolRoomSel = new DungeonDesigner.Windows.Elements.ToolsBtn.ToolBtnControl();
            this.btnToolPolygon = new DungeonDesigner.Windows.Elements.ToolsBtn.ToolBtnControl();
            this.btnToolRoom = new DungeonDesigner.Windows.Elements.ToolsBtn.ToolBtnControl();
            this.drawTest1 = new DungeonDesigner.Windows.Drawers.ZoneDrawer();
            this.itemControl1 = new DungeonDesigner.Windows.ItemControl();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ItemMenuContainer)).BeginInit();
            this.ItemMenuContainer.Panel1.SuspendLayout();
            this.ItemMenuContainer.Panel2.SuspendLayout();
            this.ItemMenuContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ItemsContainer)).BeginInit();
            this.ItemsContainer.Panel1.SuspendLayout();
            this.ItemsContainer.Panel2.SuspendLayout();
            this.ItemsContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ItemsTopContainer)).BeginInit();
            this.ItemsTopContainer.Panel1.SuspendLayout();
            this.ItemsTopContainer.Panel2.SuspendLayout();
            this.ItemsTopContainer.SuspendLayout();
            this.ItemListPnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BaseSplitContainer)).BeginInit();
            this.BaseSplitContainer.Panel1.SuspendLayout();
            this.BaseSplitContainer.Panel2.SuspendLayout();
            this.BaseSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ToolPlayerSplitContainer)).BeginInit();
            this.ToolPlayerSplitContainer.Panel1.SuspendLayout();
            this.ToolPlayerSplitContainer.Panel2.SuspendLayout();
            this.ToolPlayerSplitContainer.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TopMenuContainer)).BeginInit();
            this.TopMenuContainer.Panel1.SuspendLayout();
            this.TopMenuContainer.Panel2.SuspendLayout();
            this.TopMenuContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGridSizeY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGridSizeX)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1115, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.loadToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.projectToolStripMenuItem});
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.newToolStripMenuItem.Text = "New";
            // 
            // projectToolStripMenuItem
            // 
            this.projectToolStripMenuItem.Name = "projectToolStripMenuItem";
            this.projectToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.projectToolStripMenuItem.Text = "Project";
            this.projectToolStripMenuItem.Click += new System.EventHandler(this.projectToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Enabled = false;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Enabled = false;
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.saveAsToolStripMenuItem.Text = "Save As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem1_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.loadToolStripMenuItem.Text = "Open";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "dgd";
            this.saveFileDialog.FileName = "project";
            this.saveFileDialog.Filter = "DungeonDesigner (*.dgd)|*.dgd";
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "dgd";
            this.openFileDialog.FileName = "openFileDialog1";
            this.openFileDialog.Filter = "DungeonDesigner (*.dgd)|*.dgd";
            // 
            // ItemMenuContainer
            // 
            this.ItemMenuContainer.BackColor = System.Drawing.Color.Transparent;
            this.ItemMenuContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ItemMenuContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ItemMenuContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.ItemMenuContainer.Location = new System.Drawing.Point(0, 0);
            this.ItemMenuContainer.Name = "ItemMenuContainer";
            this.ItemMenuContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // ItemMenuContainer.Panel1
            // 
            this.ItemMenuContainer.Panel1.BackColor = System.Drawing.Color.MidnightBlue;
            this.ItemMenuContainer.Panel1.Controls.Add(this.ItemsContainer);
            // 
            // ItemMenuContainer.Panel2
            // 
            this.ItemMenuContainer.Panel2.BackColor = System.Drawing.Color.MidnightBlue;
            this.ItemMenuContainer.Panel2.Controls.Add(this.itemControl1);
            this.ItemMenuContainer.Size = new System.Drawing.Size(206, 612);
            this.ItemMenuContainer.SplitterDistance = 179;
            this.ItemMenuContainer.SplitterWidth = 1;
            this.ItemMenuContainer.TabIndex = 4;
            // 
            // ItemsContainer
            // 
            this.ItemsContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ItemsContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.ItemsContainer.IsSplitterFixed = true;
            this.ItemsContainer.Location = new System.Drawing.Point(0, 0);
            this.ItemsContainer.Name = "ItemsContainer";
            this.ItemsContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // ItemsContainer.Panel1
            // 
            this.ItemsContainer.Panel1.Controls.Add(this.ItemsTopContainer);
            this.ItemsContainer.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ItemsContainer.Panel1MinSize = 5;
            // 
            // ItemsContainer.Panel2
            // 
            this.ItemsContainer.Panel2.Controls.Add(this.ItemListPnl);
            this.ItemsContainer.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ItemsContainer.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ItemsContainer.Size = new System.Drawing.Size(204, 177);
            this.ItemsContainer.SplitterDistance = 25;
            this.ItemsContainer.SplitterWidth = 1;
            this.ItemsContainer.TabIndex = 4;
            // 
            // ItemsTopContainer
            // 
            this.ItemsTopContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ItemsTopContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.ItemsTopContainer.IsSplitterFixed = true;
            this.ItemsTopContainer.Location = new System.Drawing.Point(0, 0);
            this.ItemsTopContainer.Name = "ItemsTopContainer";
            // 
            // ItemsTopContainer.Panel1
            // 
            this.ItemsTopContainer.Panel1.Controls.Add(this.lblItems);
            this.ItemsTopContainer.Panel1MinSize = 5;
            // 
            // ItemsTopContainer.Panel2
            // 
            this.ItemsTopContainer.Panel2.Controls.Add(this.btnCreateItem);
            this.ItemsTopContainer.Size = new System.Drawing.Size(204, 25);
            this.ItemsTopContainer.SplitterDistance = 175;
            this.ItemsTopContainer.TabIndex = 3;
            // 
            // lblItems
            // 
            this.lblItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblItems.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblItems.Location = new System.Drawing.Point(0, 0);
            this.lblItems.Margin = new System.Windows.Forms.Padding(3);
            this.lblItems.Name = "lblItems";
            this.lblItems.Size = new System.Drawing.Size(175, 25);
            this.lblItems.TabIndex = 0;
            this.lblItems.Text = "Items";
            this.lblItems.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCreateItem
            // 
            this.btnCreateItem.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnCreateItem.Enabled = false;
            this.btnCreateItem.FlatAppearance.BorderColor = System.Drawing.Color.CornflowerBlue;
            this.btnCreateItem.FlatAppearance.BorderSize = 0;
            this.btnCreateItem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(56)))), ((int)(((byte)(160)))));
            this.btnCreateItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreateItem.ForeColor = System.Drawing.Color.White;
            this.btnCreateItem.Location = new System.Drawing.Point(1, 2);
            this.btnCreateItem.Margin = new System.Windows.Forms.Padding(0);
            this.btnCreateItem.Name = "btnCreateItem";
            this.btnCreateItem.Size = new System.Drawing.Size(20, 20);
            this.btnCreateItem.TabIndex = 2;
            this.btnCreateItem.Text = "+";
            this.btnCreateItem.UseVisualStyleBackColor = false;
            this.btnCreateItem.Click += new System.EventHandler(this.btnCreateItem_Click);
            // 
            // ItemListPnl
            // 
            this.ItemListPnl.AutoScroll = true;
            this.ItemListPnl.BackColor = System.Drawing.Color.Black;
            this.ItemListPnl.Controls.Add(this.tableLayoutPanel1);
            this.ItemListPnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ItemListPnl.Location = new System.Drawing.Point(0, 0);
            this.ItemListPnl.Name = "ItemListPnl";
            this.ItemListPnl.Size = new System.Drawing.Size(204, 151);
            this.ItemListPnl.TabIndex = 3;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoScrollMargin = new System.Drawing.Size(100, 0);
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(204, 60);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // BaseSplitContainer
            // 
            this.BaseSplitContainer.BackColor = System.Drawing.Color.Black;
            this.BaseSplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BaseSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BaseSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.BaseSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.BaseSplitContainer.Margin = new System.Windows.Forms.Padding(1);
            this.BaseSplitContainer.Name = "BaseSplitContainer";
            // 
            // BaseSplitContainer.Panel1
            // 
            this.BaseSplitContainer.Panel1.BackColor = System.Drawing.Color.Transparent;
            this.BaseSplitContainer.Panel1.Controls.Add(this.ToolPlayerSplitContainer);
            this.BaseSplitContainer.Panel1MinSize = 500;
            // 
            // BaseSplitContainer.Panel2
            // 
            this.BaseSplitContainer.Panel2.BackColor = System.Drawing.Color.Transparent;
            this.BaseSplitContainer.Panel2.Controls.Add(this.ItemMenuContainer);
            this.BaseSplitContainer.Panel2MinSize = 200;
            this.BaseSplitContainer.Size = new System.Drawing.Size(1115, 612);
            this.BaseSplitContainer.SplitterDistance = 908;
            this.BaseSplitContainer.SplitterWidth = 1;
            this.BaseSplitContainer.TabIndex = 5;
            // 
            // ToolPlayerSplitContainer
            // 
            this.ToolPlayerSplitContainer.BackColor = System.Drawing.Color.MidnightBlue;
            this.ToolPlayerSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ToolPlayerSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.ToolPlayerSplitContainer.IsSplitterFixed = true;
            this.ToolPlayerSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.ToolPlayerSplitContainer.Name = "ToolPlayerSplitContainer";
            // 
            // ToolPlayerSplitContainer.Panel1
            // 
            this.ToolPlayerSplitContainer.Panel1.BackColor = System.Drawing.Color.MidnightBlue;
            this.ToolPlayerSplitContainer.Panel1.Controls.Add(this.tableLayoutPanel2);
            // 
            // ToolPlayerSplitContainer.Panel2
            // 
            this.ToolPlayerSplitContainer.Panel2.Controls.Add(this.analyzeBtn);
            this.ToolPlayerSplitContainer.Panel2.Controls.Add(this.drawTest1);
            this.ToolPlayerSplitContainer.Size = new System.Drawing.Size(906, 610);
            this.ToolPlayerSplitContainer.SplitterDistance = 70;
            this.ToolPlayerSplitContainer.SplitterWidth = 1;
            this.ToolPlayerSplitContainer.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.btnItemStart, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.btnItemEnd, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.btnToolItemSelector, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.btnItemDoor, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.btnToolRoomSel, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnToolPolygon, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnToolRoom, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.Padding = new System.Windows.Forms.Padding(3);
            this.tableLayoutPanel2.RowCount = 9;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(70, 317);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // TopMenuContainer
            // 
            this.TopMenuContainer.BackColor = System.Drawing.Color.MidnightBlue;
            this.TopMenuContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TopMenuContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.TopMenuContainer.IsSplitterFixed = true;
            this.TopMenuContainer.Location = new System.Drawing.Point(0, 24);
            this.TopMenuContainer.Name = "TopMenuContainer";
            this.TopMenuContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // TopMenuContainer.Panel1
            // 
            this.TopMenuContainer.Panel1.Controls.Add(this.txtGridSizeY);
            this.TopMenuContainer.Panel1.Controls.Add(this.chbGridSnap);
            this.TopMenuContainer.Panel1.Controls.Add(this.txtGridSizeX);
            this.TopMenuContainer.Panel1.Controls.Add(this.lblGrid);
            // 
            // TopMenuContainer.Panel2
            // 
            this.TopMenuContainer.Panel2.Controls.Add(this.BaseSplitContainer);
            this.TopMenuContainer.Size = new System.Drawing.Size(1115, 641);
            this.TopMenuContainer.SplitterDistance = 25;
            this.TopMenuContainer.TabIndex = 1;
            // 
            // txtGridSizeY
            // 
            this.txtGridSizeY.Enabled = false;
            this.txtGridSizeY.Location = new System.Drawing.Point(69, 3);
            this.txtGridSizeY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtGridSizeY.Name = "txtGridSizeY";
            this.txtGridSizeY.Size = new System.Drawing.Size(35, 20);
            this.txtGridSizeY.TabIndex = 4;
            this.txtGridSizeY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGridSizeY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // chbGridSnap
            // 
            this.chbGridSnap.AutoSize = true;
            this.chbGridSnap.Enabled = false;
            this.chbGridSnap.ForeColor = System.Drawing.Color.White;
            this.chbGridSnap.Location = new System.Drawing.Point(110, 5);
            this.chbGridSnap.Name = "chbGridSnap";
            this.chbGridSnap.Size = new System.Drawing.Size(85, 17);
            this.chbGridSnap.TabIndex = 3;
            this.chbGridSnap.Text = "Snap to Grid";
            this.chbGridSnap.UseVisualStyleBackColor = true;
            // 
            // txtGridSizeX
            // 
            this.txtGridSizeX.Enabled = false;
            this.txtGridSizeX.Location = new System.Drawing.Point(28, 3);
            this.txtGridSizeX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtGridSizeX.Name = "txtGridSizeX";
            this.txtGridSizeX.Size = new System.Drawing.Size(35, 20);
            this.txtGridSizeX.TabIndex = 2;
            this.txtGridSizeX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGridSizeX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblGrid
            // 
            this.lblGrid.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblGrid.ForeColor = System.Drawing.Color.White;
            this.lblGrid.Location = new System.Drawing.Point(0, 0);
            this.lblGrid.Name = "lblGrid";
            this.lblGrid.Size = new System.Drawing.Size(35, 25);
            this.lblGrid.TabIndex = 0;
            this.lblGrid.Text = "Grid:";
            this.lblGrid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // analyzeBtn
            // 
            this.analyzeBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.analyzeBtn.BackColor = System.Drawing.Color.ForestGreen;
            this.analyzeBtn.FlatAppearance.BorderColor = System.Drawing.Color.LightGreen;
            this.analyzeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.analyzeBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.analyzeBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.analyzeBtn.Location = new System.Drawing.Point(745, -1);
            this.analyzeBtn.Name = "analyzeBtn";
            this.analyzeBtn.Size = new System.Drawing.Size(90, 31);
            this.analyzeBtn.TabIndex = 1;
            this.analyzeBtn.Text = "Analyze";
            this.analyzeBtn.UseVisualStyleBackColor = false;
            this.analyzeBtn.Click += new System.EventHandler(this.analyzeBtn_Click);
            // 
            // btnItemStart
            // 
            this.btnItemStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnItemStart.Location = new System.Drawing.Point(3, 227);
            this.btnItemStart.Margin = new System.Windows.Forms.Padding(0);
            this.btnItemStart.Name = "btnItemStart";
            this.btnItemStart.Size = new System.Drawing.Size(32, 32);
            this.btnItemStart.TabIndex = 12;
            // 
            // btnItemEnd
            // 
            this.btnItemEnd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnItemEnd.Location = new System.Drawing.Point(35, 227);
            this.btnItemEnd.Margin = new System.Windows.Forms.Padding(0);
            this.btnItemEnd.Name = "btnItemEnd";
            this.btnItemEnd.Size = new System.Drawing.Size(32, 32);
            this.btnItemEnd.TabIndex = 11;
            // 
            // btnToolItemSelector
            // 
            this.btnToolItemSelector.BackgroundImage = global::DungeonDesigner.Properties.Resources.ToolRoomSelector;
            this.btnToolItemSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnToolItemSelector.Location = new System.Drawing.Point(3, 131);
            this.btnToolItemSelector.Margin = new System.Windows.Forms.Padding(0);
            this.btnToolItemSelector.Name = "btnToolItemSelector";
            this.btnToolItemSelector.Size = new System.Drawing.Size(32, 32);
            this.btnToolItemSelector.TabIndex = 10;
            // 
            // btnItemDoor
            // 
            this.btnItemDoor.BackgroundImage = global::DungeonDesigner.Properties.Resources.ToolDoor;
            this.btnItemDoor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnItemDoor.Location = new System.Drawing.Point(35, 131);
            this.btnItemDoor.Margin = new System.Windows.Forms.Padding(0);
            this.btnItemDoor.Name = "btnItemDoor";
            this.btnItemDoor.Size = new System.Drawing.Size(32, 32);
            this.btnItemDoor.TabIndex = 9;
            // 
            // btnToolRoomSel
            // 
            this.btnToolRoomSel.BackgroundImage = global::DungeonDesigner.Properties.Resources.ToolRoomSelector;
            this.btnToolRoomSel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnToolRoomSel.Location = new System.Drawing.Point(3, 35);
            this.btnToolRoomSel.Margin = new System.Windows.Forms.Padding(0);
            this.btnToolRoomSel.Name = "btnToolRoomSel";
            this.btnToolRoomSel.Size = new System.Drawing.Size(32, 32);
            this.btnToolRoomSel.TabIndex = 8;
            // 
            // btnToolPolygon
            // 
            this.btnToolPolygon.BackgroundImage = global::DungeonDesigner.Properties.Resources.ToolPolygon;
            this.btnToolPolygon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnToolPolygon.Location = new System.Drawing.Point(35, 3);
            this.btnToolPolygon.Margin = new System.Windows.Forms.Padding(0);
            this.btnToolPolygon.Name = "btnToolPolygon";
            this.btnToolPolygon.Size = new System.Drawing.Size(32, 32);
            this.btnToolPolygon.TabIndex = 7;
            // 
            // btnToolRoom
            // 
            this.btnToolRoom.BackgroundImage = global::DungeonDesigner.Properties.Resources.ToolRoom;
            this.btnToolRoom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnToolRoom.Location = new System.Drawing.Point(3, 3);
            this.btnToolRoom.Margin = new System.Windows.Forms.Padding(0);
            this.btnToolRoom.Name = "btnToolRoom";
            this.btnToolRoom.Size = new System.Drawing.Size(32, 32);
            this.btnToolRoom.TabIndex = 1;
            // 
            // drawTest1
            // 
            this.drawTest1.Content = null;
            this.drawTest1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.drawTest1.Location = new System.Drawing.Point(0, 0);
            this.drawTest1.MouseHoverUpdatesOnly = false;
            this.drawTest1.Name = "drawTest1";
            this.drawTest1.Size = new System.Drawing.Size(835, 610);
            this.drawTest1.TabIndex = 0;
            this.drawTest1.Text = "drawTest1";
            //this.drawTest1.GotFocus += new System.EventHandler(this.onFocus);
            //this.drawTest1.LostFocus += new System.EventHandler(this.onLostFocus);
            this.drawTest1.MouseEnter += new System.EventHandler(this.onMouseEnterProject);
            this.drawTest1.MouseLeave += new System.EventHandler(this.onMouseExitProject);
            this.drawTest1.Resize += new System.EventHandler(this.MainWindow_OnResize);
            // 
            // itemControl1
            // 
            this.itemControl1.BackColor = System.Drawing.Color.MidnightBlue;
            this.itemControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.itemControl1.Enabled = false;
            this.itemControl1.Location = new System.Drawing.Point(0, 0);
            this.itemControl1.MinimumSize = new System.Drawing.Size(200, 0);
            this.itemControl1.Name = "itemControl1";
            this.itemControl1.Size = new System.Drawing.Size(204, 430);
            this.itemControl1.TabIndex = 0;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1115, 665);
            this.Controls.Add(this.TopMenuContainer);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(200, 200);
            this.Name = "MainWindow";
            this.Text = "Dungeon Designer";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            //this.Leave += new System.EventHandler(this.onLostFocus);
            //this.MouseEnter += new System.EventHandler(this.onFocus);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ItemMenuContainer.Panel1.ResumeLayout(false);
            this.ItemMenuContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ItemMenuContainer)).EndInit();
            this.ItemMenuContainer.ResumeLayout(false);
            this.ItemsContainer.Panel1.ResumeLayout(false);
            this.ItemsContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ItemsContainer)).EndInit();
            this.ItemsContainer.ResumeLayout(false);
            this.ItemsTopContainer.Panel1.ResumeLayout(false);
            this.ItemsTopContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ItemsTopContainer)).EndInit();
            this.ItemsTopContainer.ResumeLayout(false);
            this.ItemListPnl.ResumeLayout(false);
            this.ItemListPnl.PerformLayout();
            this.BaseSplitContainer.Panel1.ResumeLayout(false);
            this.BaseSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BaseSplitContainer)).EndInit();
            this.BaseSplitContainer.ResumeLayout(false);
            this.ToolPlayerSplitContainer.Panel1.ResumeLayout(false);
            this.ToolPlayerSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ToolPlayerSplitContainer)).EndInit();
            this.ToolPlayerSplitContainer.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.TopMenuContainer.Panel1.ResumeLayout(false);
            this.TopMenuContainer.Panel1.PerformLayout();
            this.TopMenuContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TopMenuContainer)).EndInit();
            this.TopMenuContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtGridSizeY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGridSizeX)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }



        #endregion

        private Drawers.ZoneDrawer drawTest1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projectToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SplitContainer ItemMenuContainer;
        private System.Windows.Forms.Panel ItemListPnl;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblItems;
        private System.Windows.Forms.Button btnCreateItem;
        private System.Windows.Forms.SplitContainer BaseSplitContainer;
        private System.Windows.Forms.SplitContainer ItemsContainer;
        private System.Windows.Forms.SplitContainer ItemsTopContainer;
        private System.Windows.Forms.SplitContainer ToolPlayerSplitContainer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private ItemControl itemControl1;
        private System.Windows.Forms.SplitContainer TopMenuContainer;
        private System.Windows.Forms.Label lblGrid;
        private System.Windows.Forms.NumericUpDown txtGridSizeX;
        private System.Windows.Forms.CheckBox chbGridSnap;
        private System.Windows.Forms.NumericUpDown txtGridSizeY;
        private Elements.ToolsBtn.ToolBtnControl btnToolRoom;
        private Elements.ToolsBtn.ToolBtnControl btnToolRoomSel;
        private Elements.ToolsBtn.ToolBtnControl btnToolPolygon;
        private Elements.ToolsBtn.ToolBtnControl btnToolItemSelector;
        private Elements.ToolsBtn.ToolBtnControl btnItemDoor;
        private Elements.ToolsBtn.ToolBtnControl btnItemStart;
        private Elements.ToolsBtn.ToolBtnControl btnItemEnd;
        private System.Windows.Forms.Button analyzeBtn;
    }
}