﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DungeonDesigner.Src.Path.States;
using DungeonDesigner.Src.Path.Render;

namespace DungeonDesigner.Windows.Elements {
    public partial class PathBtn : UserControl {

        static List<PathBtn> _pathsBtns = new List<PathBtn>();
        LogicPathNode _endNode;

        public PathBtn() {//USE FOR DEBUG
            InitializeComponent();
        }

        public PathBtn(int pos, LogicPathNode endNode) {
            InitializeComponent();

            _endNode = endNode;

            _pathsBtns.Add(this);

            Dock = DockStyle.Fill;
            mainBtn.Text = $"Path #{pos}";
            
        }

        public void performClick() {
            mainBtn.Checked = true;
            mainBtn_Click(null, null);
        }

        private void mainBtn_Click(object sender, EventArgs e) {
            uncheckBtns();

            PathLineMng.drawPathToNode(_endNode);

        }

        void uncheckBtns() {

            foreach(var item in _pathsBtns) {
                if(item == this) continue;
                item.mainBtn.Checked = false;
            }
        }

        public static void clearBtns() {
            _pathsBtns.Clear();
        }

        private void mainBtn_CheckedChanged(object sender, EventArgs e) {

        }
    }
}
