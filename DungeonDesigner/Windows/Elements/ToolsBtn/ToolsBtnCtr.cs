﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Windows.Elements {
    public static class ToolsBtnCtr {

        static HashSet<I_BtnTool> _btnTools = new HashSet<I_BtnTool>();

        public static void addBtn(I_BtnTool btn) {
            _btnTools.Add(btn);
        }

        public static void enableButtons(bool enabled) {
            foreach(var item in _btnTools) {
                item.enable(enabled);
            }
        }

        public static void onToolChange(I_BtnTool btn) {
            foreach(var item in _btnTools) {
                if(item == btn) continue;
                item.deActivate();
            }
        }

    }
}
