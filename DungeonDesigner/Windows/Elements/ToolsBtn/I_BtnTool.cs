﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Windows.Elements {
    public interface I_BtnTool {

        void deActivate();
        void enable(bool enabled);
    }
}
