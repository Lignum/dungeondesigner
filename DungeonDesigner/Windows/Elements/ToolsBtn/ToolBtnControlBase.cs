﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DungeonDesigner.Windows.Elements.ToolsBtn {
    public partial class ToolBtnControlBase : UserControl, I_BtnTool {

        public new Action Click;

        public ToolBtnControlBase() {

            ToolsBtnCtr.addBtn(this);
            InitializeComponent();
        }

        public virtual void deActivate() { }
        public virtual void enable(bool enabled) { }

        protected void onBtnClick() {
            ToolsBtnCtr.onToolChange(this);
            Click();
        }
    }
}
