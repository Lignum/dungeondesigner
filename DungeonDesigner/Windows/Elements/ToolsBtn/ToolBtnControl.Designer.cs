﻿using System.ComponentModel;
using System.Drawing;

namespace DungeonDesigner.Windows.Elements.ToolsBtn {
    partial class ToolBtnControl {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        [Description("Image displayed in the button"), Category("Data")]
        new public Image BackgroundImage {
            get { return toolBtn.BackgroundImage; }
            set { toolBtn.BackgroundImage = value; }
        }
        [Description("Text displayed in the button"), Category("Data")]
        new public string Text {
            get { return toolBtn.Text; }
            set { toolBtn.Text = value; }
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.toolBtn = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // toolBtn
            // 
            this.toolBtn.Appearance = System.Windows.Forms.Appearance.Button;
            this.toolBtn.AutoSize = true;
            this.toolBtn.BackColor = System.Drawing.Color.RoyalBlue;
            this.toolBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.toolBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolBtn.FlatAppearance.BorderColor = System.Drawing.Color.CornflowerBlue;
            this.toolBtn.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(56)))), ((int)(((byte)(160)))));
            this.toolBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(56)))), ((int)(((byte)(160)))));
            this.toolBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.toolBtn.Font = new System.Drawing.Font("Microsoft YaHei UI", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolBtn.ForeColor = System.Drawing.Color.White;
            this.toolBtn.Location = new System.Drawing.Point(0, 0);
            this.toolBtn.Name = "toolBtn";
            this.toolBtn.Size = new System.Drawing.Size(150, 150);
            this.toolBtn.TabIndex = 1;
            this.toolBtn.TabStop = true;
            this.toolBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolBtn.UseCompatibleTextRendering = true;
            this.toolBtn.UseVisualStyleBackColor = false;
            this.toolBtn.Click += new System.EventHandler(this.btnClick);
            // 
            // ToolBtnControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.toolBtn);
            this.Name = "ToolBtnControl";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton toolBtn;
    }
}
