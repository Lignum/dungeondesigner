﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DungeonDesigner.Windows.Elements.ToolsBtn {
    public partial class ItemBtnControl : ToolBtnControlBase {
        public ItemBtnControl() : base () {
            InitializeComponent();
        }

        public override void deActivate() {
            toolBtn.Checked = false;
        }

        public override void enable(bool enabled) {
            toolBtn.Enabled = enabled;
        }

        private void btnClick(object sender, EventArgs e) {
            base.onBtnClick();
        }
    }
}
