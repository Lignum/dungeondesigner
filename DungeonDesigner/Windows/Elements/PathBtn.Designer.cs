﻿namespace DungeonDesigner.Windows.Elements {
    partial class PathBtn {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.mainBtn = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // mainBtn
            // 
            this.mainBtn.Appearance = System.Windows.Forms.Appearance.Button;
            this.mainBtn.AutoSize = true;
            this.mainBtn.BackColor = System.Drawing.Color.ForestGreen;
            this.mainBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainBtn.FlatAppearance.BorderColor = System.Drawing.Color.LightGreen;
            this.mainBtn.FlatAppearance.CheckedBackColor = System.Drawing.Color.DarkGreen;
            this.mainBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mainBtn.Location = new System.Drawing.Point(0, 0);
            this.mainBtn.Name = "mainBtn";
            this.mainBtn.Size = new System.Drawing.Size(421, 35);
            this.mainBtn.TabIndex = 0;
            this.mainBtn.Text = "checkBox1";
            this.mainBtn.UseVisualStyleBackColor = false;
            this.mainBtn.CheckedChanged += new System.EventHandler(this.mainBtn_CheckedChanged);
            this.mainBtn.Click += new System.EventHandler(this.mainBtn_Click);
            // 
            // PathBtn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainBtn);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "PathBtn";
            this.Size = new System.Drawing.Size(421, 35);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox mainBtn;
    }
}
