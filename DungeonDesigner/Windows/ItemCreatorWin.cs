﻿using DungeonDesigner.Src.Controllers;
using DungeonDesigner.Src.Items;
using Microsoft.Xna.Framework;
using System;

using System.Windows.Forms;

namespace DungeonDesigner.Windows {
    public partial class ItemCreatorWin : Form {
        public ItemCreatorWin() {
            InitializeComponent();

        }

        protected override void OnShown(EventArgs e) {
            base.OnShown(e);
            txtName.Focus();
            txtName.Text = "";
        }

        private void btnCreate_Click(object sender, EventArgs e) {


            string name = txtName.Text;
            ItemData<Item> itemData = new ItemData<Item>(name, "Items/ItemKey", ItemPlaceType.Floor, InteractionType.Pick, Color.White);
            ItemCreator.addZoneItem(itemData);

            Close();

        }
        
    }
}
