﻿using DungeonDesigner.Engine2D.UI;
using DungeonDesigner.Src.Controllers;
using DungeonDesigner.Src.Items;
using DungeonDesigner.Src.Rooms;
using DungeonDesigner.Windows.Analysis;
using DungeonDesigner.Windows.Elements;
using DungeonDesigner.Windows.Elements.ToolsBtn;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DungeonDesigner.Windows {
    public partial class MainWindow : Form {

        private static MainWindow INSTANCE;

        ItemCreatorWin _itemCreatorWin;

        AnalysisWindow _analysisWindow = new AnalysisWindow();

        private MainWindow() { 
            InitializeComponent();
            init();

            _itemCreatorWin = new ItemCreatorWin();

            enableElements(false);

            //BaseSplitContainer.SplitterDistance = 914;
        }

        private void init() {
            btnToolRoom.Click = toolbtnRoomClick;
            btnToolPolygon.Click = toolPolygonRoomClick;
            btnToolRoomSel.Click = toolRoomSelectorClick;

            btnToolItemSelector.Click = toolItemSelectorClick; 
            btnItemDoor.Click = toolItemDoorClick;

            btnItemStart.Click = toolItemStartClick;
            btnItemEnd.Click = toolItemEndClick;

            btnItemStart.Text = "S";
            btnItemEnd.Text = "E";
        }

        public static MainWindow getInstance() {
            if(INSTANCE == null) {
                INSTANCE = new MainWindow();
            }
            return INSTANCE;
        }
        
        
        /*void onFocus(object sender, EventArgs e) {
            drawTest1.setFocus(true);
        }
        void onLostFocus(object sender, EventArgs e) {
            drawTest1.setFocus(false);
        }*/

        public static void setItemBtns(Dictionary<string, ItemDataBase> zoneItems) {
            INSTANCE.clearItemBtns();

            foreach(var item in zoneItems) {
                INSTANCE.createItemBtn(item.Value);
            }

        }
        public static void addItemBtn(ItemDataBase itemData) {
            INSTANCE.createItemBtn(itemData);
        }

        private void MainWindow_OnResize(Object sender, EventArgs e) {
            if(drawTest1.Initialized) drawTest1.OnResize();
        }

        FormWindowState LastWindowState = FormWindowState.Minimized;
        private void Form1_Resize(object sender, EventArgs e) {

            // When window state changes
            if(WindowState != LastWindowState) {
                LastWindowState = WindowState;

                MainWindow_OnResize(null, null);
            }

        }

        private void projectToolStripMenuItem_Click(object sender, EventArgs e) {

            //if(saveFileDialog.ShowDialog() == DialogResult.OK) {
                Project.create(/*saveFileDialog.FileName*/);
                enableElements(true);
            //}
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e) {
            if(Project.getInstance().HasSavePath) {
                Project.getInstance().save();
            } else {
                if(saveFileDialog.ShowDialog() == DialogResult.OK) {
                    Project.getInstance().save(saveFileDialog.FileName);
                }
            }
        }

        private void saveAsToolStripMenuItem1_Click(object sender, EventArgs e) {
            if(saveFileDialog.ShowDialog() == DialogResult.OK) {
                Project.getInstance().save(saveFileDialog.FileName);
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e) {
            if(openFileDialog.ShowDialog() == DialogResult.OK) {
                Project.load(openFileDialog.FileName);
                enableElements(true);
            }
        }

        private void onMouseEnterProject(object sender, EventArgs e) {
            UIEventSystem.onMouseEnterProject(true);
        }

        private void onMouseExitProject(object sender, EventArgs e) {
            UIEventSystem.onMouseEnterProject(false);
        }

        public void enableElements(bool enabled) {
            saveToolStripMenuItem.Enabled = enabled;
            saveAsToolStripMenuItem.Enabled = enabled;
            btnCreateItem.Enabled = enabled;
            analyzeBtn.Enabled = enabled;

            ToolsBtnCtr.enableButtons(enabled);
        }

        private void toolbtnRoomClick() {
            Controller.setTool(Controller.Tool.SquareRoom);
        }
        private void toolPolygonRoomClick() {
            Controller.setTool(Controller.Tool.PolygonRoom);
        }
        private void toolRoomSelectorClick() {
            Controller.setTool(Controller.Tool.CornerSelection);
        }


        private void toolItemSelectorClick() {
            Controller.setTool(Controller.Tool.ItemSelector);
        }

        private void toolItemDoorClick() {
            Controller.setTool(Controller.Tool.ItemCreator);
            ItemCreator.setCreationItem("Door");
        }
        private void toolItemStartClick() {
            Controller.setTool(Controller.Tool.ItemCreator);
            ItemCreator.setCreationItem("Start");
        }
        private void toolItemEndClick() {
            Controller.setTool(Controller.Tool.ItemCreator);
            ItemCreator.setCreationItem("End");
        }

        private void btnCreateItem_Click(object sender, EventArgs e) {
            Debug.log("Open Item");

            //IsMdiContainer = true;
            //_itemCreatorWin.MdiParent = this;
            _itemCreatorWin.ShowDialog();

        }

        private void clearItemBtns() {
            tableLayoutPanel1.Controls.Clear();
        }
        private void createItemBtn(ItemDataBase itemData) {
            //RadioButton btnItem = new RadioButton();

            ItemBtnControl btnItem = new ItemBtnControl();

            tableLayoutPanel1.Controls.Add(btnItem, 0, tableLayoutPanel1.Controls.Count);
            btnItem.Dock = DockStyle.Fill;
            btnItem.Text = itemData.ItemName;
            btnItem.Click = () => btnItem_Select(itemData);

            /*btnItem.Appearance = Appearance.Button;
            btnItem.BackColor = Color.RoyalBlue;
            btnItem.FlatAppearance.BorderColor = Color.Black;
            btnItem.FlatAppearance.CheckedBackColor = Color.FromArgb(27, 56, 160);
            btnItem.FlatAppearance.MouseDownBackColor = Color.FromArgb(27, 56, 160);
            btnItem.FlatStyle = FlatStyle.Flat;
            btnItem.Font = new Font("Microsoft Sans Serif", 8F, FontStyle.Regular, GraphicsUnit.Point, 0, true);
            btnItem.Location = new Point(0, 40);
            btnItem.Margin = new Padding(0);
            btnItem.Name = $"btn_{itemData.ItemName}";
            btnItem.RightToLeft = RightToLeft.No;
            btnItem.Size = new Size(163, 20);
            btnItem.TabIndex = 5;
            btnItem.UseCompatibleTextRendering = true;
            btnItem.UseVisualStyleBackColor = false;*/



            //btnItem.Click += (sender, e) => btnItem_Select(sender, e, itemData);
        }

        private void btnItem_Select(ItemDataBase itemData) {

            Debug.log("SElect ITEM: " + itemData.ItemName);

            Controller.setTool(Controller.Tool.ItemCreator);
            ItemCreator.setCreationItem(itemData.ItemName);

        }

        private void ToolSelectItem_CheckedChanged(object sender, EventArgs e) {

            Controller.setTool(Controller.Tool.ItemSelector);
        }

        public void onToolChange() {
            //analizeTableControl.clearPathBtns();
        }
        

        private void txtGridSize_TextChanged(object sender, EventArgs e) {

        }

        private void toolBtnControl1_Load(object sender, EventArgs e) {

        }

        private void toolBtnControl1_Load_1(object sender, EventArgs e) {

        }

        private void toolBtnControl1_Load_2(object sender, EventArgs e) {

        }

        private void MainWindow_Load(object sender, EventArgs e) {

        }

        private void analyzeBtn_Click(object sender, EventArgs e) {
            _analysisWindow.ShowDialog();
        }
    }
}
