﻿namespace DungeonDesigner.Windows.Analysis {
    partial class AnalysisWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AnalysisWindow));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.nodeDrawer1 = new DungeonDesigner.Windows.Drawers.NodeDrawer();
            this.analysisDrawer1 = new DungeonDesigner.Windows.Drawers.AnalysisDrawer();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.analysisDrawer1);
            this.splitContainer1.Size = new System.Drawing.Size(1052, 507);
            this.splitContainer1.SplitterDistance = 350;
            this.splitContainer1.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.nodeDrawer1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(350, 507);
            this.panel1.TabIndex = 1;
            // 
            // nodeDrawer1
            // 
            this.nodeDrawer1.Location = new System.Drawing.Point(0, 0);
            this.nodeDrawer1.MouseHoverUpdatesOnly = false;
            this.nodeDrawer1.Name = "nodeDrawer1";
            this.nodeDrawer1.Size = new System.Drawing.Size(401, 564);
            this.nodeDrawer1.TabIndex = 0;
            this.nodeDrawer1.Text = "nodeDrawer1";
            // 
            // analysisDrawer1
            // 
            this.analysisDrawer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.analysisDrawer1.Location = new System.Drawing.Point(0, 0);
            this.analysisDrawer1.MouseHoverUpdatesOnly = false;
            this.analysisDrawer1.Name = "analysisDrawer1";
            this.analysisDrawer1.Size = new System.Drawing.Size(698, 507);
            this.analysisDrawer1.TabIndex = 0;
            this.analysisDrawer1.Text = "analysisDrawer1";
            this.analysisDrawer1.Resize += new System.EventHandler(this.onResize);
            // 
            // AnalysisWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1052, 507);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AnalysisWindow";
            this.Text = "Dungeon Desinger";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Drawers.AnalysisDrawer analysisDrawer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Drawers.NodeDrawer nodeDrawer1;
        private System.Windows.Forms.Panel panel1;
    }
}