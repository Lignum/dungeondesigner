﻿using DungeonDesigner.Src.Controllers;
using DungeonDesigner.Src.Path;
using DungeonDesigner.Src.Path.Render;
using DungeonDesigner.Src.Path.States;
using DungeonDesigner.Src.Rooms;
using DungeonDesigner.Windows.Elements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DungeonDesigner.Windows.Analysis {
    public partial class AnalysisWindow : Form {

        public AnalysisWindow() {
            InitializeComponent();

            PathSolver.addOnCalculationEnd(showPath);
        }
        

        protected override void OnShown(EventArgs e) {
            base.OnShown(e);
            Controller.setTool(Controller.Tool.None);
            Project.getInstance().getZone().solveZone();
            ToolsBtnCtr.onToolChange(null);

           
        }
        
        void showPath(LogicPath path) {

            //PathLineMng.drawPathToNode(path.EndNodes[0]);


            // for(int i = 0; i < path.EndNodes.Count; i++) {
            //PathBtn pathBtn = new PathBtn(i, path.EndNodes[i]);
            //pathsTable.Controls.Add(pathBtn, 0, pathsTable.Controls.Count);
            // }
        }

        /*void onFocus(object sender, EventArgs e) {
            analysisDrawer1.setFocus(true);
        }
        void onLostFocus(object sender, EventArgs e) {
            analysisDrawer1.setFocus(false);
        }*/

        private void onResize(Object sender, EventArgs e) {
            if(analysisDrawer1.Initialized) analysisDrawer1.OnResize();
        }
    }
}
