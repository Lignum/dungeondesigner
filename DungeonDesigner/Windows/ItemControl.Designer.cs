﻿namespace DungeonDesigner.Windows {
    partial class ItemControl {


        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblBlock = new System.Windows.Forms.Label();
            this.cbBlock = new System.Windows.Forms.ComboBox();
            this.lblName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblBlock
            // 
            this.lblBlock.AutoSize = true;
            this.lblBlock.ForeColor = System.Drawing.Color.White;
            this.lblBlock.Location = new System.Drawing.Point(3, 33);
            this.lblBlock.Name = "lblBlock";
            this.lblBlock.Size = new System.Drawing.Size(66, 13);
            this.lblBlock.TabIndex = 0;
            this.lblBlock.Text = "Unlock with:";
            // 
            // cbBlock
            // 
            this.cbBlock.FormattingEnabled = true;
            this.cbBlock.Location = new System.Drawing.Point(75, 30);
            this.cbBlock.Name = "cbBlock";
            this.cbBlock.Size = new System.Drawing.Size(121, 21);
            this.cbBlock.TabIndex = 1;
            this.cbBlock.SelectedIndexChanged += new System.EventHandler(this.cbBlock_SelectedIndexChanged);
            // 
            // lblName
            // 
            this.lblName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.White;
            this.lblName.Location = new System.Drawing.Point(0, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(203, 23);
            this.lblName.TabIndex = 2;
            this.lblName.Text = "ItemName";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ItemControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.cbBlock);
            this.Controls.Add(this.lblBlock);
            this.MinimumSize = new System.Drawing.Size(200, 0);
            this.Name = "ItemControl";
            this.Size = new System.Drawing.Size(203, 295);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBlock;
        private System.Windows.Forms.ComboBox cbBlock;
        private System.Windows.Forms.Label lblName;
    }
}
