﻿using DungeonDesigner.Extensions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D { 
    public class Mesh {

        public VertexPositionTexture[] Vertices;

        public RectF Rect;
        public Vector2 Center { get { return Rect.Center; } }

        public Mesh (VertexPositionTexture[] vert) {
            Vertices = vert;

            calculateBox();
        }
        public Mesh(Vector3[] vert, int[] triangleInd) {
            Vertices = new VertexPositionTexture[triangleInd.Length];

            for(int i = 0; i < triangleInd.Length; i++) {
                Vertices[i].Position = vert[triangleInd[i]];
            }

            calculateBox();

        }

        public void translateVertices(Vector3 dir) {
            for(int i = 0; i < Vertices.Length; i++) {
                Vertices[i].Position += dir;
            }

            calculateBox();
        }

        void calculateBox() {

            if(Vertices.Length < 1) return;

            Vector3 min = Vertices[0].Position;
            Vector3 max = Vertices[0].Position;

            foreach(var v in Vertices) {
                min = Vector3.Min(min, v.Position);
                max = Vector3.Max(max, v.Position);
            }

            Vector2 pos = new Vector2(min.X, min.Y);
            Vector2 size = new Vector2(max.X, max.Y) - pos;

            Rect = new RectF(pos, size);
        }
    }
}
