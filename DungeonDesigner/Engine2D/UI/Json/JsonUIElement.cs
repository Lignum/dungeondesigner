﻿using DungeonDesigner.Engine2D.UI.Elements;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.UI.Json {

    public enum JsonUIElementType { Image, Text, Button, RadioButton}

    public class JsonUIElement {

        public string name;
        public string groupName;

        public JsonUIElementType type;

        public Vector2 position;
        public Vector2 size;

        public Vector2 pivot;
        public Vector2 minAnchor;
        public Vector2 maxAnchor;

        public JsonUIElement[] childs;

        //IMAGE
        public string sheetName;
        public string imageName;

        //TEXT
        public string text;

        //BUTTOM
        public JsonStateDisplay stateDisplay;


        public UIElement toUIElement(Vector2 parentSize) {

            UIElement created = null;

            if(size == Vector2.Zero) size = parentSize;

            switch(type) {
                case JsonUIElementType.Image:
                    created = new Image(name, sheetName, imageName, position, size);
                    break;
                case JsonUIElementType.Text:
                    created = new Text(name, position, size, text);
                    break;
                case JsonUIElementType.Button:
                    created = new Button(name, position, size, createDisplay());
                    break;
                case JsonUIElementType.RadioButton:
                    created = new RadioButton(name, groupName, position, size, createDisplay());
                    break;
            }

            created.setPivot(pivot);            
            created.setAnchors(minAnchor, maxAnchor);

            if(childs != null) {
                UIElement child;
                foreach(var item in childs) {
                    child = item.toUIElement(size);
                    created.addChild(child);
                }
            }

            return created;
        }

        StateDisplay createDisplay() {
            return new StateDisplay(stateDisplay.sheetName, stateDisplay.pressedImageName);
        }
    }
}
