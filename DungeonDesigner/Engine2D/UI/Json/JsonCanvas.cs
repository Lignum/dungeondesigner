﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.UI.Json {
    public class JsonCanvas {


        public Vector2 screenSizeReference;
        public JsonUIElement[] elements;


        public Canvas toCanvas() {
            Canvas created = new Canvas(screenSizeReference);

            foreach(var item in elements) {
                created.addChild(item.toUIElement(screenSizeReference));

            }

            return created;
        }

    }
}
