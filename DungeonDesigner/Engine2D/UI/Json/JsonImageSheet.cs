﻿using DungeonDesigner.Engine2D.Managers;
using DungeonDesigner.Src.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.UI.Json {
    public class JsonImageSheet {

        public JsonImageSheetElement[] images;

        public Dictionary<string, UITexture> toUIImages(string textureName) {
            Texture2D texture = ResourceMng.getTexture(textureName);

            Dictionary<string, UITexture> dic = new Dictionary<string, UITexture>();

            for(int i = 0; i < images.Length; i++) {
                dic.Add(images[i].imageName, images[i].toUIImage(texture));
            }

            return dic;
        }
    }

    public class JsonImageSheetElement {

        public string imageName;

        public Point position;
        public Point size;

        public int borderTop, borderBottom, borderRight, borderLeft;

        public UITexture toUIImage(Texture2D texture) {

            UITexture image = new UITexture(texture, position, size, borderTop, borderBottom, borderRight, borderLeft);

            return image;

        }
    }
}
