﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.UI {
    public class Canvas: UIChild {


        public Canvas(Vector2 screenSizeRefence) : base(Point.Zero, screenSizeRefence.ToPoint()) {
            UIRenderer.addCanvas(this);

            ElementRectangle.Size = UIRenderer.ScreenSize.ToPoint();
        }

        public override void insertChild(int pos, UIElement element) {
            base.insertChild(pos, element);
            element.recalculateRectangle();
        }

        public override void addChild(UIElement element) {
            base.addChild(element);
            element.recalculateRectangle();
        }

        public void draw(SpriteBatch _spriteBatch) {

            //_spriteBatch.Begin();

            _spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp);
            foreach(var obj in _childs) {
                obj.draw(_spriteBatch);
                obj.drawChilds(_spriteBatch);
            }
            _spriteBatch.End();
        }

        public UIElement getElementIn(Vector2 position) {

            UIElement onPoint;

            foreach(var obj in _childs) {
                onPoint = obj.getElementIn(position);
                if(onPoint != null) {
                    return onPoint;
                }
            }

            return null;
        }

        

        

        public void onWindowResize() {

            ElementRectangle.Size = UIRenderer.ScreenSize.ToPoint();

            foreach(var obj in _childs) {
                obj.onWindowResize();
            }
        }
    }
}
