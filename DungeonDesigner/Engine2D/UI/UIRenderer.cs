﻿using DungeonDesigner.Engine2D.UI.Json;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace DungeonDesigner.Engine2D.UI {
    public static class UIRenderer {

        static GraphicsDevice _graphicsDevice;
        static SpriteBatch _spriteBatch;

        static HashSet<Canvas> _canvasList = new HashSet<Canvas>();

        public static DisplayMode DisplayMode {
            get {
                return _graphicsDevice.DisplayMode;
            }
        }

        static Vector2 _screenSize;
        public static Vector2 ScreenSize { get { return _screenSize; } }
        public static Point ResolutionReference { get; private set; }

        public static void Init(GraphicsDevice graphicsDevice, Point resolution) {
            _graphicsDevice = graphicsDevice;
            _spriteBatch = new SpriteBatch(_graphicsDevice);

            ResolutionReference = resolution;
        }

        public static void addCanvas(Canvas canvas) {
            _canvasList.Add(canvas);
        }

        public static void removeCanvas(Canvas canvas) {
            _canvasList.Remove(canvas);
        }

        public static Canvas loadCanvas(ContentManager content, string path, string fileName) {

            var filePath = Path.Combine(content.RootDirectory + path, fileName);            
            Canvas canvas = null;

            using(var stream = TitleContainer.OpenStream(filePath)) {
                using(var reader = new StreamReader(stream)) {
                    string text = reader.ReadToEnd();
                    //Debug.WriteLine(text);
                    JsonCanvas created = JsonConvert.DeserializeObject<JsonCanvas>(text);
                    canvas = created.toCanvas();
                }
            }

            return canvas;
        }

        public static void render() {
            _screenSize.X = _graphicsDevice.Viewport.Width;
            _screenSize.Y = _graphicsDevice.Viewport.Height;

            drawCanvas();
        }

        public static void onWindowResize() {

            _screenSize.X = _graphicsDevice.Viewport.Width;
            _screenSize.Y = _graphicsDevice.Viewport.Height;

            foreach(var obj in _canvasList) {
                obj.onWindowResize();
            }
        }

        public static void onWindowResize(int width, int height) {

            _screenSize.X = width;
            _screenSize.Y = height;

            foreach(var obj in _canvasList) {
                obj.onWindowResize();
            }
        }

        public static UIElement getElementIn(Vector2 position) {
            UIElement onPoint;
            foreach(var canvas in _canvasList) {
                onPoint = canvas.getElementIn(position);
                if(onPoint != null){
                    return onPoint;
                }
            }

            return null;
        }

        static void drawCanvas() {

            foreach(var obj in _canvasList) {
                obj.draw(_spriteBatch);
            }
        }
    }
}
