﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.UI {
    public abstract class UIChild {

        public UIChild Parent { get; protected set; }
        protected List<UIElement> _childs;

        public Rectangle ElementRectangle;

        public virtual Point Position {
            get {
                return ElementRectangle.Location;
            }
            set {
                ElementRectangle.Location = value;
            }
        }
        public Point Size {
            get { return ElementRectangle.Size; }
        }

        public Vector2 V_Position {
            get { return Position.ToVector2(); }
            set { Position = value.ToPoint(); }
        }
        public Vector2 V_Size { get { return Size.ToVector2(); } }
        public virtual Vector2 ReferenceSize { get; set; }

        public UIChild(Point position, Point size) {
            _childs = new List<UIElement>();

            ElementRectangle = new Rectangle(position, size);
            ReferenceSize = size.ToVector2();
        }

        public virtual void insertChild(int pos, UIElement element) {
            _childs.Insert(pos, element);
            element.Parent = this;
        }
        public virtual void addChild(UIElement element) {
            _childs.Add(element);
            element.Parent = this;
        }



        public I_EventCaster getParentCaster() {
            if(this is I_EventCaster) return this as I_EventCaster;
            if(Parent != null)return Parent.getParentCaster();
            return null;
        }

        public T findElement<T>(string name) where T : UIElement {

            UIElement found = null;

            foreach(var obj in _childs) {
                if(obj.Name == name) {
                    found = obj;
                } else {
                    found = obj.findElement<T>(name);
                }

                if(found != null) break;
            }

            return found as T;
        }

        public T findDirectPathElement<T>(string name) where T : UIElement {

            UIChild found = this;
            string[] dir = name.Split('/');

            for(int i = 0; i < dir.Length; i++) {
                found = found.findDirectElement<UIElement>(dir[i]);

                if(found == null) break;
            }

            return found as T;
        }

        public T findDirectElement<T>(string name) where T : UIElement {
            
            foreach(var child in _childs) {
                if(child.Name == name) {
                    return child as T;
                }
            }

            return null;
        }
    }
}
