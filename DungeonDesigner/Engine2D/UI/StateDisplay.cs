﻿using DungeonDesigner.Engine2D.Managers;
using DungeonDesigner.Engine2D.UI.Elements;
using DungeonDesigner.Src.Managers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.UI {
    public class StateDisplay {

        public Image Target;

        public UITexture DefaultTexture;
        public UITexture PressedTexture;


        public StateDisplay(string sheet, string pressedImage) : this(ResourceMng.getUIImage(sheet, pressedImage)) {}
        public StateDisplay(UITexture pressedTexture) {
            PressedTexture = pressedTexture;
        }
        

        public void setImageTarget(Image target) {

            Debug.log($"setImageTarget {target}");
            Target = target;

            DefaultTexture = target.Texture;
        }

        public void setPressed() {
            Target.setTexture(PressedTexture);
        }
        public void setDefauld() {
            Target.setTexture(DefaultTexture);
        }

    }
}
