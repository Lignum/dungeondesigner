﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.UI {
    public struct UITexture {

        public static UITexture Empty = new UITexture();

        public Texture2D Texture { get; private set; }

        public UIImagePart[] parts;
        public int Top, Bottom, Right, Left;

        public UITexture(Texture2D texture, Point position, Point size, int top, int bottom, int right, int left) {
            Texture = texture;

            Top = top;
            Bottom = bottom;
            Right = right;
            Left = left;

            parts = null;

            calculateRectangle(position, size, Top, Bottom, Right, Left);

        }

        public void setTexture(Texture2D texture) {
            if(Texture == texture) return;

            Texture = texture;
            calculateRectangle();
        }
        public void calculateRectangle() {
            if(Texture == null) return;
            calculateRectangle(Point.Zero, new Point(Texture.Width, Texture.Height), Top, Bottom, Right, Left);
        }

        public void calculateRectangle(Point position, Point size, int top, int bottom, int right, int left) {
            parts = new UIImagePart[9];

            parts[0] = new UIImagePart(Point.Zero, Vector2.Zero, new Rectangle(position, new Point(left, top)));

            parts[1] = new UIImagePart(new Point(left, 0), Vector2.UnitX, new Rectangle(position + new Point(left, 0), new Point(size.X - (right + left), top)));
            parts[1].Size.X = -1;

            parts[2] = new UIImagePart(new Point(-right, 0), Vector2.Zero, new Rectangle(position + new Point(size.X - right, 0), new Point(right, top)));

            parts[3] = new UIImagePart(new Point(0, top), Vector2.UnitY, new Rectangle(position + new Point(0, top), new Point(left, size.Y - (right + left))));
            parts[3].Size.Y = -1;

            parts[4] = new UIImagePart(new Point(left, top), Vector2.One, new Rectangle(position + new Point(left, top), size - new Point(right + left, top + bottom)));
            parts[4].Size = -Vector2.One;

            parts[5] = new UIImagePart(new Point(-right, top), Vector2.UnitY, new Rectangle(position + new Point(size.X - right, top), new Point(right, size.Y - (top + bottom))));
            parts[5].Size.Y = -1;

            parts[6] = new UIImagePart(new Point(0, -bottom), Vector2.Zero, new Rectangle(position + new Point(0, size.Y - bottom), new Point(left, bottom)));
            parts[7] = new UIImagePart(new Point(left, -bottom), Vector2.UnitX, new Rectangle(position + new Point(left, size.Y - bottom), new Point(size.X - (right + left), bottom)));
            parts[7].Size.X = -1;
            parts[8] = new UIImagePart(new Point(-right, -bottom), Vector2.Zero, new Rectangle(position + new Point(size.X - right, size.Y - bottom), new Point(right, bottom)));

        }
    }

    public struct UIImagePart{

        public Point DesPosition;
        public Vector2 Size;
        public Rectangle ImageRectangle;

        public Vector2 Anchor;

        public UIImagePart(Point des, Vector2 anchor, Rectangle rec) {
            DesPosition = des;
            Anchor = anchor;
            ImageRectangle = rec;

            Size = new Vector2(rec.Width, rec.Height);
        }
    }

}
