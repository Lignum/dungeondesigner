﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.UI {
    public interface I_EventCaster {

        void onMousePressed();
        void onMouseClick();
        void onMouseReleased();

    }
}
