﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.UI.Elements {
    public class RadioButton : Button {

        RadioButtonGroup _group;

        public RadioButton(string name, string groupName, Vector2 position, Vector2 size, StateDisplay display) : base(name, position, size, display) {
            _group = RadioButtonGroupMng.getInstance().getGroup(groupName);
            _group.addButton(this);
        }

        public override void onMouseClick() {
            onClick?.Invoke();
            _group.setSellected(this);
        }
        public override void onMouseReleased() {

            if(_group.Selected != this) {
                base.onMouseReleased();
            }
        }

    }
}
