﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DungeonDesigner.Engine2D.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDesigner.Engine2D.UI.Elements {
    public class Image : UIElement {

        protected UITexture _image;
        public UITexture Texture {
            get {
                return _image;
            }
            set {
                setTexture(value);
            }
        }

        Rectangle[] _imageRectangles;
        Rectangle _drawRectangle = new Rectangle();

        public Color Color = Color.White;
        public int Layer = 0;
        public bool Active = true;

        public Image(string name, string sheetName, string imageName, Vector2 position, Vector2 size) : this(name, ResourceMng.getUIImage(sheetName, imageName), position, size) { }

        public Image(string name, UITexture image, Vector2 position, Vector2 size) : base(name, position, size) {
            _image = image;            
        }


        public override void draw(SpriteBatch _spriteBatch) {

            if(!Active) return;
            if(_image.Texture == null) return;

            //_spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointClamp);

            for(int i = 0; i < 9; i++) {
                _drawRectangle.Location = ElementRectangle.Location + _imageRectangles[i].Location;
                _drawRectangle.Size = _imageRectangles[i].Size;
                _spriteBatch.Draw(_image.Texture, _drawRectangle, _image.parts[i].ImageRectangle, Color, 0, Vector2.Zero, SpriteEffects.None, Layer);
            }
            //_spriteBatch.End();
        }

        public override void recalculateRectangle() {
            base.recalculateRectangle();
            calculateRectangles();
        }
        
        void calculateRectangles() {

            _imageRectangles = new Rectangle[9];

            _imageRectangles[0] = new Rectangle(Point.Zero, new Point(_image.Left, _image.Top));
            _imageRectangles[1] = new Rectangle(new Point(_image.Left, 0), new Point(ElementRectangle.Width - (_image.Right + _image.Left), _image.Top));
            _imageRectangles[2] = new Rectangle(new Point(ElementRectangle.Size.X - _image.Right, 0), new Point(_image.Right, _image.Top));

            _imageRectangles[3] = new Rectangle(new Point(0, _image.Top), new Point(_image.Left, ElementRectangle.Height - (_image.Top + _image.Bottom)));
            _imageRectangles[4] = new Rectangle(new Point(_image.Left, _image.Top), new Point(ElementRectangle.Width - (_image.Right + _image.Left), ElementRectangle.Height - (_image.Top + _image.Bottom)));
            _imageRectangles[5] = new Rectangle(new Point(ElementRectangle.Size.X - _image.Right, _image.Top), new Point(_image.Right, ElementRectangle.Height - (_image.Top + _image.Bottom)));

            _imageRectangles[6] = new Rectangle(new Point(0, ElementRectangle.Size.Y - _image.Bottom), new Point(_image.Left, _image.Top));
            _imageRectangles[7] = new Rectangle(new Point(_image.Left, ElementRectangle.Size.Y - _image.Bottom), new Point(ElementRectangle.Width - (_image.Right + _image.Left), _image.Top));
            _imageRectangles[8] = new Rectangle(new Point(ElementRectangle.Size.X - _image.Right, ElementRectangle.Size.Y - _image.Bottom), new Point(_image.Right, _image.Top));

        }

        public void setTexture(UITexture texture) {
            _image = texture;
            recalculateRectangle();
        }

        public void setTexture(string texture) {
            _image.setTexture(ResourceMng.getTexture(texture));

            ReferenceSize = new Vector2(_image.Texture.Width, _image.Texture.Height);

            recalculateRectangle();
        }
        public void setTexture(Texture2D texture) {
            _image.setTexture(texture);

            recalculateRectangle();
        }
    }
}
