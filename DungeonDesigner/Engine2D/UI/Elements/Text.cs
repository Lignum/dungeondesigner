﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DungeonDesigner.Engine2D.Managers;
using DungeonDesigner.Src.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDesigner.Engine2D.UI.Elements {
    public class Text : UIElement {

        string _text;
        SpriteFont font;

        public Text(string name, Vector2 position, Vector2 size, string text): base(name, position, size) {
            _text = text;
            font = ResourceMng.getFont("Fonts/Arial");
        }

        public override void draw(SpriteBatch _spriteBatch) {
            
            _spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.AnisotropicClamp);
            _spriteBatch.DrawString(font, _text, V_Position, Color.White);
            _spriteBatch.End();
        }
    }
}
