﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDesigner.Engine2D.UI.Elements {
    public class Button : UIElement, I_EventCaster {

        protected StateDisplay _display;

        public Action onClick;

        public Button(string name, Vector2 position, Vector2 size) : this(name, position, size, null) {}
        public Button(string name, Vector2 position, Vector2 size, StateDisplay display) : base(name, position, size) {
            _display = display;
        }

        public override void draw(SpriteBatch _spriteBatch) {}

        public override void addChild(UIElement element) {
            base.addChild(element);

            if(_childs.Count == 1)
                _display.setImageTarget(_childs[0] as Image);
        }

        public virtual void onMousePressed() {
            if(_display != null) _display.setPressed();
        }
        public virtual void onMouseClick() {
            onClick?.Invoke();
        }
        public virtual void onMouseReleased() {
            resetDisplay();

        }

        public void resetDisplay() {
            if(_display != null) _display.setDefauld();
        }


    }
}
