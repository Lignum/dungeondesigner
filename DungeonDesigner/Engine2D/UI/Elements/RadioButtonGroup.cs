﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.UI.Elements {

    public class RadioButtonGroupMng {

        static RadioButtonGroupMng INSTANCE;

        static Dictionary<string, RadioButtonGroup> _groups;

        private RadioButtonGroupMng() {
            _groups = new Dictionary<string, RadioButtonGroup>();
        }

        public static RadioButtonGroupMng getInstance() {
            if(INSTANCE == null) {
                INSTANCE = new RadioButtonGroupMng();
            }
            return INSTANCE;
        }

        public RadioButtonGroup getGroup(string groupName) {
            if(!_groups.TryGetValue(groupName, out RadioButtonGroup group)) {               
                group = new RadioButtonGroup();
                _groups.Add(groupName, group);
            }
            return group;
            
        }

    }

    public class RadioButtonGroup {

        List<RadioButton> _buttons;

        public RadioButton Selected { get; private set; }

        public RadioButtonGroup() {
            _buttons = new List<RadioButton>();
        }

        public void addButton(RadioButton button) {
            _buttons.Add(button);
        }

        public void setSellected(RadioButton seleccted) {

            Selected = seleccted;

            foreach(var btn in _buttons) {
                if(btn == seleccted) continue;
                btn.resetDisplay();
            }
        }

    }
}
