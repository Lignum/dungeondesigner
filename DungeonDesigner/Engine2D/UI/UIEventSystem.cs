﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.UI {
    public static class UIEventSystem {

        public static UIElement HoverElement { get; private set; }

        static UIElement _lastHoverElement;
        static I_EventCaster _EventCaster;
        static I_EventCaster _EventPressed;

        public static bool IsMouseOverUI {
            get {
                return HoverElement != null;
            }
        }


        public static bool IsMouseOverProgram { get; private set; }

        public static void update() {

            //IsMouseOverProgram = checkPositionInsideProgram();

            if(!IsMouseOverProgram) return;

            calculateHoverElement();
            checkInput();
        }

        public static void onMouseEnterProject(bool enter) {
            IsMouseOverProgram = enter;
        }


        static void calculateHoverElement() {
           // HoverElement = UIRenderer.getElementIn(Input.MousePosition);

            if(_lastHoverElement == HoverElement) return;

            if(_EventPressed == null) {
                if(HoverElement != null) {
                    _EventCaster = HoverElement.getParentCaster();
                } else {
                    _EventCaster = null;
                }
            }

            _lastHoverElement = HoverElement;
        }


        static void checkInput() {
            if(_EventCaster == null) return;

            /*if(Input.isMouseButtonPressed(0)) {
                _EventPressed = _EventCaster;
                _EventPressed.onMousePressed();
            }
            if(_EventPressed != null) {
                if(Input.isMouseButtonReleased(0)) {

                    if(HoverElement != null && HoverElement.getParentCaster() == _EventPressed) {
                        _EventPressed.onMouseClick();
                    }
                    _EventPressed.onMouseReleased();

                    _EventPressed = null;
                }
            }*/
        }

    }
}
