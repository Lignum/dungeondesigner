﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DungeonDesigner.Extensions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDesigner.Engine2D.UI {
    public abstract class UIElement : UIChild {

        public string Name;

        Vector2 _referencePosition;

        public override Vector2 ReferenceSize {
            set {
                base.ReferenceSize = value;
                calculateOrigin();
            }
        }

        public Vector2 Pivot { get; private set; }
        public Vector2 Center { get; protected set; }

        public Vector2 MinAnchor { get; private set; }
        public Vector2 MaxAnchor { get; private set; }

        public Vector2 anchoredPosition;

        public bool IgnoreMouseHover = false;

        public override Point Position {
            get {
                return ElementRectangle.Location + Center.ToPoint();
            }
            set {
                ElementRectangle.Location = value - Center.ToPoint();
                _referencePosition = value.ToVector2() - Center;
            }
        }

        public UIElement(string name, Vector2 position, Vector2 size) : this(name, position, size, new Vector2(0, 0)) { }

        public UIElement(string name, Vector2 position, Vector2 size, Vector2 pivot) : this(name, position.ToPoint(), size.ToPoint(), pivot) { }

        public UIElement(string name, Point position, Point size, Vector2 pivot) : base(position, size) {

            Name = name;
            _referencePosition = position.ToVector2();
            setPivot(pivot);
        }


        public void setPivot(Vector2 pivot) {
            Pivot = pivot;
            calculateOrigin();
            //calculateRectangle();
        }

        void calculateOrigin() {
            Center = Pivot * ReferenceSize;
        }

        public void setAnchors(Vector2 min, Vector2 max) {
            MinAnchor = min;
            MaxAnchor = max;
        }
        public void setMinAnchor(Vector2 anchor) {
            MinAnchor = anchor;
        }
        public void setMaxAnchor(Vector2 anchor) {
            MaxAnchor = anchor;
        }

        void calculateDrawPosition() {
            ElementRectangle.Location = ((Vector2.One - MinAnchor) * _referencePosition + (MinAnchor * (Parent.V_Size - (Parent.ReferenceSize - _referencePosition))) - Center).ToPoint();
            ElementRectangle.Location += Parent.ElementRectangle.Location;
        }

        void calculateRectangle() {

            calculateDrawPosition();

            Vector2 sizePoint = ElementRectangle.Location.ToVector2() + ReferenceSize;
            Vector2 calculatedSize = (Vector2.One - MaxAnchor) * sizePoint + (MaxAnchor * (Parent.V_Size - (Parent.ReferenceSize - sizePoint)));

            ElementRectangle.Size = calculatedSize.ToPoint() - (ElementRectangle.Location);
        }

        public virtual void recalculateRectangle() {

            if(Parent == null) return;

            calculateRectangle();
            foreach(var item in _childs) {
                item.recalculateRectangle();
            }
        }


        public abstract void draw(SpriteBatch _spriteBatch);

        public void drawChilds(SpriteBatch _spriteBatch) {
            foreach(var item in _childs) {
                item.draw(_spriteBatch);
                item.drawChilds(_spriteBatch);
            }
        }


        public UIElement getElementIn(Vector2 position) {

            UIElement onPoint = null;

            foreach(var item in _childs) {
                onPoint = item.getElementIn(position);
                if(onPoint != null) break;
            }

            if(onPoint == null && !IgnoreMouseHover) {
                if(ElementRectangle.Contains(position)) {
                    onPoint = this;
                }
            }

            return onPoint;
        }

        

        public void onWindowResize() {
            recalculateRectangle();
        }

        public override string ToString() {
            return $"{base.ToString()} - {Name}";
        }
    }
}

