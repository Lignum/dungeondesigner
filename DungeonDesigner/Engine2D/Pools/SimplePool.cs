﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.Pools {
    public class SimplePool<T> where T : I_Poolable, new() {

        public List<T> _activeList;
        public List<T> _poolList;

        public SimplePool() {
            _activeList = new List<T>();
            _poolList = new List<T>();
        }

        public virtual T spawn() {

            T res;

            if(_poolList.Count > 0) {
                res = _poolList[0];
                _poolList.RemoveAt(0);
            } else {
                res = new T();
            }

            _activeList.Add(res);

            res.onSpawn();
            return res;
        }

        public void despawn(T item) {
            _activeList.Remove(item);
            _poolList.Add(item);

            item.onDeSpawn();
        }

        public void despawnAll() {
            foreach(var item in _activeList) {
                _poolList.Add(item);
                item.onDeSpawn();
            }

            _activeList.Clear();
        }
    }
}
