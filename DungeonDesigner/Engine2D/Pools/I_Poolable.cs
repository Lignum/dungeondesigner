﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.Pools {
    public interface I_Poolable {

        void onSpawn();
        void onDeSpawn();

    }
}
