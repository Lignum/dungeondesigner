﻿using GameMath;
using Microsoft.Xna.Framework;

namespace DungeonDesigner.Engine2D.Logic {
    public struct Line {

        public Vector2 Origin;
        public Vector2 End;

        public Line(Vector2 origin, Vector2 end) {
            Origin = origin;
            End = end;
        }

        public static Vector2 intersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4) {
            return intersection(new Line(p1, p2), new Line(p3, p4));
        }

        public static Vector2 intersection(Line l1, Line l2) {
            // Get the segments' parameters.
            float dx12 = l1.End.X - l1.Origin.X;
            float dy12 = l1.End.Y - l1.Origin.Y;
            float dx34 = l2.End.X - l2.Origin.X;
            float dy34 = l2.End.Y - l2.Origin.Y;

            // Solve for t1 and t2
            float denominator = (dy12 * dx34 - dx12 * dy34);

            if(denominator == 0) {// The lines are parallel.
                return new Vector2(float.NaN, float.NaN);
            }

            float t1 = ((l1.Origin.X - l2.Origin.X) * dy34 + (l2.Origin.Y - l1.Origin.Y) * dx34) / denominator;
            float t2 = ((l2.Origin.X - l1.Origin.X) * dy12 + (l1.Origin.Y - l2.Origin.Y) * dx12) / -denominator;

            // Find the point of intersection.
            // The segments intersect if t1 and t2 are between 0 and 1.
            if(((t1 >= 0) && (t1 <= 1) && (t2 >= 0) && (t2 <= 1))) {
                return new Vector2(l1.Origin.X + dx12 * t1, l1.Origin.Y + dy12 * t1);
            }

            return new Vector2(float.NaN, float.NaN);
        }

        public static bool checkIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4) {
            return checkIntersection(new Line(p1, p2), new Line(p3, p4));
        }

        public static bool checkIntersection(Line l1, Line l2) {
            // Get the segments' parameters.
            float dx12 = l1.End.X - l1.Origin.X;
            float dy12 = l1.End.Y - l1.Origin.Y;
            float dx34 = l2.End.X - l2.Origin.X;
            float dy34 = l2.End.Y - l2.Origin.Y;

            // Solve for t1 and t2
            float denominator = (dy12 * dx34 - dx12 * dy34);

            if(denominator == 0) {// The lines are parallel.
                return false;
            }

            float t1 = ((l1.Origin.X - l2.Origin.X) * dy34 + (l2.Origin.Y - l1.Origin.Y) * dx34) / denominator;
            float t2 = ((l2.Origin.X - l1.Origin.X) * dy12 + (l1.Origin.Y - l2.Origin.Y) * dx12) / -denominator;

            return ((t1 >= 0) && (t1 <= 1) && (t2 >= 0) && (t2 <= 1));
        }

        private bool clossestIntersection( Line l1, Line l2, out bool segments_intersect, out Vector2 intersection, out Vector2 close_l1, out Vector2 close_l2) {

            // Get the segments' parameters.
            float dx12 = l1.End.X - l1.Origin.X;
            float dy12 = l1.End.Y - l1.Origin.Y;
            float dx34 = l2.End.X - l2.Origin.X;
            float dy34 = l2.End.Y - l2.Origin.Y;

            // Solve for t1 and t2
            float denominator = (dy12 * dx34 - dx12 * dy34);
            
            if(denominator == 0) {
                // The lines are parallel (or close enough to it).
                segments_intersect = false;
                intersection = new Vector2(float.NaN, float.NaN);
                close_l1 = new Vector2(float.NaN, float.NaN);
                close_l2 = new Vector2(float.NaN, float.NaN);
                return false;
            }

            float t1 = ((l1.Origin.X - l2.Origin.X) * dy34 + (l2.Origin.Y - l1.Origin.Y) * dx34) / denominator;
            float t2 = ((l2.Origin.X - l1.Origin.X) * dy12 + (l1.Origin.Y - l2.Origin.Y) * dx12) / -denominator;

            // Find the point of intersection.
            intersection = new Vector2(l1.Origin.X + dx12 * t1, l1.Origin.Y + dy12 * t1);

            // The segments intersect if t1 and t2 are between 0 and 1.
            segments_intersect = ((t1 >= 0) && (t1 <= 1) && (t2 >= 0) && (t2 <= 1));

            // Find the closest points on the segments.
            t1 = MathHelper.Clamp(t1, 0, 1);
            t2 = MathHelper.Clamp(t2, 0, 1);

            close_l1 = new Vector2(l1.Origin.X + dx12 * t1, l1.Origin.Y + dy12 * t1);
            close_l2 = new Vector2(l2.Origin.X + dx34 * t2, l2.Origin.Y + dy34 * t2);

            return true;
        }

        public static float distanceTo(Line line, Vector2 position, out Vector2 closestPoint) {
            //t = [dx * (Pt.X - Pt1.X) + dy * (Pt.Y - Pt1.Y)] / (dx2 + dy2)

            float dx = line.End.X - line.Origin.X;
            float dy = line.End.Y - line.Origin.Y;
            if((dx == 0) && (dy == 0)) {
                // It's a point not a line segment.
                closestPoint = line.Origin;
                dx = position.X - line.Origin.X;
                dy = position.Y - line.Origin.Y;
                return Mathf.Sqrt(dx * dx + dy * dy);
            }

            // Calculate the t that minimizes the distance.
            float t = ((position.X - line.Origin.X) * dx + (position.Y - line.Origin.Y) * dy) / (dx * dx + dy * dy);

            // See if this represents one of the segment's
            // end points or a point in the middle.
            if(t < 0) {
                closestPoint = new Vector2(line.Origin.X, line.Origin.Y);
                dx = position.X - line.Origin.X;
                dy = position.Y - line.Origin.Y;
            } else if(t > 1) {
                closestPoint = new Vector2(line.End.X, line.End.Y);
                dx = position.X - line.End.X;
                dy = position.Y - line.End.Y;
            } else {
                closestPoint = new Vector2(line.Origin.X + t * dx, line.Origin.Y + t * dy);
                dx = position.X - closestPoint.X;
                dy = position.Y - closestPoint.Y;
            }

            return Mathf.Sqrt(dx * dx + dy * dy);
        }

        public override string ToString() {
            return $"Line {Origin}, {End}";
        }
    }
}
