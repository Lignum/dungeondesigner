﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.Logic {
    public interface I_Updatable {

        void update();
        void lateUpdate();

    }
}
