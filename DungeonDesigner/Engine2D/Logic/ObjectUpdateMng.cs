﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.Logic {
    public static class ObjectUpdateMng {

        static Dictionary<string, ObjectUpdate> _updatables = new Dictionary<string, ObjectUpdate>();

        public static ObjectUpdate createObjectUpdate(string name) {

            if(_updatables.ContainsKey(name)) {
                return _updatables[name];
            } else {
                ObjectUpdate upd = new ObjectUpdate();
                _updatables.Add(name, upd);

                return upd;
            }
        }


        public static void addObject(string updateName, I_Updatable obj) {
            if(!_updatables.ContainsKey(updateName)) {
                createObjectUpdate(updateName);
            }
            _updatables[updateName].addObject(obj);
        }

        public static void removeObject(string updateName, I_Updatable obj) {
            _updatables[updateName].removeObject(obj);
        }
    }

    public class ObjectUpdate {
        HashSet<I_Updatable> _updatableObjs = new HashSet<I_Updatable>();
        Queue<I_Updatable> _addQueue = new Queue<I_Updatable>();
        Queue<I_Updatable> _removeQueue = new Queue<I_Updatable>();

        public void update() {
            foreach(var item in _updatableObjs) {
                item.update();
            }

            while(_addQueue.Count > 0) {
                _updatableObjs.Add(_addQueue.Dequeue());
            }
        }

        public void lateUpdate() {
            foreach(var item in _updatableObjs) {
                item.lateUpdate();
            }
        }

        public void addObject(I_Updatable obj) {
            _addQueue.Enqueue(obj);
        }
        public void removeObject(I_Updatable obj) {
            _updatableObjs.Remove(obj);
        }
    }
}
