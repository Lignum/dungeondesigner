﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.Logic {
    public static class Time {

        static GameTime _gameTime;

        public static float DeltaTime { get; private set; }

        public static void update(GameTime gameTime) {
            _gameTime = gameTime;

            DeltaTime = (float)_gameTime.ElapsedGameTime.TotalSeconds;
        }

    }
}
