﻿using DungeonDesigner.Src.Managers;
using DungeonDesigner.Engine2D.Renderer;
using GameMath;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DungeonDesigner.Engine2D.Managers;

namespace DungeonDesigner.Engine2D.Renderer {
    public abstract class RenderObject : Transform {

        Texture2D _texture;

        Vector2 _center;
        Vector2 _origin;
        
        public readonly RenderSystem RenderSystem;

        public bool Render {
            get {
                return _texture != null;
            }
        }

        public RenderObject(string rendererName, string texture) : this(rendererName, texture, new Vector2(.5f, .5f)) {}

        public RenderObject(string rendererName, string texture, Vector2 center) {

            _center = center;

            if(texture != null) {
                _texture = ResourceMng.getTexture(texture);                
                calculateOrigin();
            }

            RenderSystem = RenderMng.getRenderer(rendererName);
            RenderSystem.addObject(this);
        }

        public void setTexture(string texture) {
            _texture = ResourceMng.getTexture(texture);
            calculateOrigin();
        }

        public void setCenter(Vector2 center) {
            _center = center;
            calculateOrigin();
        }

        void calculateOrigin() {
            _origin = new Vector2(_texture.Width, _texture.Height) * _center;
        }

        public void draw(ref SpriteBatch spriteBatch) {

            spriteBatch.Draw(_texture, Position, null, Color.White, Mathf.ToRadians( Rotation), _origin, Scale, SpriteEffects.None, 0);
        }

        public void destroySelf() {
            _texture.Dispose();

            RenderSystem.removeObject(this);
        }

    }
    
}
