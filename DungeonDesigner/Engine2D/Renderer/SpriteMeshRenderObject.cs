﻿using DungeonDesigner.Engine2D.Managers;
using DungeonDesigner.Src.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.Renderer {
    public class SpriteMeshRenderObject : MeshRenderObject {

        Vector2 _pivot;
        /*public Vector2 Center {
            get {
                return  (new Vector2(Texture.Width, Texture.Height) * .5f - new Vector2(Texture.Width, Texture.Height) * _pivot) * Scale;
            }
        }*/

        public SpriteMeshRenderObject(string[] rendererName, string textureName, Vector2 pivot, int layer) : base(rendererName, ResourceMng.getTexture(textureName), layer) {
            _pivot = pivot;
            createMesh();
        }
        public SpriteMeshRenderObject(string rendererName, string textureName, Vector2 pivot, int layer) : this( new string[] { rendererName },textureName, pivot, layer) {}

        public void setCenter(Vector2 center) {
            Vector3 dir = new Vector3(center - _pivot, 0);

            mesh.translateVertices(dir);
        }

        void createMesh() {

            if(Texture == null) return;

            VertexPositionTexture[] vertices = new VertexPositionTexture[6];

            Vector3 origin = new Vector3(Texture.Width * _pivot.X, Texture.Height * _pivot.Y, 1);

            vertices[0].Position = new Vector3(0, 0, 0) - origin;
            vertices[1].Position = new Vector3(0, Texture.Height, 0) - origin;
            vertices[2].Position = new Vector3(Texture.Width, 0, 0) - origin;

            vertices[3].Position = vertices[2].Position;
            vertices[4].Position = vertices[1].Position;
            vertices[5].Position = new Vector3(Texture.Width, Texture.Height, 0) - origin;

            vertices[0].TextureCoordinate = new Vector2(0, 1);
            vertices[1].TextureCoordinate = new Vector2(0, 0);
            vertices[2].TextureCoordinate = new Vector2(1, 1);

            vertices[3].TextureCoordinate = vertices[2].TextureCoordinate;
            vertices[4].TextureCoordinate = vertices[1].TextureCoordinate;
            vertices[5].TextureCoordinate = new Vector2(1, 0);

            mesh = new Mesh(vertices);
        }


        public override void setTexture(Texture2D texture) {
            base.setTexture(texture);
            createMesh();
        }


    }
}
