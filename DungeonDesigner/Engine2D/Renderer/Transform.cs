﻿using DungeonDesigner.Extensions;
using Microsoft.Xna.Framework;

namespace DungeonDesigner.Engine2D.Renderer {
    public class Transform {

        public virtual Vector2 Position { get; set; } = Vector2.Zero;
        public Vector2 Scale = Vector2.One;
        public float Rotation = 0;


        public Vector2 Up {
            get {
                return Vector2Ext.AngleToVector(Rotation);
            }
            set {
                Rotation = Vector2Ext.VectorToAngle(value);
            }
        }

        public Vector2 Down {
            get {
                return -Up;
            }
            set {
                Up = -value;
            }
        }
    }
}
