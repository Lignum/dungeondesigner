﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDesigner.Engine2D.Renderer {
    public class SimpleLineRenderer : Transform {

        public bool Active = true;
        public VertexPosition[] Vertices { get; private set; }
        public Vector3 LineColor { get; private set; }
        public float Alpha { get; private set; }
        public bool OverMesh;

        public RenderSystem RenderSystem;

        public SimpleLineRenderer(string rendererName) :this(rendererName, Color.White, false) {
        }

        public SimpleLineRenderer(string rendererName, Color color, bool overMesh) {
            LineColor = color.ToVector3();
            Alpha = color.A / 255f;

            if(rendererName != null) {
                RenderSystem = RenderMng.getRenderer(rendererName);
                RenderSystem.addObject(this);
            }

            OverMesh = overMesh;
        }
        public SimpleLineRenderer(string rendererName, Color color, Vector3[] points, bool overMesh) : this (rendererName, color, overMesh) {
            setPoints(points);
        }

        public void setRenderer(string rendererName) {

            if(RenderSystem != null) {
                RenderSystem.removeObject(this);
            }

            RenderSystem = RenderMng.getRenderer(rendererName);
            RenderSystem.addObject(this);
        }
        public void setRenderer(RenderSystem renderer) {

            if(RenderSystem != null) {
                RenderSystem.removeObject(this);
            }

            RenderSystem = renderer;
            RenderSystem.addObject(this);
        }

        public void setPoints(Vector3[] points) {
            Vertices = new VertexPosition[points.Length];

            for(int i = 0; i < points.Length; i++) {
                Vertices[i] = new VertexPosition(points[i]);
            }
        }

        public void Destroy() {
            Debug.log($"Destroy: {this}");
            RenderSystem.removeObject(this);
            
            onDestroy();
        }
        public virtual void onDestroy() {

        }

    }
}
