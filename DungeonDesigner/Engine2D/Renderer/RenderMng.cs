﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.Renderer {
    public static class RenderMng {

        static Dictionary<string, RenderSystem> _renderers = new Dictionary<string, RenderSystem>();

        public static RenderSystem addRenderer(string name, GraphicsDevice graphicsDevice) {

            RenderSystem render;

            if(_renderers.ContainsKey(name)) {
                render = _renderers[name];
                render.setGraphicsDevice(graphicsDevice);
            } else {
                render = new RenderSystem(graphicsDevice);
                _renderers.Add(name, render);
            }

            return render;
        }

        public static RenderSystem getRenderer(string name) {
            
            if(_renderers.ContainsKey(name)) {
                return _renderers[name];
            } else {
                RenderSystem render = new RenderSystem(null);
                _renderers.Add(name, render);

                return render;
            }
        }
    }
}
