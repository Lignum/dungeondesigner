﻿using DungeonDesigner.Engine2D.Managers;
using DungeonDesigner.Src.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using DungeonDesigner.Extensions;

namespace DungeonDesigner.Engine2D.Renderer {   

    public class MeshRenderObject : Transform, IComparable<MeshRenderObject> {

        //public VertexPositionTexture[] mesh;
        public Mesh mesh;
        public Texture2D Texture { get; private set; }
        public Color color = Color.White;
        public virtual bool Visible { get; set; } = true;

        public readonly RenderSystem[] RenderSystems;

        public virtual Vector2 Center {
            get {
                return Position + Vector2Ext.rotate(mesh.Center * Scale, Rotation);
            }
        }

        int _layer;
        public int Layer {
            get { return _layer; }
            set {
                if(_layer == value) return;
                _layer = value;

                foreach(var item in RenderSystems) {
                    item.reorderObject(this);
                }
            }
        }

        public MeshRenderObject(string[] renderersName, Texture2D texture, int layer) {
            this.Texture = texture;
            _layer = layer;

            RenderSystems = new RenderSystem[renderersName.Length];

            for(int i = 0; i < renderersName.Length; i++) {
                RenderSystems[i] = RenderMng.getRenderer(renderersName[i]);
                RenderSystems[i].addObject(this);
            }
        }

        public MeshRenderObject(string rendererName, Texture2D texture, int layer) : this(new string[] { rendererName }, texture, layer) { }
        public MeshRenderObject(string rendererName, string textureName, int layer) : this(rendererName, ResourceMng.getTexture(textureName), layer) { }
        public MeshRenderObject(string[] renderersName, string textureName, int layer) : this(renderersName, ResourceMng.getTexture(textureName), layer) { }

        public int CompareTo(MeshRenderObject other) {
            return Layer - other.Layer;
        }

        public void setTexture(string textureName) {
            setTexture(ResourceMng.getTexture(textureName));
        }
        public virtual void setTexture(Texture2D texture) {
            Texture = texture;
        }

        public void Destroy() {
            Debug.log($"Destroy: {this}");

            foreach(var item in RenderSystems) {
                item.removeObject(this);
            }
            

            mesh = null;
            Texture = null;

            onDestroy();
        }
        public virtual void onDestroy() {

        }
        
    }
}
