﻿using DungeonDesigner.Extensions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace DungeonDesigner.Engine2D.Renderer {
    public class Camera2D : Transform {
        
        GraphicsDevice _graphicsDevice;

        public float Zoom { get; set; }
        public Vector2 Origin { get; set; }

        public Matrix ViewMatrix { get; set; }
        public Matrix ProjectionMatrix { get; set; }
        public Matrix ProjectionTranslationMatrix { get; set; }

        public RectF ViewRectangle;

        public Camera2D(GraphicsDevice graphicsDevice) {
            Zoom = 1;
            Rotation = 0;
            Origin = Vector2.Zero;

            ViewRectangle = new RectF();

            _graphicsDevice = graphicsDevice;
        }

        public void update() {
            calculateMatrix();
        }

        void calculateMatrix() {

            var translationMatrix = Matrix.CreateTranslation(new Vector3(Position.X, -Position.Y, -500));
            var rotationMatrix = Matrix.CreateRotationZ(Rotation);
            var scaleMatrix = Matrix.CreateScale(new Vector3(Zoom, Zoom, 1));
            var originMatrix = Matrix.CreateTranslation(new Vector3(Origin.X, Origin.Y, 0));

            ViewMatrix = translationMatrix * rotationMatrix * scaleMatrix * originMatrix;
            ProjectionMatrix = Matrix.CreateOrthographic(_graphicsDevice.Viewport.Width, _graphicsDevice.Viewport.Height, .2f, 1000f);
            ProjectionTranslationMatrix = Matrix.CreateTranslation(_graphicsDevice.Viewport.Width / 2, -_graphicsDevice.Viewport.Height / 2, 0);

            ViewRectangle.Position = screenToWorldPosition(Vector2.Zero);
            ViewRectangle.Size = screenToWorldPosition(new Vector2(_graphicsDevice.Viewport.Width, -_graphicsDevice.Viewport.Height)) - ViewRectangle.Position;
        }

        public Vector2 screenToWorldPosition(Vector2 screenP) {
            //return Vector2.Transform(screenP, Matrix.Invert(CameraMatrix * Matrix.CreateOrthographic(800, 450, .2f, 1000f)));
            //return Vector2.Transform(screenP, Matrix.Invert(CameraMatrix));
            return Vector2.Transform(screenP * new Vector2(1, -1), Matrix.Invert(ViewMatrix * ProjectionTranslationMatrix));
        }
        public Vector2 worldToScreenPosition(Vector2 screenP) {
            //return Vector2.Transform(screenP, Matrix.Invert(CameraMatrix * Matrix.CreateOrthographic(800, 450, .2f, 1000f)));
            //return Vector2.Transform(screenP, Matrix.Invert(CameraMatrix));
            return Vector2.Transform(screenP, ViewMatrix * ProjectionTranslationMatrix) * new Vector2(1, -1);
        }
    }
}
