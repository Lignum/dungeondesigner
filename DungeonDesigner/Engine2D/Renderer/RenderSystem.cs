﻿using DungeonDesigner.Engine2D.Renderer;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System.Diagnostics;

namespace DungeonDesigner.Engine2D.Renderer {
    public class RenderSystem {

        GraphicsDevice _graphicsDevice;
        SpriteBatch _spriteBatch;

        HashSet<RenderObject> _rederObjects = new HashSet<RenderObject>();
        List<MeshRenderObject> _meshRenderObjects = new List<MeshRenderObject>();
        public List<SimpleLineRenderer> _lineRenderObjects = new List<SimpleLineRenderer>();

        public Camera2D MainCamera { get; private set; }

        public Vector3 ScreenScale { get; private set; } = Vector3.One;

        BasicEffect _effect;
        BasicEffect _lineEffect;

        public RenderSystem(GraphicsDevice graphicsDevice) {

            if(graphicsDevice != null) {
                setGraphicsDevice(graphicsDevice);
            }
        }

        public void setGraphicsDevice(GraphicsDevice graphicsDevice) {

            _graphicsDevice = graphicsDevice;
            _graphicsDevice.BlendState = BlendState.AlphaBlend;

            _spriteBatch = new SpriteBatch(_graphicsDevice);
            MainCamera = new Camera2D(_graphicsDevice);

            _effect = new BasicEffect(_graphicsDevice);
            _effect.TextureEnabled = true;

            _lineEffect = new BasicEffect(_graphicsDevice);
        }

        public void updateCamera() {

            MainCamera.update();
        }

        Vector3 calculateStartPoint() {

            Vector3 _startPoint = new Vector3(MainCamera.ViewRectangle.Position, 0);
            //_startPoint = new Vector3( Input.WorldMousePosition, 0);

            _startPoint = Vector3.Floor(_startPoint / 50) * 50;
            return _startPoint;
            //_startPoint = Vector3.Zero;
        }

        public void renderAll() {
            /*for(int i = 0; i < _lineRenderObjects.Count; i++) {

                SimpleLineRenderer renderObj = _lineRenderObjects[i];
                renderObj.Vertices[0] = new VertexPosition(calculateStartPoint());
                renderObj.Vertices[1].Position = renderObj.Vertices[0].Position + Vector3.Up * -100;
            }*/
            drawSprites();
            drawLines(false);
            drawMeshes();
            drawLines(true);
            

            //drawSprites();
        }

        void drawMeshes() {

            _effect.View = MainCamera.ViewMatrix * Matrix.CreateScale(ScreenScale);
            _effect.Projection = MainCamera.ProjectionMatrix;

            foreach(var renderObj in _meshRenderObjects) {
                if(!renderObj.Visible) continue;
                if(renderObj.mesh == null) continue;
                if(renderObj.mesh.Vertices.Length == 0) continue;
                
                _effect.World = Matrix.CreateScale(new Vector3(renderObj.Scale, 1)) * 
                    Matrix.CreateRotationZ(MathHelper.ToRadians(renderObj.Rotation)) *
                    Matrix.CreateTranslation(new Vector3(renderObj.Position, 0));

                _effect.Texture = renderObj.Texture;
                _effect.DiffuseColor = renderObj.color.ToVector3();
                _effect.Alpha = renderObj.color.A / 255f;

                foreach(var pass in _effect.CurrentTechnique.Passes) {
                    
                    pass.Apply();
                    _graphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleList, renderObj.mesh.Vertices, 0, renderObj.mesh.Vertices.Length/3);
                }
            }
        }

        void drawLines(bool overMesh) {

            _lineEffect.View = MainCamera.ViewMatrix * Matrix.CreateScale(ScreenScale);
            _lineEffect.Projection = MainCamera.ProjectionMatrix;

            foreach(var renderObj in _lineRenderObjects) {

                if(!renderObj.Active) continue;
                if(renderObj.Vertices == null) continue;
                if(renderObj.Vertices.Length < 2) continue;
                if(renderObj.OverMesh != overMesh) continue;

                _lineEffect.DiffuseColor = renderObj.LineColor;
                _lineEffect.Alpha = renderObj.Alpha;

                foreach(var pass in _lineEffect.CurrentTechnique.Passes) {
                    pass.Apply();

                    for(int i = 0; i < renderObj.Vertices.Length -1; i++) {
                        _graphicsDevice.DrawUserPrimitives(PrimitiveType.LineList, renderObj.Vertices, i, 1);
                    }
                }
            }
        }

        void drawSprites() {

            _spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied);
            //_spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied,
           //                            null, null, null, null, MainCamera.ViewMatrix * Matrix.CreateScale(ScreenScale));
            //_spriteBatch.Begin();
            /*foreach(var obj in _rederObjects) {
                if(!obj.Render) continue;
                obj.draw(ref _spriteBatch);
            }*/
            _spriteBatch.End();
        }

        public void addObject(RenderObject obj) {
            _rederObjects.Add(obj);
        }
        public void addObject(SimpleLineRenderer obj) {
            _lineRenderObjects.Add(obj);
        }

        public void addObject(MeshRenderObject obj) {
            int pos;
            for(pos = 0; pos < _meshRenderObjects.Count; pos++) {
                if(_meshRenderObjects[pos].Layer >= obj.Layer) {
                    break;
                }
            }

            _meshRenderObjects.Insert(pos, obj);
            //_meshRenderObjects.Sort();
        }

        public void removeObject(RenderObject obj) {
            _rederObjects.Remove(obj);
        }
        public void removeObject(SimpleLineRenderer obj) {
            _lineRenderObjects.Remove(obj);
        }
        public void removeObject(MeshRenderObject obj) {
            _meshRenderObjects.Remove(obj);
        }

        public void reorderObject(MeshRenderObject obj) {
            _meshRenderObjects.Remove(obj);
            addObject(obj);

        }
        
    }
}
