﻿using DungeonDesigner.Engine2D.UI;
using DungeonDesigner.Engine2D.UI.Json;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Engine2D.Managers {
    public static class ResourceMng {

        static Dictionary<string, Texture2D> _textureDictionary;
        static Dictionary<string, Dictionary<string, UITexture>> _UISheetDictionary;
        static Dictionary<string, SpriteFont> _fontsDictionary;
        static ContentManager _content;

        public static void loadResources(ContentManager content) {

            _content = content;
            _textureDictionary = new Dictionary<string, Texture2D>();
            _UISheetDictionary = new Dictionary<string, Dictionary<string, UITexture>>();
            _fontsDictionary = new Dictionary<string, SpriteFont>();

        }

        public static void unloadAll() {
            foreach(var item in _textureDictionary) {
                item.Value.Dispose();
            }

            _textureDictionary.Clear();
        }

        public static void loadTexture(string name) {
            _textureDictionary.Add(name, _content.Load<Texture2D>(name));
        }

        public static Texture2D getTexture(string name) {
            if(name == null) return null;
            if(!_textureDictionary.TryGetValue(name, out Texture2D texture)) {
                loadTexture(name);
                texture = _textureDictionary[name];
            }
            return texture;
        }

        public static UITexture getUIImage(string sheetName, string imageName) {
            if(!_UISheetDictionary.ContainsKey(sheetName)) {
                loadUISheet(sheetName);
            }
            return _UISheetDictionary[sheetName][imageName];
        }

        public static SpriteFont getFont(string name) {
            if(!_fontsDictionary.ContainsKey(name)) {
                loadFont(name);
            }

            return _fontsDictionary[name];
        }

        static void loadUISheet(string fileName) {
            var filePath = $"{_content.RootDirectory}/{fileName}.json";
            using(var stream = TitleContainer.OpenStream(filePath)) {
                using(var reader = new StreamReader(stream)) {
                    string text = reader.ReadToEnd();
                    JsonImageSheet created = JsonConvert.DeserializeObject<JsonImageSheet>(text);

                    _UISheetDictionary.Add(fileName, created.toUIImages(fileName));
                }
            }
            
        }

        static void loadFont(string name) {
            _fontsDictionary.Add(name, _content.Load<SpriteFont>(name));
        }

    }
}
