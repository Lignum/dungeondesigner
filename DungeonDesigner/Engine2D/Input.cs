﻿using DungeonDesigner.Engine2D.Logic;
using DungeonDesigner.Engine2D.Renderer;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace DungeonDesigner.Engine2D {
    public class Input {

        static Dictionary<string, Input> _inputList = new Dictionary<string, Input>();

        KeyboardState state;
        KeyboardState preState;

        MouseState mouseState;
        MouseState preMouseState;

        public Vector2 MousePosition { get; private set; }
        public Vector2 WorldMousePosition { get; private set; }

        float[] _holdTime = new float[3];
        Vector2[] _presPositions = new Vector2[3];

        public float ScrollWheelSensitivity = .02f;
        public float ScrollWheelValue { get; private set; }
        float _lastScrollWheel;
        
        public readonly RenderSystem RenderSystem;
        public string renderName;

        public Input(string rendererName) {
            this.renderName = rendererName;
            RenderSystem = RenderMng.getRenderer(rendererName);

            _inputList.Add(rendererName, this);
        }

        public static Input GetInput(string rendererName) {
             return _inputList[rendererName];
        }

        public void update() {
            state = Keyboard.GetState();
            mouseState = Mouse.GetState();

            MousePosition = new Vector2(mouseState.X, mouseState.Y);

            for(int i = 0; i < _holdTime.Length; i++) {
                if(isMouseButtonHolded(i)) {
                    _holdTime[i] += Time.DeltaTime;
                } else {
                    _holdTime[i] = 0;
                }
            }

            for(int i = 0; i < _presPositions.Length; i++) {
                if(isMouseButtonPressed(i)) {
                    _presPositions[i] = MousePosition;
                }else if(isMouseButtonReleased(i)) {
                    _presPositions[i] = Vector2.One * float.MaxValue;
                }
            }

            calculateScrollWheel();
        }

        void calculateScrollWheel() {

            ScrollWheelValue = (_lastScrollWheel - mouseState.ScrollWheelValue) * ScrollWheelSensitivity;            
            _lastScrollWheel = mouseState.ScrollWheelValue;

        }

        public void resetScrollWheel() {
            ScrollWheelValue = 0;
            _lastScrollWheel = Mouse.GetState().ScrollWheelValue;
        }

        public void lateUpdate() {
            preState = Keyboard.GetState();
            preMouseState = Mouse.GetState();
            WorldMousePosition = RenderSystem.MainCamera.screenToWorldPosition(MousePosition);
        }

        public bool isKeyDown(Keys key) {
            return state.IsKeyDown(key) && preState.IsKeyUp(key);
        }
        public bool isKeyUp(Keys key) {
            return state.IsKeyUp(key) && preState.IsKeyDown(key);
        }
        public bool isKeyHold(Keys key) {
            return state.IsKeyDown(key);
        }

        public bool isMouseButtonPressed(int button) {
            MouseButtonState state = getMouseButtonState(button);
            return state.currentState == ButtonState.Pressed && state.preState == ButtonState.Released;
        }
        public bool isMouseButtonReleased(int button) {
            MouseButtonState state = getMouseButtonState(button);
            return state.currentState == ButtonState.Released && state.preState == ButtonState.Pressed;
        }
        public bool isMouseButtonHolded(int button) {
            MouseButtonState state = getMouseButtonState(button);
            return state.currentState == ButtonState.Pressed && state.preState == ButtonState.Pressed;
        }
        public float getMouseHoldTiem(int button) {
            return _holdTime[button];
        }

        public bool isMouseButtonDraging(int button) {
            return _holdTime[button] > .05f && _presPositions[button].X != float.MaxValue && MousePosition != _presPositions[button] ;
        }


        MouseButtonState getMouseButtonState(int button) {
            MouseButtonState state = new MouseButtonState();


            switch(button) {
                case 0:
                    state.currentState = mouseState.LeftButton;
                    state.preState = preMouseState.LeftButton;
                    break;
                case 1:
                    state.currentState = mouseState.RightButton;
                    state.preState = preMouseState.RightButton;
                    break;
                case 2:
                    state.currentState = mouseState.MiddleButton;
                    state.preState = preMouseState.MiddleButton;
                    break;
            }

            return state;
        }


        struct MouseButtonState {
            public ButtonState currentState;
            public ButtonState preState;

        }
    }
}
