﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DungeonDesigner.Engine2D.Logic;
using DungeonDesigner.Extensions;
using Microsoft.Xna.Framework;

namespace DungeonDesigner.Engine2D.Physics {

    class PolygonCollider : I_Collider2D {
        
        public RectF Rect { get; private set; }

        Vector2[] _points;

        public PolygonCollider(Vector2[] points) {
            setPoints(points);
        }

        public void setPoints(Vector2[] points) {
            _points = points;
            calculateRect();
        }

        public bool checkPoint(Vector2 point) {
            if(!Rect.contains(point)) return false;

            Line PointLine = new Line(point, Vector2.UnitX * Rect.Width);
            Line segmentLine;
            
            int intersectionCount = 0;

            for(int i = 0; i < _points.Length; i++) {
                if(i<_points.Length - 1) {
                    segmentLine = new Line(_points[i], _points[i + 1]);
                } else {
                    segmentLine = new Line(_points[i], _points[0]);
                }
                if(Line.checkIntersection(PointLine, segmentLine)){
                    intersectionCount++;
                }
            }
            return intersectionCount % 2 == 1;
        }

        void calculateRect() {

            RectF r = new RectF(_points[0], Vector2.Zero);

            foreach(var item in _points) {
                r.X = MathHelper.Min(r.X, item.X);
                r.Y = MathHelper.Min(r.Y, item.Y);
            }
            foreach(var item in _points) {
                r.Width = MathHelper.Max(r.Width, item.X - r.X);
                r.Height = MathHelper.Max(r.Height, item.Y - r.Y);
            }

            Rect = r;

        }
    }
}
