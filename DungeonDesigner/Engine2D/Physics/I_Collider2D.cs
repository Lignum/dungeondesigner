﻿using DungeonDesigner.Extensions;
using Microsoft.Xna.Framework;

namespace DungeonDesigner.Engine2D.Physics {
    public interface I_Collider2D {

        RectF Rect {
            get;
        }

        bool checkPoint(Vector2 point);
    }
}
