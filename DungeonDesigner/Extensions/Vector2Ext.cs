﻿
using GameMath;
using Microsoft.Xna.Framework;
using System;
using System.Diagnostics;

namespace DungeonDesigner.Extensions {
    public static class Vector2Ext {

        public static Vector2 Up { get; } = new Vector2(0, 1);
        public static Vector2 Down { get; } = new Vector2(0, -1);
        public static Vector2 Left { get; } = new Vector2(-1, 0);
        public static Vector2 Right { get; } = new Vector2(1, 0);

        public static Vector2 rotate(this Vector2 v, float angle) {
            float ca = (float)Math.Cos(Mathf.ToRadians(angle));
            float sa = (float)Math.Sin(Mathf.ToRadians(angle));
            return new Vector2(ca * v.X - sa * v.Y, sa * v.X + ca * v.Y);
        }

        public static float angle(Vector2 from, Vector2 to) {
            return VectorToAngle(to) - VectorToAngle(from);
        }
        public static Vector2 AngleToVector(float angle) {
            angle = Mathf.ToRadians(angle + 90);
            return new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));
        }
        public static float VectorToAngle(Vector2 vector) {
            return Mathf.ToDegrees((float)Math.Atan2(-vector.X, vector.Y));
        }

        public static float distanceToLine(Vector2 pt, Vector2 lineStart, Vector2 lineEnd, out Vector2 closest) {
            //t = [dx * (Pt.X - Pt1.X) + dy * (Pt.Y - Pt1.Y)] / (dx2 + dy2)
            
            float dx = lineEnd.X - lineStart.X;
            float dy = lineEnd.Y - lineStart.Y;
            if((dx == 0) && (dy == 0)) {
                // It's a point not a line segment.
                closest = lineStart;
                dx = pt.X - lineStart.X;
                dy = pt.Y - lineStart.Y;
                return Mathf.Sqrt(dx * dx + dy * dy);
            }

            // Calculate the t that minimizes the distance.
            float t = ((pt.X - lineStart.X) * dx + (pt.Y - lineStart.Y) * dy) /
                (dx * dx + dy * dy);

            // See if this represents one of the segment's
            // end points or a point in the middle.
            if(t < 0) {
                closest = new Vector2(lineStart.X, lineStart.Y);
                dx = pt.X - lineStart.X;
                dy = pt.Y - lineStart.Y;
            } else if(t > 1) {
                closest = new Vector2(lineEnd.X, lineEnd.Y);
                dx = pt.X - lineEnd.X;
                dy = pt.Y - lineEnd.Y;
            } else {
                closest = new Vector2(lineStart.X + t * dx, lineStart.Y + t * dy);
                dx = pt.X - closest.X;
                dy = pt.Y - closest.Y;
            }

            return Mathf.Sqrt(dx * dx + dy * dy);
        }
    }
}
