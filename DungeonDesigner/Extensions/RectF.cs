﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Extensions {
    public struct RectF {

        public float X, Y, Width, Height;

        public Vector2 Position {
            get { return new Vector2(X, Y); }
            set {
                X = value.X;
                Y = value.Y;
            }
        }

        public Vector2 Size {
            get { return new Vector2(Width, Height); }
            set {
                Width = value.X;
                Height = value.Y;
            }
        }

        public Vector2 Center {
            get {
                return Position + (Size / 2);
            }
            set {
                Position = value - (Size / 2);
            }
        }


        public RectF(Vector2 position, Vector2 size) {
            X = position.X;
            Y = position.Y;

            Width = size.X;
            Height = size.Y;
        }

        public bool contains(Vector2 point) {
            if(point.X < X) return false;
            if(point.Y < Y) return false;
            if(point.X > X + Width) return false;
            if(point.Y > Y + Height) return false;

            return true;
        }

    }
}
