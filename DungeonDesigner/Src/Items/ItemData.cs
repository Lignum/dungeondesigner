﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Items {

    public enum ItemPlaceType { Floor, Wall }
    public enum InteractionType { None, Pick, SwitchValue, Permanent}

    public abstract class ItemDataBase {

        public string ItemName;
        public string ImageName;
        public ItemPlaceType PlaceType { get; private set; }
        public InteractionType InteractionType { get; private set; }

        public Color ItemColor;

        public ItemDataBase(string itemName, string imageName, ItemPlaceType placeType, InteractionType interactionType, Color color) {
            ItemName = itemName;
            ImageName = imageName;
            PlaceType = placeType;
            InteractionType = interactionType;
            ItemColor = color;
        }

        public string getTexture() {
            return ImageName;
        }

        public abstract Item instantiate(Vector2 position);

    }

    public class ItemData<T> : ItemDataBase where T : Item, new() {

        public ItemData(string itemName, string imageName, ItemPlaceType placeType, InteractionType interactionType, Color color) : 
            base(itemName, imageName, placeType, interactionType, color) {

        }

        public override Item instantiate(Vector2 position) {
            return create(position);
        }

        T create(Vector2 position) {
            T created = new T();
            created.Position = position;

            created.init(ItemName, ImageName, ItemColor);

            return created;
        }

    }
}
