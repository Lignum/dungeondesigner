﻿using DungeonDesigner.Engine2D.DebugSrc.RenderDebug;
using DungeonDesigner.Engine2D.Managers;
using DungeonDesigner.Engine2D.Renderer;
using DungeonDesigner.Extensions;
using DungeonDesigner.Src.Controllers;
using DungeonDesigner.Src.Managers;
using DungeonDesigner.Src.Other;
using DungeonDesigner.Src.Path;
using DungeonDesigner.Src.Rooms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Items {
    
    public class Item : SelectableObject, I_PathElement {

        readonly static RoomSelector _roomSelector = RoomSelector.getInstance();

        const int DEFAULT_SIZE = 32;

        public string Name {
            get { return name; }
        }
        protected string name;


        public InteractionType InteractionType {
            get {
                return getItemData().InteractionType;
            }
        }

        Vector2 _fixedPosition;
        float _fixedRotation = 0;
        protected Room _parentRoom;

        public PathPoint PathPoint { get; private set; }

        //List<Item> _connections = new List<Item>();

        RectF _rect;

        public virtual bool Serializable {
            get {
                return Visible;
            }
        }

        public virtual Vector2 BuildingPosition {
            set {
                Position = value;
            }
        }

        public bool IsFixed {
            get { return _fixedPosition != Vector2.Zero; }
        }

        

        public bool IsInteractable {
            get {
                return Visible;
            }
        }


        public Item() : this(new Vector2(.5f, .5f)) {

        }

        public Item(Vector2 center) : base(new string[] { GlobalData.ZONE_RENDERER, GlobalData.ANALYSIS_RENDERER }, null, center, GlobalData.ITEMS_LAYER) {

            _rect = new RectF(Vector2.Zero, Vector2.One * 32);
            PathPoint = new PathPoint(this);
        }

        public virtual void init(string name, string texture, Color color) {
            init(name, ResourceMng.getTexture(texture));
            _defaultColor = color;
        }
        public virtual void init(string name, Texture2D texture) {
            this.name = name;
            setTexture(texture);
        }

        public override void setTexture(Texture2D texture) {
            base.setTexture(texture);

            Vector2 size = new Vector2(texture.Width, texture.Height);

            Scale = size / (Vector2.One * DEFAULT_SIZE);
        }

        public void addConnection(Item item) {
            /* _connections.Add(item);
             item._connections.Add(this);
             */
            PathPoint.addConnection(item.PathPoint);
        }
        public void addConnection(PathPoint point) {
            PathPoint.addConnection(point);
        }


        protected override PlaceState checkPosition() {
            
            if(_roomSelector.getRoomAt(Center) == null) {
                return PlaceState.Blocked;
            } else if(getItemData().PlaceType == ItemPlaceType.Wall && _roomSelector.getWallAt(Position) == null) {
                return PlaceState.Blocked;
            }

            return PlaceState.Building;
        }

        public void setColor(Color defaultColor) {
            _defaultColor = defaultColor;
            color = _defaultColor;
        }

        public virtual bool fixPosition(Room targetRoom) {

            if(CanBePlaced) {

                clearConnections();

                //_parentRoom = RoomSelector.getInstance().getRoomAt(Center);
                _parentRoom = targetRoom ?? RoomSelector.getInstance().getRoomAt(Center);
                _parentRoom.addItem(this);

                if(getItemData().PlaceType == ItemPlaceType.Wall) {
                    setItemOnWall(_parentRoom.getClosestWall(Center, out float distance, out Vector2 point));
                }

                _fixedPosition = Position;
                _fixedRotation = Rotation;

                onFixPosition();

                return true;
            } else if(_fixedPosition != Vector2.Zero) {

                resetPosition();
            }
            return false;
        }

        protected void resetPosition() {

            BuildingPosition = _fixedPosition;
            Rotation = _fixedRotation;

            onFixPosition();
        }

        public void setItemOnWall(Wall wall) {

            if(wall != null) {
                Up = wall.getInsidePerpendicularDir();
            } else {
                Up = Vector2.UnitY;
            }

        }

        void clearConnections() {

            /*foreach(var item in _connections) {
                item._connections.Remove(this);
            }

            _connections.Clear();*/
            PathPoint.clearConnections();

            _parentRoom?.removeItem(this);
            _parentRoom = null;
        }

        public ItemDataBase getItemData() {
            return ItemCreator.getItemData(name);
        }

        public bool isPointInside(Vector2 position) {

            return Vector2.Distance(Center, position) < DEFAULT_SIZE / 2;
        }

        public override void onDestroy() {
            base.onDestroy();

            clearConnections();
        }

        public void drawDebug() {
            PathPoint.drawDebug();

            /*DebugDrawer.Color = Color.Blue;
            DebugDrawer.Alpha = .1f;

            for(int i = 0; i < _connections.Count; i++) {
                DebugDrawer.drawLine(Center, _connections[i].Center);
            }*/

        }
    }
}
