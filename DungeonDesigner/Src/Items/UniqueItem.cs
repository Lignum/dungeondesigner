﻿using DungeonDesigner.Src.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Items {
    public class UniqueItem : Item {

        static Dictionary<Zone, Dictionary<string, UniqueItem>> instances = new Dictionary<Zone, Dictionary<string, UniqueItem>>();
        
        public override bool fixPosition(Room targetRoom) {

            if(base.fixPosition(targetRoom)) {

                if(!instances.ContainsKey(targetRoom.ParentZone)) {
                    instances.Add(targetRoom.ParentZone, new Dictionary<string, UniqueItem>());
                }

                if(instances[targetRoom.ParentZone].ContainsKey(name)) {
                    if(instances[targetRoom.ParentZone][name] != this) {
                        instances[targetRoom.ParentZone][name].Destroy();
                        instances[targetRoom.ParentZone][name] = this;
                    }
                } else {
                    instances[targetRoom.ParentZone].Add(name, this);
                }
                return true;
            }
            return false;
        }

        public override void onDestroy() {

            if(_parentRoom != null) {
                instances[_parentRoom.ParentZone].Remove(name);
            }

            base.onDestroy();
            

        }

        public static UniqueItem findUniqueItem(Zone zone, string name) {
            if(instances[zone].TryGetValue(name, out UniqueItem val)) { 
                return val;
            }
            return null;
        }

    }
}
