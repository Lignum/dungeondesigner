﻿using DungeonDesigner.Src.Controllers;
using DungeonDesigner.Src.Rooms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Items {
    public class ConnectionItem : Item{

        ConnectionItem _connectedItem;

        public override Vector2 BuildingPosition {
            set {
                base.BuildingPosition = value;

                _connectedItem.Position = value;
                _connectedItem.Rotation = Rotation + 180;
            }
        }

        public ConnectionItem() : base(new Vector2(.5f, 0) ){ }

        private ConnectionItem(ConnectionItem connection) : base(new Vector2(.5f, 0)) {
            _connectedItem = connection;
        }

        public override void init(string name, Texture2D texture) {
            base.init(name, texture);

            if(_connectedItem == null) {
                _connectedItem = new ConnectionItem(this);
                _connectedItem.init(name, Texture);
                //_connectedItem.setColor(new Color(1,0,0,1f));
                _connectedItem.Visible = false;
            }
        }

        protected override PlaceState checkPosition() {
            PlaceState state = base.checkPosition();
            if(state == PlaceState.Building) {
                state = _connectedItem.baseCheckPosition();
            }
            return state;
        }

        private PlaceState baseCheckPosition() {
            return base.checkPosition();
        }

        public override bool fixPosition(Room targetRoom) {
            if(base.fixPosition(targetRoom)) {
                
                _connectedItem.Position = Position;
                _connectedItem.Rotation = Rotation + 180;

                Room connectedRoom = RoomSelector.getInstance().getRoomAt(_connectedItem.Center);

                _connectedItem.baseFixPosition(connectedRoom);

                return true;

            } else {
                resetPosition();
                _connectedItem.resetPosition();

                return false;
            }
            
        }

        void baseFixPosition(Room targetRoom) {
            base.fixPosition(targetRoom);

            addConnection(_connectedItem);
        }

        public override void onDestroy() {
            base.onDestroy();

            if(_connectedItem != null) {
                _connectedItem._connectedItem = null;
                _connectedItem.Destroy();
            }
        }
    }
}
