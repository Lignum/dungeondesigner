﻿using DungeonDesigner.Engine2D.Renderer;
using DungeonDesigner.Windows;
using System;
using System.Windows.Forms;

namespace DungeonDesigner {
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            /*using(var game = new Game1())
                game.Run();*/
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(MainWindow.getInstance());
        }
        
    }
}
