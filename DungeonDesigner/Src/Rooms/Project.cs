﻿using DungeonDesigner.Src.Controllers;
using DungeonDesigner.Src.Items;
using DungeonDesigner.Src.Managers;
using DungeonDesigner.Src.Other;
using DungeonDesigner.Src.Path;
using DungeonDesigner.Src.Rooms.Json;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace DungeonDesigner.Src.Rooms {
    public class Project {

        static Project INSTANCE;

        public static bool isCreated {
            get {
                return INSTANCE != null;
            }
        }

        Grid _grid;
        Zone _zone;
        string _savePath;

        public bool HasSavePath {
            get {
                return _savePath != null;
            }
        }

        private Project(string savePath) : this(new Zone(), savePath) { }
        private Project(Zone zone, string savePath) {

            _grid = new Grid(GlobalData.ZONE_RENDERER, 1, 1);
            _zone = zone;

            _savePath = savePath;
            INSTANCE = this;

            ItemCreator.init();
            Controller.setTool(Controller.Tool.None);
        }

        public static Project getInstance() {
            return INSTANCE;
        }

        public static void create() {
            Debug.log($"CREATE");

            INSTANCE?.unLoad();
            Project created = new Project(null);
            ItemCreator.onZoneChange(created.getZone());
        }

        public void save(string fileName = null) {
            if(_savePath == null) _savePath = fileName;

            Debug.log($"SAVE: {fileName ?? _savePath}");

            JsonProject jsonData = new JsonProject(this);
            string data = Base64Encode(JsonConvert.SerializeObject(jsonData));

            if(fileName == null) {
                File.WriteAllText(_savePath, data);
            } else {
                File.WriteAllText(fileName, data);
            }
        }
        private static string Base64Encode(string plainText) {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        private static string Base64Decode(string base64EncodedData) {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static void load(string fileName) {
            Debug.log($"LOAD: {fileName}");

            INSTANCE?.unLoad();

            string data = Base64Decode(File.ReadAllText(fileName));
            //string data = File.ReadAllText(fileName);
            JsonProject jsonData = JsonConvert.DeserializeObject<JsonProject>(data);

            Project created = new Project(fileName);
            jsonData.createProject(created);

            ItemCreator.onZoneChange(created.getZone());
        }

        public void setZone(Zone zone) {
            _zone = zone;
        }

        public Zone getZone() {
            return _zone;
        }

        public Room getRoomIn(Vector2 point, Room[] ignoredRooms) {
            return _zone.getRoomIn(point, ignoredRooms);
        }

        public Wall getClossestWall(Vector2 point, Room[] ignoredRooms, out float distance, out Vector2 closestPoint) {
            return _zone.getClosestWall(point, ignoredRooms, out distance, out closestPoint);
        }
        public WallPoint getClossestWallCorner(Vector2 point, Room[] ignoredRooms, out float distance) {
            return _zone.getClosestWallCorner(point, ignoredRooms, out distance);
        }

        void unLoad() {
            _zone.unLoad();
        }

    }
}
