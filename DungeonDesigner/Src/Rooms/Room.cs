﻿using DungeonDesigner.Src.Meshes;
using DungeonDesigner.Engine2D.Physics;
using DungeonDesigner.Engine2D.Renderer;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Diagnostics;
using DungeonDesigner.Src.Controllers;
using DungeonDesigner.Src.Items;
using DungeonDesigner.Engine2D.DebugSrc.RenderDebug;
using DungeonDesigner.Engine2D.DebugSrc;
using DungeonDesigner.Extensions;
using DungeonDesigner.Src.Path;
using DungeonDesigner.Src.Managers;

namespace DungeonDesigner.Src.Rooms {
    public class Room : MeshRenderObject, I_DebugDrawerObject, I_PathElement {

        const byte ALFA_VALUE = 255 / 2;

        public Zone ParentZone { get; private set; }

        PolygonCollider _collider;

        Color _defaultColor = Color.White;

        List<WallPoint> _wallPoints;

        List<Item> _items;
        PathPoint _pathPoint;
        
        public Vector2 Size { get { return _collider.Rect.Size; } }

        public bool ClockWise { get; private set; }

        public string Name {
            get { return "Room"; }
        }
        public InteractionType InteractionType {
            get {
                return InteractionType.None;
            }
        }

        public Room(Zone zone, Vector2 position) : base(new string[]{ GlobalData.ZONE_RENDERER, GlobalData.ANALYSIS_RENDERER }, "Pixel", 0) {
            
            constructor(zone, new List<WallPoint>(4) {
                new WallPoint(position),
                new WallPoint(position + new Vector2(0, 1)),
                new WallPoint(position + new Vector2(1, 1)),
                new WallPoint(position + new Vector2(1, 0))
            }, true);
        }

        public Room(Zone zone, List<WallPoint> wallPoints, bool createWalls = false) : base(new string[] { GlobalData.ZONE_RENDERER, GlobalData.ANALYSIS_RENDERER }, "Pixel", 0) {
            constructor(zone, wallPoints, createWalls);
        }

        private void constructor(Zone zone, List<WallPoint> wallPoints, bool createWalls) {
            
            //################
            DebugRenderer.addDebugObject(this);//DEBUG
            //################

            ParentZone = zone;
            zone.addRoom(this);

            _wallPoints = new List<WallPoint>(wallPoints);

            foreach(var corner in _wallPoints) {
                corner.setRoomRef(this);
            }

            if(createWalls) {
                buildWalls();
            }

            paint(Color.Transparent);
            setOnTop(false);

            triangulateWallPoints();

            _items = new List<Item>(1);
            _pathPoint = new PathPoint(this);

            checkClockWiseDirection();

        }

        void buildWalls() {
            for(int i = 0; i < _wallPoints.Count - 1; i++) {
                new Wall(_wallPoints[i], _wallPoints[i+1]);
            }
            new Wall(_wallPoints[_wallPoints.Count - 1], _wallPoints[0]);
        }

        public void setPoint(int point, Vector2 position, bool triangulate = true) {
            _wallPoints[point].Position = position;

            if(triangulate)
                triangulateWallPoints();
        }

        public WallPoint getPoint(int point) {
            return _wallPoints[point];
        }

        public void addPointAfter(WallPoint point, WallPoint created) {
            _wallPoints.Insert(_wallPoints.IndexOf(point) + 1, created);
            created.setRoomRef(this);
        }

        public Wall getClosestWall(Vector2 position, out float distance, out Vector2 closestPoint) {

            distance = -1;
            closestPoint = Vector2.Zero;

            //if(_walls == null) return null;

            float dis;
            Wall closestWall = null;

            foreach(var corner in _wallPoints) {
                foreach(var wall in corner.Walls) {
                    dis = wall.distanceTo(position, out Vector2 point);
                    if(dis < distance || distance == -1) {
                        distance = dis;
                        closestWall = wall;
                        closestPoint = point;
                    }
                }
            }

            return closestWall;
        }

        public WallPoint getClosestWallCorner(Vector2 position, out float distance) {

            distance = -1;

            if(_wallPoints == null) return null;

            float dis;
            WallPoint closestWallPoint = null;

            foreach(var corner in _wallPoints) {

                dis = Vector2.Distance(corner.Position, position);
                if(dis < distance || distance == -1) {
                    distance = dis;
                    closestWallPoint = corner;
                }
            }

            return closestWallPoint;
        }

        public bool contains(Vector2 point) {
            return _collider.checkPoint(point);
        }

        public void paint(Color paintColor) {

            if(paintColor == Color.Transparent) {
                color = _defaultColor;
            } else {
                color = paintColor;
            }

            /*foreach(var wall in _walls) {
                wall.color = color;
            }*/
            foreach(var corner in _wallPoints) {
                //corner.color = color;
                corner.paintWalls(color, false);
            }
            color.A = ALFA_VALUE;
        }

        public void setOnTop(bool topLayer, int des = 0) {

            /*foreach(var wall in _walls) {
                wall.setOnTop(topLayer, des);
            }*/

            foreach(var corner in _wallPoints) {
                corner.setWallsOnTop(topLayer, des);
            }
        }

        public void addItem(Item item) {

            item.addConnection(_pathPoint);
            /*
            foreach(var i in _items) {
                i.addConnection(item);
            }*/
            _items.Add(item);

        }
        public void removeItem(Item item) {
            
            _items.Remove(item);

        }

        public bool isValid() {

            if(mesh.Vertices.Length < _wallPoints.Count) return false;
            
            if(Size.X < 10 || Size.Y < 10) return false;

            foreach(var corner in _wallPoints) {
                foreach(var c2 in _wallPoints) {
                    if(c2 == corner) continue;

                    if(Vector2.Distance(c2.Position, corner.Position) <= RoomSelector.HOVER_WALL_DISTANCE) {
                        return false;
                    }
                }
            }

            return true;
        }

        public void onEdit() {
            triangulateWallPoints();

            List<Item> auxItems = new List<Item>(_items);

            foreach(var item in auxItems) {
                item.Destroy();
            }
        }

        public void triangulateWallPoints() {
            List<Vector2> corners = new List<Vector2>(_wallPoints.Count);

            foreach(var item in _wallPoints) {
                corners.Add(item.Position);
            }

            mesh = Triangulator.TriangulateToMesh(corners);
            color.A = ALFA_VALUE;

            _collider = new PolygonCollider(corners.ToArray());

        }

        public void fixCornerPositions() {
            foreach(var corner in _wallPoints) {
                corner.fixPosition();
            }

            checkClockWiseDirection();
        }

        public WallPoint[] getWallPoints() {
            return _wallPoints.ToArray();
        }

        public Item getItemAt(Vector2 position) {
            
            foreach(var item in _items) {
                if(item.isPointInside(position)) {
                    return (item.IsInteractable) ? item : null;
                }
            }

            return null;
        }

        public List<Item> getAllItems() {
            return _items;
        }

        public override void onDestroy() {
            base.onDestroy();

            /*foreach(var walls in _walls) {
                walls.Destroy();
            }*/

            foreach(var points in _wallPoints) {
                points.Destroy();
            }
            _wallPoints.Clear();

            Item[] tItems = _items.ToArray();
            foreach(var item in tItems) {
                item.Destroy();
            }
            _items.Clear();
        }

        private void checkClockWiseDirection() {
            Vector2 direction = _wallPoints[1].Position - _wallPoints[0].Position;
            Vector2 midPoint = _wallPoints[0].Position + direction * .5f;

            direction.Normalize();
            direction = direction.rotate(90);

            ClockWise = _collider.checkPoint(midPoint + direction * -16);
        }

        public HashSet<PathPoint> getAllPathPoints() {
            HashSet<PathPoint> points = new HashSet<PathPoint>();
            points.Add(_pathPoint);
            foreach(var item in _items) {
                points.Add(item.PathPoint);
            }

            return points;
        }

        public override string ToString() {
            return $"Room[{_collider.Rect.Center}, {_collider.Rect.Size}]";
        }

        public void drawDebug() {

            foreach(var item in _items) {
                item.drawDebug();
            }
            foreach(var points in _wallPoints) {
                foreach(var wall in points.Walls) {
                    wall.drawDebug();
                }
            }
        }
    }
}

