﻿using DungeonDesigner.Src.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Rooms.Json {
    public class JsonZone {

        public JsonRoom[] rooms;
        public JsonItemData[] itemsData;

        public JsonZone() { }

        public JsonZone(Zone zone) {
            Room[] baseRooms = zone.getRooms();

            rooms = new JsonRoom[baseRooms.Length];

            for(int i = 0; i < baseRooms.Length; i++) {
                rooms[i] = new JsonRoom(baseRooms[i]);
            }

            saveItems(zone);
        }

        void saveItems(Zone zone) {

            Dictionary<string, ItemDataBase> zoneItems = zone.getZoneItems();
            itemsData = new JsonItemData[zoneItems.Count];

            int pos = 0;
            foreach(var item in zoneItems) {
                itemsData[pos] = new JsonItemData(item.Value);
                pos++;
            }

        }

        public Zone createZone() {
            Zone zone = new Zone();
            createItemsData(zone);

            return zone;
        }

        public void createRooms(Zone zone) {
            if(rooms == null) return;

            Dictionary<JsonRoom, Room> roomsDic = new Dictionary<JsonRoom, Room>();
            Room createdRoom;
            foreach(var room in rooms) {
                createdRoom = room.createRoom(zone);
                roomsDic.Add(room, createdRoom);
            }

            createItems(roomsDic);
        }

        void createItemsData(Zone zone) {
            if(itemsData == null) return;
            foreach(var item in itemsData) {
                zone.addItemData(item.createItemData());
            }
        }

        void createItems(Dictionary<JsonRoom, Room> roomsDic) {

            foreach(var room in roomsDic) {
                room.Key.createItems(room.Value);
            }
        }

    }
}
