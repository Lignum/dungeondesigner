﻿using DungeonDesigner.Src.Path;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Rooms.Json {
    public class JsonPathPoint {

        public string UnlockItem;

        public JsonPathPoint() {}

        public void createPathPoint(PathPoint point) {
            point.UnlockItem = UnlockItem;

        }

    }
}
