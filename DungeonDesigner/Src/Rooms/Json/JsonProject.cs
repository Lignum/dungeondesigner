﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Rooms.Json {
    public class JsonProject {

        public JsonZone zone;

        public JsonProject() { }

        public JsonProject(Project project) {

            zone = new JsonZone( project.getZone());

        }

        public void createProject(Project created) {
            Zone createdZone = zone.createZone();

            created.setZone(createdZone);
            zone.createRooms(createdZone);
        }

    }
}
