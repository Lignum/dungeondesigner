﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DungeonDesigner.Src.Items;
using DungeonDesigner.Src.Controllers;
using DungeonDesigner.Src.Managers;

namespace DungeonDesigner.Src.Rooms.Json {
    public class JsonRoom {

        public Vector2[] corners;
        public JsonPlaceItem[] items;

        public JsonRoom() {

        }

        public JsonRoom (Room room) {

            WallPoint[] points = room.getWallPoints();
            corners = new Vector2[points.Length];

            for(int i = 0; i < points.Length; i++) {
                corners[i] = points[i].Position;
            }


            serializeItems(room);
        }

        void serializeItems(Room room) {
            List<Item> placedItems = room.getAllItems();
            List<JsonPlaceItem> serialized = new List<JsonPlaceItem>();

            foreach(var item in placedItems) {
                if(item.Serializable) {
                    serialized.Add(new JsonPlaceItem(item));
                }
            }

            items = serialized.ToArray();
        }

        public Room createRoom(Zone zone) {

            List<WallPoint> points = new List<WallPoint>(corners.Length);

            foreach(var item in corners) {
                points.Add(new WallPoint(item));
            }

            Room created = new Room(zone, points, true);
            created.fixCornerPositions();

            return created;
        }

        public void createItems(Room room) {

            if(items == null) return;

            foreach(var item in items) {
                item.createItem(room);
            }
        }

    }
}
