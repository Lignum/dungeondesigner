﻿using DungeonDesigner.Src.Items;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Rooms.Json {
    public class JsonItemData {

        public string name;
        public string imageName;
        public ItemPlaceType placeType;
        public Color color;

        public string itemType;

        public JsonItemData() { }

        public JsonItemData(ItemDataBase item) {
            itemType = item.GetType().ToString();

            name = item.ItemName;
            imageName = item.ImageName;
            placeType = item.PlaceType;
            color = item.ItemColor;
        }

        public ItemDataBase createItemData() {
            Type type = Type.GetType(itemType);

            ItemDataBase obj = (ItemDataBase)Activator.CreateInstance(type, name, imageName, placeType, InteractionType.Pick, color);

            return obj;
        }

    }
}
