﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DungeonDesigner.Src.Items;
using DungeonDesigner.Src.Controllers;

namespace DungeonDesigner.Src.Rooms.Json {
    public class JsonPlaceItem {

        public string name;
        public Vector2 position;

        public JsonPathPoint pathPoint;

        public JsonPlaceItem() { }
         
        public JsonPlaceItem(Item item) {
            ItemDataBase data = item.getItemData();

            name = data.ItemName;
            position = item.Position;

            pathPoint = new JsonPathPoint {
                UnlockItem = item.PathPoint.UnlockItem
            };
        }

        public void createItem(Room room) {
            
            Item createdItem = ItemCreator.createNewItemInstance(name);
            createdItem.BuildingPosition = position;

            createdItem.fixPosition(room);

            if(pathPoint != null)
                pathPoint.createPathPoint(createdItem.PathPoint);
            
        }
    }
}
