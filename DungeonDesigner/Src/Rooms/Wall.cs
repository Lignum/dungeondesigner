﻿using DungeonDesigner.Engine2D.DebugSrc.RenderDebug;
using DungeonDesigner.Engine2D.Logic;
using DungeonDesigner.Engine2D.Renderer;
using DungeonDesigner.Extensions;
using DungeonDesigner.Src.Managers;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Diagnostics;

namespace DungeonDesigner.Src.Rooms {
    public class Wall : SpriteMeshRenderObject {

        const int WALL_WIDTH = 4;

        Color _defaultColor = Color.White;

        public WallPoint Start { get; private set; }
        public WallPoint End { get; private set; }
        Line _line;
        public Vector2 Direction { get; private set; }

        public Room RoomRef {
            get { return Start.RoomRef; }
        }

        public Wall(WallPoint start, WallPoint end) : base(new string[] { GlobalData.ZONE_RENDERER, GlobalData.ANALYSIS_RENDERER }, "Pixel", new Vector2(.5f, 0), GlobalData.WALL_LAYER) {

            Start = start;
            End = end;

            Position = Start.Position;
            Scale.X = WALL_WIDTH;

            Scale.Y = GlobalData.WALL_TOP_LAYER;

            Start.addWallRef(this);
            End.addWallRef(this);

            _line = new Line(Start.Position, End.Position);

            updateDimension();
        }

        public void updateDimension() {
            Position = Start.Position;

            //Up = Position - End.Position;
            Up = End.Position - Position;

            Scale.Y = Vector2.Distance(Position, End.Position);

            _line.Origin = Start.Position;
            _line.End = End.Position;

            Vector2 dir = Start.Position - End.Position;
            dir.Normalize();
            Direction = dir;
        }

        public void setEndPoint(WallPoint end) {
            End.removeWallRef(this);
            End = end;
            End.addWallRef(this);

            updateDimension();
        }

        public void removePoint(WallPoint point) {
            if(Start == point) Start = null;
            if(End == point) End = null;

            if(Start == null && End == null) Destroy();
        }


        public void divideBy(WallPoint mid) {
            new Wall(mid, End);

            setEndPoint(mid);
            Start.RoomRef.addPointAfter(Start, mid);
        }

        public WallPoint divideAt(Vector2 position) {

            HashSet<Wall> siblings = new HashSet<Wall>(Start.getSiblingWalls(Start, End));
            List<Wall> sibEnd = new List<Wall>(End.getSiblingWalls(Start, End));

            foreach(var item in sibEnd) {
                siblings.Add(item);
            }
            

            WallPoint created = null;
            foreach(var item in siblings) {
                created = new WallPoint(position);
                item.divideBy(created);
            }

            created.fixPosition();
            return created;

        }

        public void paint(Color paintColor) {
            if(paintColor == Color.Transparent) {
                color = _defaultColor;
            } else {
                color = paintColor;
            }
        }

        public void paintCorners(Color paintColor) {
            Start.paint(paintColor);
            End.paint(paintColor);
        }
        public void setCornersOnTop(bool top, int des = 0) {
            Start.setOnTop(top, des);
            End.setOnTop(top, des);
        }

        public void setOnTop(bool top, int des = 0) {
            if(top) {
                Layer = GlobalData.WALL_TOP_LAYER + des;
            } else {
                Layer = GlobalData.WALL_LAYER;
            }
        }

        public float distanceTo(Vector2 position, out Vector2 closestPoint) {
            return Line.distanceTo(_line, position, out closestPoint);
        }

        public bool equals(WallPoint p1, WallPoint p2) {
            if(Start.Position == p1.Position && End.Position == p2.Position) return true;
            if(End.Position == p1.Position && Start.Position == p2.Position) return true;

            return false;
        }

        public Vector2 getInsidePerpendicularDir() {
            return Direction.rotate(RoomRef.ClockWise ? 90 : -90);
        }

        public override void onDestroy() {
            base.onDestroy();

            Start?.removeWallRef(this);
            End?.removeWallRef(this);
        }

        public void drawDebug() {

            DebugDrawer.Color = Color.Magenta;
            Vector2 midPoint = Start.Position + Direction * .5f;

            Vector2 dir = getInsidePerpendicularDir();

            DebugDrawer.drawRay(Center, dir * 8);
        }
    }
}
