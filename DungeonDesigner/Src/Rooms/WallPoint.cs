﻿using DungeonDesigner.Engine2D.Renderer;
using DungeonDesigner.Src.Controllers;
using DungeonDesigner.Src.Managers;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Diagnostics;

namespace DungeonDesigner.Src.Rooms {
    public class WallPoint : SpriteMeshRenderObject {

        const float DEFAULT_SCALE = .25f;
        const float TOP_SCALE = .75f;
        
        Color _defaultColor = Color.White;

        public override Vector2 Position {
            get => base.Position;
            set {
                setPosition(value, true);
            }
        }
        Vector2 _lastFixedPosition;

        public List<Wall> Walls = new List<Wall>(2);
        public Room RoomRef;

        HashSet<WallPoint> _siblings;

        public int WallCount { get { return Walls.Count; } }

        public WallPoint(Vector2 position) : base(new string[] { GlobalData.ZONE_RENDERER, GlobalData.ANALYSIS_RENDERER }, "WallPoint", new Vector2(.5f,.5f), GlobalData.WALL_POINT_LAYER) {

            _siblings = new HashSet<WallPoint>();
            Scale = Vector2.One * DEFAULT_SCALE;

            Position = position;

            //fixPosition(Position);
        }

        public void addWallRef(Wall wall) {
            Walls.Add(wall);
        }
        public void removeWallRef(Wall wall) {
            Walls.Remove(wall);
        }

        public void setRoomRef(Room room) {
            RoomRef = room;

            RoomRef.triangulateWallPoints();
        }

        public void addSibling(WallPoint point) {
            _siblings.Add(point);
            point._siblings.Add(this);

        }

        public Room[] getSiblingRooms() {
            Room[] rooms = new Room[_siblings.Count+1];

            rooms[0] = RoomRef;

            int pos = 1;
            foreach(var item in _siblings) {
                rooms[pos] = item.RoomRef;
                pos++;
            }
            return rooms;
        }

        public List<Wall> getSiblingWalls(WallPoint p1, WallPoint p2) {

            List<Wall> walls = new List<Wall>(2);

            foreach(var wall in Walls) {
                if(wall.equals(p1, p2) && !walls.Contains(wall)) {
                    walls.Add(wall);
                }
            }

            foreach(var sib in _siblings) {
                foreach(var wall in sib.Walls) {
                    if(wall.equals(p1, p2) && !walls.Contains(wall)) {
                        walls.Add(wall);
                    }
                }
            }

            return walls;
        }

        public void paint(Color paintColor) {
            if(paintColor == Color.Transparent) {
                color = _defaultColor;
            } else {
                color = paintColor;
            }
        }

        public void paintWalls(Color paintColor, bool paintSiblings) {
            foreach(var wall in Walls) {
                wall.paint(paintColor);
                wall.paintCorners(paintColor);
            }

            if(paintSiblings) {
                foreach(var sib in _siblings) {
                    sib.paintWalls(paintColor, false);
                }
            }

        }

        public void setOnTop(bool top, int des = 0) {
            if(top) {
                Layer = GlobalData.WALL_POINT_TOP_LAYER + des;
                Scale = Vector2.One * TOP_SCALE;
            } else {
                Layer = GlobalData.WALL_POINT_LAYER;
                Scale = Vector2.One * DEFAULT_SCALE;
            }
        }
        public void setWallsOnTop(bool top, int des) {
            foreach(var wall in Walls) {
                wall.setOnTop(top, des);
                wall.setCornersOnTop(top, des);
            }
        }

        public void fixPosition() {

            //setPosition(position, true);

            if(!RoomRef.isValid()) {
                Position = _lastFixedPosition;
                return;
            }
            
            WallPoint exists = RoomSelector.getInstance().getWallCornerAt(Position, getSiblingRooms());
            if(exists != null) {
                addSibling(exists);
            } else {
                Wall wall = RoomSelector.getInstance().getWallAt(Position, getSiblingRooms(), out Vector2 position);
                
                if(wall!= null) {
                    WallPoint p = new WallPoint(position);
                    wall.divideBy(p);
                    p.fixPosition();
                }
            }

            _lastFixedPosition = Position;
        }

        void setPosition(Vector2 position, bool updateSiblings) {
            base.Position = position;

            onPositionChange(updateSiblings);
        }

        void onPositionChange(bool updateSiblings) {
            foreach(var wall in Walls) {
                wall.updateDimension();
            }
            RoomRef?.onEdit();

            if(updateSiblings) {
                foreach(var item in _siblings) {
                    item.setPosition(Position, false);
                }
            }
        }


        public override void onDestroy() {
            base.onDestroy();

            foreach(var wall in Walls) {
                wall.removePoint(this);
            }
        }
    }
}
