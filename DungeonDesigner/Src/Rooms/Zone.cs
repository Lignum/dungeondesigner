﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DungeonDesigner.Src.Controllers;
using DungeonDesigner.Src.Items;
using DungeonDesigner.Src.Path;
using DungeonDesigner.Src.Path.States;
using Microsoft.Xna.Framework;

namespace DungeonDesigner.Src.Rooms {
    public class Zone {

        List<Room> _rooms;
        Dictionary<string, ItemDataBase> _zoneItems;


        public Zone() {

            _rooms = new List<Room>();
            _zoneItems = new Dictionary<string, ItemDataBase>();
        }

        public void addRoom(Room room) {
            _rooms.Add(room);
        }

        public Room getRoomIn(Vector2 point, Room[] ignoredRooms) {

            foreach(var room in _rooms) {
                if(ignoredRooms != null) {
                    if(ignoredRooms.Contains(room)) continue;
                }
                if(room.contains(point)) return room;
            }

            return null;
        }

        public Wall getClosestWall(Vector2 position, Room[] ignoredRooms, out float distance, out Vector2 closestPoint) {

            Wall closestWall = null;
            Wall auxWall;

            closestPoint = Vector2.Zero;
            distance = -1;

            Room targetRoom = getRoomIn(position, ignoredRooms);
            if(targetRoom != null) {
                return targetRoom.getClosestWall(position, out distance, out closestPoint);
            }

            foreach(var room in _rooms) {

                if(ignoredRooms != null) {
                    if(ignoredRooms.Contains(room)) continue;
                }

                auxWall = room.getClosestWall(position, out float dis, out Vector2 point);
                if(auxWall != null && dis >= 0 && (dis < distance || distance == -1)) {
                    distance = dis;
                    closestWall = auxWall;
                    closestPoint = point;
                }
            }

            return closestWall;
        }

        public WallPoint getClosestWallCorner(Vector2 position, Room[] ignoredRooms, out float distance) {

            WallPoint closestCorner = null;
            WallPoint auxCorner;
            
            distance = -1;

            foreach(var room in _rooms) {

                if(ignoredRooms != null) {
                    if(ignoredRooms.Contains(room)) continue;
                }

                auxCorner = room.getClosestWallCorner(position, out float dis);
                if(auxCorner != null && dis >= 0 && (dis < distance || distance == -1)) {
                    distance = dis;
                    closestCorner = auxCorner;
                }
            }

            return closestCorner;

        }

        public Room[] getRooms() {
            return _rooms.ToArray();
        }


        public void addItemData(ItemDataBase itemData) {
            _zoneItems.Add(itemData.ItemName, itemData);
            //allItems.Add(new KeyValuePair<string, ItemDataBase>(itemData.ItemName, itemData));

            //MainWindow.addItemBtn(itemData);
        }
        public ItemDataBase getItemData(string item) {
            return _zoneItems[item];
        }
        public bool tryGetItemData(string item, out ItemDataBase data) {
            if(_zoneItems.TryGetValue(item, out data)) {
                return true;
            }
            return false;
        }
        public Dictionary<string, ItemDataBase> getZoneItems() {
            return _zoneItems;
        }

        public void solveZone() {

            Item start = UniqueItem.findUniqueItem(this, ItemCreator.BASIC_ITEM_START);
            Item end = UniqueItem.findUniqueItem(this, ItemCreator.BASIC_ITEM_END);

            if(start == null || end == null) {
                MessageBox.Show("Place the Start and End points");
                return;
            }

            HashSet<PathPoint> zonePoints = getAllPathPoints();

            PlayerState playerState = new PlayerState(zonePoints, start.PathPoint, end.PathPoint);

            PathSolver.solvePath(playerState, end.PathPoint);
        }

        public HashSet<PathPoint> getAllPathPoints() {
            HashSet<PathPoint> zonePoints = new HashSet<PathPoint>();

            HashSet<PathPoint> roomPoints;
            foreach(var room in _rooms) {
                roomPoints = room.getAllPathPoints();
                foreach(var point in roomPoints) {
                    zonePoints.Add(point);
                }
            }

            return zonePoints;
        }

        public void unLoad() {
            foreach(var item in _rooms) {
                item.Destroy();
            }
        }
    }
}
