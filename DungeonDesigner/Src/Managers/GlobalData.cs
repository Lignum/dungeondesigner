﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Managers {
    public static class GlobalData {

        public static Color CREATING_COLOR = Color.Blue;
        public static Color HOVER_COLOR = Color.Green;
        public static Color SELECTED_COLOR = Color.Yellow;

        //Item Colors
        public static Color ITEM_DEFAULT_COLOR = Color.White;
        public static Color ITEM_CREATING_COLOR = Color.Blue;
        public static Color ITEM_HOVER_COLOR = Color.Green;
        public static Color ITEM_SELECTED_COLOR = Color.Yellow;
        public static Color ITEM_BLOCKED_COLOR = Color.Red;


        //Layers
        public const int WALL_POINT_LAYER = 20;
        public const int WALL_POINT_TOP_LAYER = 200;

        public const int WALL_LAYER = 10;
        public const int WALL_TOP_LAYER = 100;


        public const int ITEMS_LAYER = 25;

        //Renderers
        public const string ZONE_RENDERER = "ZoneRenderer";
        public const string ANALYSIS_RENDERER = "AnalysisRenderer";
        public const string NODE_RENDERER = "NodeRenderer";

    }
}
