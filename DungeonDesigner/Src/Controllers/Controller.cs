﻿using DungeonDesigner.Engine2D;
using Microsoft.Xna.Framework.Input;using Microsoft.Xna.Framework;
using DungeonDesigner.Engine2D.Renderer;
using DungeonDesigner.Src.UI;
using DungeonDesigner.Engine2D.UI;
using System.Diagnostics;
using Microsoft.Xna.Framework.Graphics;
using DungeonDesigner.Windows;
using DungeonDesigner.Src.Path;
using DungeonDesigner.Src.Path.Render;

namespace DungeonDesigner.Src.Controllers {

    public static class Controller {

        public enum Tool { None, PolygonRoom, SquareRoom, CornerSelection, ItemSelector, ItemCreator};

        readonly static CursorItem _cursor = CursorItem.getInstance();
        readonly static RoomSelector _roomSelector = RoomSelector.getInstance();

        static Tool _selectedTool; 

        public static void init() {

            setTool(Tool.None);

        }

        public static void update() {

            if(!UIEventSystem.IsMouseOverProgram) return;
            if(UIEventSystem.IsMouseOverUI) return;



            switch(_selectedTool) {
                case Tool.PolygonRoom:
                    RoomCreator.performCreateWall();
                    break;
                case Tool.SquareRoom:
                    RoomCreator.performCreateRoom();
                    break;
                case Tool.CornerSelection:
                    RoomCreator.performEditRoom();
                    break;
                case Tool.ItemSelector:
                    ItemCreator.performSelectItem();
                    break;
                case Tool.ItemCreator:
                    ItemCreator.performPlaceItem();
                    break;
            }

        }

        public static void setTool(Tool tool) {
            _selectedTool = tool;

            _cursor.resetCenter();
            ItemCreator.clearControllerItems();

            _cursor.setVisible(false);

            _cursor.SnapToCorners = false;
            _cursor.SnapToWalls = false;

            _roomSelector.HoverRooms = false;
            _roomSelector.HoverItems = false;

            switch(_selectedTool) {
                case Tool.PolygonRoom:
                case Tool.SquareRoom:
                    _cursor.setVisible(true);
                    _cursor.setTexture("WallPoint");

                    _cursor.SnapToCorners = true;
                    _cursor.SnapToWalls = true;

                    _roomSelector.HoverRooms = false;
                    break;

                case Tool.CornerSelection:
                    _cursor.setTexture("WallPoint");

                    _cursor.SnapToCorners = true;
                    _cursor.SnapToWalls = true;
                    _roomSelector.HoverRooms = true;
                    break;
                case Tool.ItemSelector:

                    _roomSelector.HoverItems = true;

                    break;                    
                default:
                    break;
            }

            MainWindow.getInstance().onToolChange();
            //PathLineMng.clearPath();
            Debug.log($"New Tool: {_selectedTool}");
        }
        
    }
}
