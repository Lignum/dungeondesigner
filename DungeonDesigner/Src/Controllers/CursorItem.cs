﻿using DungeonDesigner.Engine2D;
using DungeonDesigner.Engine2D.Logic;
using DungeonDesigner.Engine2D.Renderer;
using DungeonDesigner.Engine2D.UI;
using DungeonDesigner.Engine2D.UI.Elements;
using DungeonDesigner.Src.Managers;
using DungeonDesigner.Src.Rooms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace DungeonDesigner.Src.Controllers {
    public class CursorItem : Image, I_Updatable{

        static CursorItem INSTANCE;

        //const float SNAP_CORNER_DISTANCE = 35;
        //const float SNAP_WALL_DISTANCE = 25;

        public Vector2 WorldMousePosition { get; private set; }
        public Vector2 WorldMouseRawPosition {
            get {
                return Input.WorldMousePosition;
            }
        }

        //SNAP PROPERTIES
        bool _snaped;

        bool _snapToWalls;
        bool _snapToCorners;
        public bool SnapToWalls {
            get {
                return _snapToWalls;
            }
            set{
                _snapToWalls = value;
                RoomSelector.getInstance().HoverWalls = _snapToWalls;
            }
        }
        public bool SnapToCorners {
            get {
                return _snapToCorners;
            }
            set {
                _snapToCorners = value;
                RoomSelector.getInstance().HoverCorners = _snapToCorners;
            }
        }
        /*public Wall SnapedWall { get; private set; }
        public WallPoint SnapedCorner { get; private set; }*/

        public Input Input { get; private set; }
        RenderSystem _renderSystem;
        
        private CursorItem(Input input) : base("Cursor", UITexture.Empty, new Vector2(400, 400), Vector2.One * 16) {
            ObjectUpdateMng.addObject(GlobalData.ZONE_RENDERER, this);
            IgnoreMouseHover = true;
            setPivot(Vector2.One * .5f);

            //Layer = 1000;
            Color = Color.Blue;

            Input = input;
            _renderSystem = Input.RenderSystem;
        }

        public static void createInstance(Input input) {
            INSTANCE = new CursorItem(input);
        }

        public static CursorItem getInstance() {
            return INSTANCE;
        }

        public void update() {
            WorldMousePosition = Input.WorldMousePosition;//RenderSystem.MainCamera.screenToWorldPosition(V_Position);

            snapPosition();

            V_Position = _renderSystem.MainCamera.worldToScreenPosition(WorldMousePosition);
        }

        public void lateUpdate() { }

        public void setWorldPosition(Vector2 worldPos) {
            WorldMousePosition = worldPos;
            V_Position = _renderSystem.MainCamera.worldToScreenPosition(WorldMousePosition);
        }

        public void resetCenter() {
            setPivot(new Vector2(.5f,.5f));
        }

        public void setCenterOnBase() {
            setPivot(new Vector2(.5f, 1));
        }

        void snapPosition() {

            if(SnapToCorners) {
                _snaped = snapToWallCorners();
            }

            if(SnapToWalls) {
                if(!_snaped) {
                    snapToWalls();
                }
            }
        }

        void snapToWalls() {

            if(RoomSelector.getInstance().HoveredWall != null) {
                setWorldPosition(RoomSelector.getInstance().HoveredWallPoint);
            }
        }

        bool snapToWallCorners() {

            if(RoomSelector.getInstance().HoveredCorner != null) {
                setWorldPosition(RoomSelector.getInstance().HoveredCorner.Position);
                return true;
            }
            return false;
        }

        public void setVisible(bool visible) {
            Active = visible;
        }

        public override void draw(SpriteBatch _spriteBatch) {

            base.draw(_spriteBatch);
        }
    }
}
