﻿using DungeonDesigner.Src.Rooms;
using DungeonDesigner.Engine2D;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DungeonDesigner.Engine2D.Renderer;
using DungeonDesigner.Src.Managers;

namespace DungeonDesigner.Src.Controllers {
    public static class RoomCreator {

        readonly static CursorItem _cursor = CursorItem.getInstance();
        readonly static RoomSelector _roomSelector = RoomSelector.getInstance();

        static Wall _createdWall;

        static WallPoint _firstSectionPoint;
        static WallPoint _createdWallPoint;
        static WallPoint _nextWallPoint;

        static List<WallPoint> _createdWallPoints = new List<WallPoint>();
        static List<Wall> _createdWalls = new List<Wall>();

        static Room _createdRoom;

        static WallPoint _selectedWallPoint;         

        public static void performCreateRoom() {

            if(_cursor.Input.isMouseButtonPressed(0)) {
                if(_createdRoom == null) {
                    _createdRoom = new Room(Project.getInstance().getZone(), _cursor.WorldMousePosition);
                    _createdRoom.paint(GlobalData.CREATING_COLOR);
                    _createdRoom.setOnTop(true, 1);
                    _roomSelector.setIgnoredRoom(_createdRoom);
                } else {
                    if(_createdRoom.Size.X < 10 || _createdRoom.Size.Y < 10) {
                        _createdRoom.Destroy();
                    } else {
                        _createdRoom.paint(Color.Transparent);
                        _createdRoom.setOnTop(false);
                        _createdRoom.fixCornerPositions();
                    }
                    _createdRoom = null;
                    _roomSelector.setIgnoredRoom(null);
                }
            }
            
            if(_createdRoom != null) {
                _createdRoom.setPoint(1, new Vector2(_createdRoom.getPoint(0).Position.X, _cursor.WorldMousePosition.Y));
                _createdRoom.setPoint(2, _cursor.WorldMousePosition);
                _createdRoom.setPoint(3, new Vector2(_cursor.WorldMousePosition.X, _createdRoom.getPoint(0).Position.Y));
            }
           
        }


        public static void performCreateWall() {
            
            if(_cursor.Input.isMouseButtonPressed(0)) {
                bool endCreation = false;

                if(_createdWall == null) {
                    _createdWallPoints.Clear();
                    _createdWalls.Clear();

                    _createdWallPoint = new WallPoint(_cursor.WorldMousePosition);
                    //_createdWallPoint.fixPosition();
                    _firstSectionPoint = _createdWallPoint;

                    _createdWallPoints.Add(_firstSectionPoint);
                } else {
                    if(_nextWallPoint.Position == _firstSectionPoint.Position) {

                        _createdWall.setEndPoint(_firstSectionPoint);

                        _createdRoom = new Room(Project.getInstance().getZone(), _createdWallPoints/*, _createdWalls*/);

                        if(_createdRoom.Size.X < 10 || _createdRoom.Size.Y < 10) {
                            _createdRoom.Destroy();
                        } else {
                            _createdRoom.fixCornerPositions();
                        }

                        _createdRoom = null;
                        endCreation = true;

                        _nextWallPoint.Destroy();
                    } else {
                        _createdWallPoints.Add(_nextWallPoint);
                    }

                    //_nextWallPoint.fixPosition();
                    _createdWallPoint = _nextWallPoint;
                }

                if(!endCreation) {
                    _nextWallPoint = new WallPoint(_cursor.WorldMousePosition);
                    _createdWall = new Wall(_createdWallPoint, _nextWallPoint);

                    _createdWall.paint(GlobalData.CREATING_COLOR);
                    _createdWall.paintCorners(GlobalData.CREATING_COLOR);

                    _createdWall.setOnTop(true, 1);
                    _createdWall.setCornersOnTop(true, 1);

                    _createdWalls.Add(_createdWall);

                } else {
                    _nextWallPoint = null;
                    _createdWall = null;
                    _createdWallPoint = null;
                    _firstSectionPoint = null;
                }
            }
                        

            if(_nextWallPoint != null) {

                if(Vector2.Distance(_cursor.WorldMousePosition, _firstSectionPoint.Position) < 20) {
                    _cursor.setWorldPosition(_firstSectionPoint.Position);
                }

                _nextWallPoint.Position = _cursor.WorldMousePosition;
            }
        }



        public static void performEditRoom() {

            if(_selectedWallPoint == null) {

                if(_roomSelector.HoveredCorner != null) {
                    _cursor.setVisible(true);

                    if(_cursor.Input.isMouseButtonPressed(0)) {
                        _selectedWallPoint = _roomSelector.HoveredCorner;
                        _roomSelector.setIgnoredRooms(_selectedWallPoint.getSiblingRooms());
                    }
                } else if(_roomSelector.HoveredWall != null) {
                    _cursor.setVisible(true);

                    if(_cursor.Input.isMouseButtonPressed(0)) {
                        //_selectedWallPoint = new WallPoint(_cursor.WorldMousePosition);
                        _selectedWallPoint = _roomSelector.HoveredWall.divideAt(_cursor.WorldMousePosition);
                        _roomSelector.setIgnoredRooms(_selectedWallPoint.getSiblingRooms());
                    }
                } else {
                    _cursor.setVisible(true);
                }
            } else {
                _selectedWallPoint.Position = _cursor.WorldMousePosition;

                if(_cursor.Input.isMouseButtonPressed(0)) {
                    _selectedWallPoint.fixPosition();
                    _selectedWallPoint = null;
                    _roomSelector.setIgnoredRoom(null);
                }
            }


        }

    }
}
