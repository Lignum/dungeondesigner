﻿using DungeonDesigner.Engine2D.Logic;
using DungeonDesigner.Src.Managers;
using DungeonDesigner.Src.Rooms;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DungeonDesigner.Src.Items;

namespace DungeonDesigner.Src.Controllers {
    public class RoomSelector: I_Updatable {

        static RoomSelector INSTANCE;
        static readonly CursorItem _cursor = CursorItem.getInstance();

        public const float HOVER_CORNER_DISTANCE = 20;
        public const float HOVER_WALL_DISTANCE = 10;
        public const float HOVER_ITEM_DISTANCE = 20;

        public Room HoveredRoom { get; private set; }
        Room _preHoveredRoom;

        public Wall HoveredWall { get; private set; }
        Wall _preHoveredWall;
        public Vector2 HoveredWallPoint;

        public WallPoint HoveredCorner { get; private set; }
        WallPoint _preHoveredCorner;

        Room _hoveredItemRoom;
        public Item HoveredItem { get; private set; }
        Item _preHoveredItem;

        Room[] _ignoredRooms;

        bool _hover;

        public bool HoverRooms;
        public bool HoverWalls;
        public bool HoverCorners;
        public bool HoverItems;

        private RoomSelector() {
            ObjectUpdateMng.addObject(GlobalData.ZONE_RENDERER, this);
        }

        public static RoomSelector getInstance() {
            if(INSTANCE == null) {
                INSTANCE = new RoomSelector();
            }
            return INSTANCE;
        }

        public void update() {

            _hover = false;

            if(HoverCorners)
                _hover = hoverWallCorner(_cursor.WorldMouseRawPosition, _ignoredRooms);

            if(HoverWalls && !_hover) {
                _hover = hoverWall(_cursor.WorldMouseRawPosition, _ignoredRooms);
            } else {
                //clearHoveredWall();
            }

            if(HoverRooms && !_hover) {
                _hover = hoverRoom(_cursor.WorldMouseRawPosition);
            } else {
                //clearHoveredRoom();
            }

            if(HoverItems) {
                hoverItem(_cursor.WorldMouseRawPosition);
            }
        }

        public void lateUpdate() { }

        public void setIgnoredRoom(Room ignored) {
            _ignoredRooms = new Room[1] { ignored };
        }
        public void setIgnoredRooms(Room[] ignored) {
            _ignoredRooms = ignored;
        }

        public Room getRoomAt(Vector2 position) {
            return Project.getInstance().getRoomIn(position, _ignoredRooms);
        }

        private bool hoverRoom(Vector2 position) {
            HoveredRoom = getRoomAt(position);

            if(HoveredRoom != null) {
                HoveredRoom.paint(GlobalData.HOVER_COLOR);
                HoveredRoom.setOnTop(true);
            }
            if(_preHoveredRoom != null && _preHoveredRoom != HoveredRoom) {
                _preHoveredRoom.paint(Color.Transparent);
                _preHoveredRoom.setOnTop(false);
            }

            _preHoveredRoom = HoveredRoom;

            return HoveredRoom != null;
        }

        public void clearHoveredRoom() {
            if(HoveredRoom != null) {
                HoveredRoom.paint(Color.Transparent);
                HoveredRoom.setOnTop(false);
                HoveredRoom = null;
                _preHoveredRoom = null;
            }
        }
        public void clearHoveredWall() {
            if(HoveredWall != null) {
                HoveredWall.paint(Color.Transparent);
                HoveredWall.setOnTop(false);

                HoveredWall.paintCorners(Color.Transparent);
                HoveredWall.setCornersOnTop(false);

                HoveredWall = null;
                _preHoveredWall = null;
            }
        }
        public void clearHoveredCorner() {
            if(HoveredCorner != null) {
                HoveredCorner.paint(Color.Transparent);
                HoveredCorner.setOnTop(false);

                HoveredCorner.paintWalls(Color.Transparent, true);
                HoveredCorner.setWallsOnTop(false, 0);

                HoveredCorner = null;
                _preHoveredCorner = null;
            }
        }

        public Wall getWallAt(Vector2 position, float maxDistance = 1) {
            return getWallAt(position, _ignoredRooms, out Vector2 c, maxDistance);
        }
        public Wall getWallAt(Vector2 position, Room[] ignoreRooms, out Vector2 closestPoint, float maxDistance = 1) {
            Wall wall = Project.getInstance().getClossestWall(position, ignoreRooms, out float distance, out closestPoint);

            if((distance <= HOVER_WALL_DISTANCE * maxDistance || maxDistance == -1) && distance >= 0) {
                return wall;
            }
            return null;
        }

        public bool hoverWall(Vector2 position, Room[] ignoreRooms) {
            HoveredWall = getWallAt(position, ignoreRooms, out HoveredWallPoint);


            if(HoveredWall != null) {
                if(HoveredWall != _preHoveredWall) {

                    clearHoveredRoom();

                    HoveredWall.paint(GlobalData.HOVER_COLOR);
                    HoveredWall.setOnTop(true);

                    HoveredWall.paintCorners(GlobalData.HOVER_COLOR);
                    HoveredWall.setCornersOnTop(true);
                }
            }

            if(_preHoveredWall != null && _preHoveredWall != HoveredWall) {

                _preHoveredWall.paint(Color.Transparent);
                _preHoveredWall.setOnTop(false);

                _preHoveredWall.paintCorners(Color.Transparent);
                _preHoveredWall.setCornersOnTop(false);
            }

            _preHoveredWall = HoveredWall;            

            return HoveredWall != null;
        }


        public WallPoint getWallCornerAt(Vector2 position, Room[] ignoreRooms) {
            WallPoint corner = Project.getInstance().getClossestWallCorner(position, ignoreRooms, out float distance);

            if(distance <= HOVER_CORNER_DISTANCE && distance >= 0) {
                return corner;
            }
            return null;
        }

        public bool hoverWallCorner(Vector2 position, Room[] ignoreRooms) {
            HoveredCorner = getWallCornerAt(position, ignoreRooms);

            if(HoveredCorner != null) {

                if(HoveredCorner != _preHoveredCorner) {

                    clearHoveredWall();
                    clearHoveredRoom();

                    HoveredCorner.paintWalls(GlobalData.HOVER_COLOR, true);
                    HoveredCorner.setWallsOnTop(true, 0);
                }
            }

            if(_preHoveredCorner != null && _preHoveredCorner != HoveredCorner) {

                _preHoveredCorner.paintWalls(Color.Transparent, true);
                _preHoveredCorner.setWallsOnTop(false, 0);
            }

            _preHoveredCorner = HoveredCorner;

            return HoveredCorner != null;
        }

        public bool hoverItem(Vector2 position) {
            if(HoveredRoom != null) {
                _hoveredItemRoom = HoveredRoom;
            } else {
                _hoveredItemRoom = getRoomAt(position);
            }

            if(_preHoveredItem != null) {
                _preHoveredItem.paint(Color.Transparent);
            }

            if(_hoveredItemRoom != null) {

                HoveredItem = _hoveredItemRoom.getItemAt(position);

                if(HoveredItem != null) {

                    if(Vector2.Distance(position, HoveredItem.Center) > HOVER_ITEM_DISTANCE) {
                        HoveredItem = null;
                    }
                }

                if(HoveredItem != null) {
                    HoveredItem.paint(GlobalData.ITEM_HOVER_COLOR);
                }


                _preHoveredItem = HoveredItem;

            }
            return HoveredItem != null;
        }
    }
}
