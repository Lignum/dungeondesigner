﻿using DungeonDesigner.Engine2D;
using DungeonDesigner.Engine2D.Logic;
using DungeonDesigner.Engine2D.Renderer;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Controllers {
    public class CameraController: I_Updatable {

        Camera2D _camera;

        Vector2 _mousePos;
        Vector2 _lastMousePos = Vector2.Zero;
        Vector2 _moveDirecton;

        const float MAX_ZOOM = 1f;
        const float MIN_ZOOM = .3f;

        Input _input;

        public CameraController(string updateName, Camera2D camera, Input input) {
            _camera = camera;
            _input = input;

            ObjectUpdateMng.addObject(updateName, this);
        }

        

        public void update() {


            if(_input.isMouseButtonPressed(2)) {
                _lastMousePos = Vector2.Zero;
            }

            if(_input.isMouseButtonHolded(2)) {
                _mousePos = _camera.screenToWorldPosition(_input.MousePosition);
                if(_lastMousePos != Vector2.Zero) {
                    _moveDirecton = _mousePos - _lastMousePos;
                    _moveDirecton.Y *= -1;
                    _camera.Position += _moveDirecton;
                }
            }
            
            _camera.Zoom -= _input.ScrollWheelValue * Time.DeltaTime;
            
            if(_camera.Zoom > MAX_ZOOM) {
                _camera.Zoom = MAX_ZOOM;
            }
            if(_camera.Zoom < MIN_ZOOM) {
                _camera.Zoom = MIN_ZOOM;
            }
        }

        public void lateUpdate() {

            if(_input.isMouseButtonHolded(2)) {
                _lastMousePos = _camera.screenToWorldPosition(_input.MousePosition);
            }
        }
    }
}
