﻿using DungeonDesigner.Engine2D;
using DungeonDesigner.Extensions;
using DungeonDesigner.Src.Items;
using DungeonDesigner.Src.Rooms;
using DungeonDesigner.Windows;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Controllers {
    public static class ItemCreator {

        const string ITEM_DOOR_IMAGE = "Items/ItemDoor";
        const string ITEM_START_IMAGE = "Items/ItemStart";
        const string ITEM_END_IMAGE = "Items/ItemEnd";

        public const string BASIC_ITEM_DOOR = "Door";
        public const string BASIC_ITEM_START = "Start";
        public const string BASIC_ITEM_END = "End";

        static Dictionary<string, ItemDataBase> _basicItems;

        readonly static CursorItem _cursor = CursorItem.getInstance();
        readonly static RoomSelector _roomSelector = RoomSelector.getInstance();

        static Item _createdItem;
        static ItemDataBase _creationItemData;
        static Item _movedItem;
        static ItemDataBase _movedItemData;
        static Item _selectedItem;
        static Item _preSelectedItem;
        //static ItemDataBase _movedItemData;

        public static void init() {

            //allItems = new BindingList<KeyValuePair<string, ItemDataBase>>();
            //allItems.Add(new KeyValuePair<string, ItemDataBase>("-None-", null));
            
            loadBasicItems();
        }

        public static void onZoneChange(Zone selectedZone) {
            ItemControl.getInstance().initComboBlockData(selectedZone.getZoneItems());
            MainWindow.setItemBtns(selectedZone.getZoneItems());
        }

        public static void setCreationItem(string item) {

            _creationItemData = getItemData(item);

            _createdItem = createNewItemInstance(_creationItemData);

            setMovedItem(_createdItem);
            setSelectedItem(null);
        }

        static void setSelectedItem(Item item) {

            _selectedItem?.onSelected(false);
            _preSelectedItem = _selectedItem;

            _selectedItem = item;
            _selectedItem?.onSelected(true);

            ItemControl.getInstance().setItem(_selectedItem);
        }


        static void setMovedItem(Item item) {
            _movedItem = item;
            _movedItemData = item.getItemData();
                        
            _cursor.SnapToWalls = _movedItemData.PlaceType == ItemPlaceType.Wall;
        }

        static void clearMovedItem() {
            _movedItem = null;
            _movedItemData = null;

            _cursor.SnapToWalls = false;
        }
        static void clearCreatedItem() {
            _createdItem?.Destroy();
            _createdItem = null;
        }

        public static void clearControllerItems() {
            clearMovedItem();
            clearCreatedItem();
            setSelectedItem(null);
        }



        public static void performPlaceItem() {
            if(placeItem()) {
                setSelectedItem(null);
                _createdItem = createNewItemInstance(_creationItemData);
                setMovedItem(_createdItem);
            }
        }

        public static void performSelectItem() {

            if(_movedItem == null) {
                if(_roomSelector.HoveredItem != null) {
                    if(_cursor.Input.isMouseButtonPressed(0)) {
                        setSelectedItem(_roomSelector.HoveredItem);
                    } else if(_cursor.Input.isMouseButtonDraging(0)) {
                        setMovedItem(_roomSelector.HoveredItem);
                    }
                }

                if(_selectedItem != null) {
                    if(_cursor.Input.isKeyDown(Microsoft.Xna.Framework.Input.Keys.Delete)) {
                        _selectedItem.Destroy();
                        setSelectedItem(null);
                    }
                }
            } else{
                moveItem();
            } 
        }

        static bool placeItem() {
            _movedItem.BuildingPosition = _cursor.WorldMousePosition;

            if(_movedItem.getItemData().PlaceType == ItemPlaceType.Wall) {
                _movedItem.setItemOnWall(_roomSelector.HoveredWall);
            }

            _movedItem.onMoveState();
            //_selectedItem.checkState(_selectedItem.getItemData().PlaceType);

            if(_cursor.Input.isMouseButtonReleased(0)) {
                //if(_movedItem.CanBePlaced) {
                if(_movedItem.fixPosition(RoomSelector.getInstance().getRoomAt(_cursor.WorldMouseRawPosition))) {
                    setSelectedItem(_movedItem);
                    clearMovedItem();
                    return true;
                }
                //}
            }
            return false;
        }

        static bool moveItem() {
            _movedItem.BuildingPosition = _cursor.WorldMousePosition;

            if(_movedItem.getItemData().PlaceType == ItemPlaceType.Wall) {
                _movedItem.setItemOnWall(_roomSelector.HoveredWall);
            }

            _movedItem.onMoveState();
            //_selectedItem.checkState(_selectedItem.getItemData().PlaceType);

            if(_cursor.Input.isMouseButtonReleased(0)) {
                //if(_movedItem.CanBePlaced) {
                _movedItem.fixPosition(RoomSelector.getInstance().getRoomAt(_cursor.WorldMouseRawPosition));
                setSelectedItem(_movedItem);
                clearMovedItem();
                return true;
                //}
            }
            return false;
        }

        public static Item createNewItemInstance(string itemName) {
            return createNewItemInstance(getItemData(itemName));
        }
        static Item createNewItemInstance(ItemDataBase itemData) {
            return itemData.instantiate(_cursor.WorldMousePosition);
        }

        static void loadBasicItems() {
            _basicItems = new Dictionary<string, ItemDataBase> {
                { BASIC_ITEM_DOOR, new ItemData<ConnectionItem>(BASIC_ITEM_DOOR, ITEM_DOOR_IMAGE, ItemPlaceType.Wall, InteractionType.Permanent, Color.White) },
                { BASIC_ITEM_START, new ItemData<UniqueItem>(BASIC_ITEM_START, ITEM_START_IMAGE, ItemPlaceType.Floor, InteractionType.None, Color.White) },
                { BASIC_ITEM_END, new ItemData<UniqueItem>(BASIC_ITEM_END, ITEM_END_IMAGE, ItemPlaceType.Floor, InteractionType.Pick, Color.White) }
            };
        }
        

        public static void addZoneItem(ItemDataBase itemData) {
            Project.getInstance().getZone().addItemData(itemData);

            ItemControl.getInstance().addComboBlockData(itemData);
            MainWindow.addItemBtn(itemData);
        }

        public static ItemDataBase getItemData(string name) {
            if(_basicItems.TryGetValue(name, out ItemDataBase data)) {
                return data;
            }
            if(Project.getInstance().getZone().tryGetItemData(name, out data)) {
                return data;
            }

            return null;
        }

    }
}
