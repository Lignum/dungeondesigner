﻿using DungeonDesigner.Engine2D.UI;
using DungeonDesigner.Engine2D.UI.Elements;
using DungeonDesigner.Src.Controllers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.UI {
    public static class UISystem {

        const string CANVAS_DIR = "/UI/Canvas";
        const string BASE_CANVAS = "BaseCanvas.json";

        const string SIDE_BAR_DIR = "SideBar/";

        static ContentManager _content;

        static Canvas _baseCanvas;

        public static void Init(ContentManager content) {
            _content = content;

            _baseCanvas = UIRenderer.loadCanvas(_content, CANVAS_DIR, BASE_CANVAS);

            //_baseCanvas.insertChild(0, CursorItem.getInstance());

           // Button btnLines = _baseCanvas.findElement<Button>("btnLines");
            /*Button btnPolygonRoom = _baseCanvas.findDirectPathElement<Button>($"{SIDE_BAR_DIR}btnPolygonRoom");
            btnPolygonRoom.onClick += () => Controller.setTool(Controller.Tool.PolygonRoom);

            Button btnSquareRoom = _baseCanvas.findDirectPathElement<Button>($"{SIDE_BAR_DIR}btnSquareRoom");
            btnSquareRoom.onClick += () => Controller.setTool(Controller.Tool.SquareRoom);

            Button btnSelectCorner = _baseCanvas.findDirectPathElement<Button>($"{SIDE_BAR_DIR}btnSelectCorner");
            btnSelectCorner.onClick += () => Controller.setTool(Controller.Tool.CornerSelection);*/

        }
        
    }

}
