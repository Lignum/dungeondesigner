﻿using DungeonDesigner.Engine2D;
using DungeonDesigner.Engine2D.Logic;
using DungeonDesigner.Engine2D.Renderer;
using DungeonDesigner.Src.Managers;
using DungeonDesigner.Src.Path.States;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Path.Render {
    public class GraphicPathNode : SpriteMeshRenderObject, I_Updatable {

        public static Vector2 NODE_SEPARATION { get; private set; } = new Vector2(40, 50);
        
        LogicPathNode _logicNode;

        List<GraphicPathNode> _parents;
        GraphicPathNode[] _childs;

        Input _input = Input.GetInput(GlobalData.NODE_RENDERER);

        bool isHover;
        bool isChildHover;

        List<Arrow> _parentArrows;
        List<Arrow> _childArrows;

        public GraphicPathNode(LogicPathNode logicNode) : base(GlobalData.NODE_RENDERER, "WallPoint", new Vector2(.5f,.5f), 0) {
            GraphicPath.addNode(this);

            _logicNode = logicNode;
            ObjectUpdateMng.addObject(GlobalData.NODE_RENDERER, this);

            _parentArrows = new List<Arrow>();
            _childArrows = new List<Arrow>();
        }

        public void calculateFamily(List<GraphicPathNode> allNodes) {
            List<LogicPathNode> logicParent = _logicNode.Parents;

            List<LogicPathNode> logicChilds = _logicNode.Childs;
            List<GraphicPathNode> childs = new List<GraphicPathNode>();

            _parents = new List<GraphicPathNode>();

            if(_logicNode.Level == 0) Debug.log(_logicNode.ChildCount);

            foreach(var item in allNodes) {
                if(logicParent.Contains(item._logicNode)) {
                    _parents.Add(item);
                }

                if(logicChilds.Contains(item._logicNode)) {
                    childs.Add(item);
                }
            }

            _childs = childs.ToArray();

            
            //createArrows();
        }

        public void createArrows() {

            Arrow created;
            foreach(var child in _childs) {
                created = new Arrow(Position, child.Position);
                _childArrows.Add(created);
                child.addParentArrow(created);

                created.Visible = Visible && child.Visible;
            }
            
        }

        void addParentArrow(Arrow arrow) {
            _parentArrows.Add(arrow);
        }

        public int getLevel() {
            return _logicNode.Level;
        }

        public void setPosition(Vector2 pos) {
            Position = pos * NODE_SEPARATION;
            
            /*Debug.log($"{Position} -> {_childs.Length}");

            for(int i = 0; i < _childs.Length; i++) {
                _childArrows[i].setPoints(Position, _childArrows[i].Position);
            }

            foreach(var pArrow in _parentArrows) {
                pArrow.setPoints(_parent.Position, Position);
            }*/
        }

        public void update() {

            if(!Visible) return;
            
            if(Vector2.Distance(Position, _input.WorldMousePosition) < 10) {
                if(!isHover) {
                    onMouseHoverEnter();
                    isHover = true;
                }
            } else if(isHover) {
                onMouseHoverExit();
                isHover = false;
            }

        }

        public void onMouseHoverEnter() {
            color = Color.Red;
            PathLineMng.drawPathToNode(_logicNode);

            setParentHover(true);
        }

        void setParentHover(bool childHover) {
            if(_parents.Count > 0) {
                _parents[0].setParentHover(childHover);

                _parents[0].isChildHover = childHover;

                if(childHover) {
                    _parents[0].color = Color.Blue;
                } else {
                    _parents[0].color = Color.White;
                }
            }
        }

        public void onMouseHoverExit() {
            color = Color.White;

            setParentHover(false);
        }

        public void lateUpdate() {
            //throw new NotImplementedException();
        }

        public override void onDestroy() {
            base.onDestroy();

            ObjectUpdateMng.removeObject(GlobalData.NODE_RENDERER, this);

            foreach(var item in _childArrows) {
                item.Destroy();
            }

            _childArrows = null;
        }
    }
}
