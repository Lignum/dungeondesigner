﻿using DungeonDesigner.Src.Managers;
using DungeonDesigner.Src.Path.States;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Path.Render {
    public static class PathLineMng {

        static PathLine _pathLine = new PathLine(GlobalData.ANALYSIS_RENDERER, Color.Yellow);

        public static void drawPathToNode(LogicPathNode endNode) {
            LogicPathNode head = endNode.getHead();

            List<LogicPathNode> path = endNode.getPath();

            _pathLine.Active = true;
            _pathLine.setPoints(path);

        }

        public static void clearPath() {
            _pathLine.Active = false;
        }
    }
}
