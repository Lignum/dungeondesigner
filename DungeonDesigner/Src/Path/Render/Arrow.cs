﻿using DungeonDesigner.Engine2D.Renderer;
using DungeonDesigner.Src.Managers;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Path.Render {
    public class Arrow : SpriteMeshRenderObject {

        const int ARROW_WIDTH = 10;

        SpriteMeshRenderObject _head;
        Vector2 _start, _end;

        public override bool Visible {
            get => base.Visible;
            set {
                base.Visible = value;
                _head.Visible = value;
            }
        }

        public Arrow() : this(Vector2.Zero, Vector2.Zero) {}

        public Arrow(Vector2 start, Vector2 end) : base(GlobalData.NODE_RENDERER, "Path/ArrowBody", new Vector2(.5f, 0), 0) {

            _head = new SpriteMeshRenderObject(GlobalData.NODE_RENDERER, "Path/ArrowHead", new Vector2(.5f, .5f), 1);
            _head.Scale = Vector2.One / 2f;
            //Scale.X = ARROW_WIDTH;

            //color = Color.Red;

            setPoints(start, end);
        }

        public void setStart(Vector2 start) {
            setPoints(start, _end);
        }
        public void setEnd(Vector2 end) {
            setPoints(_start, end);
        }

        public void setPoints(Vector2 start, Vector2 end) {

            Up = end - start;

            _start = start;
            _end = end - Up * 15;

            Position = _start;

            Scale.Y = Vector2.Distance(Position, _end) / 8;

            _head.Position = _end;
            _head.Up = Up;
        }


        public override void onDestroy() {
            _head.Destroy();
        }
    }
}
