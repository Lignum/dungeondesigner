﻿using DungeonDesigner.Engine2D.Renderer;
using DungeonDesigner.Src.Path.States;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Path.Render {
    public class PathLine : SimpleLineRenderer {

        public PathLine(string rendererName, Color color) : base(rendererName, color, true) {

        }


        public void setPoints(List<LogicPathNode> nodes) {

            List<Vector3> positions = new List<Vector3>();
            foreach(var item in nodes) {
                positions.Add(new Vector3(item.State.CurrentPoint.Parent.Center, 0));
            }

            base.setPoints(positions.ToArray());
        }

    }
}
