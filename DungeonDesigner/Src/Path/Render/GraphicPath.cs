﻿using DungeonDesigner.Extensions;
using DungeonDesigner.Src.Path.States;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Path.Render {
    public static class GraphicPath {

        static List<GraphicPathNode> _nodes = new List<GraphicPathNode>();
        public static RectF Rect = new RectF();

        static int[] _levelSiblings;

        public static void addNode(GraphicPathNode node) {
            _nodes.Add(node);
            
        }

        public static void clear() {
            foreach(var item in _nodes) {
                item.Destroy();
            }

            _nodes.Clear();
        }
        

        public static void reorderNodes(LogicPath path) {
            //_levelSiblings = new int[path.TotalLevels + 1];

            _levelSiblings = new int[200];

            int level;
            int sibling;

            int maxSiblings = 0;
            int maxLevel = 0;

            List<LogicPathNode> ends = path.EndNodes;
            List<LogicPathNode> endPath;

            GraphicPathNode gNode;

            foreach(var item in _nodes) {

                item.Visible = false;

                item.calculateFamily(_nodes);

                //6item.setPosition(new Vector2(10, 10));
                /*level = item.getLevel();
                
                
                sibling = _levelSiblings[level]++;

                //Debug.log($"{level} -- {sibling}");

                item.setPosition(new Vector2(sibling, -level));

                if(sibling > maxSiblings) maxSiblings = sibling;
                if(level > maxLevel) maxLevel = level;*/
            }


            foreach(var end in ends) {

                endPath = end.getPath();
                foreach(var n in endPath) {

                    gNode = n.GraphicNode;
                    if(gNode.Visible) continue;

                    gNode.Visible = true;

                    level = gNode.getLevel();
                    sibling = _levelSiblings[level]++;

                    gNode.setPosition(new Vector2(sibling, -level));

                    if(sibling > maxSiblings) maxSiblings = sibling;
                    if(level > maxLevel) maxLevel = level;
                }

            }
            
            /*foreach(var item in _nodes) {

                item.calculateFamily(_nodes);

                level = item.getLevel();
                
                
                sibling = _levelSiblings[level]++;

                //Debug.log($"{level} -- {sibling}");

                item.setPosition(new Vector2(sibling, -level));

                if(sibling > maxSiblings) maxSiblings = sibling;
                if(level > maxLevel) maxLevel = level;
            }*/

            foreach(var item in _nodes) {
                item.createArrows();
            }

            calculateRect(maxSiblings, maxLevel);
            
        }

        static void calculateRect(int siblings, int levels) {

            Vector2 size = new Vector2(siblings + 2, levels + 2) * GraphicPathNode.NODE_SEPARATION;

            //Rect.Center = size / _nodes.Count;
            Rect.Size = size;

            Rect.Center = -((size / 2) - GraphicPathNode.NODE_SEPARATION);
        }

    }
}
