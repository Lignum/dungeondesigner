﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Path.States {
    public class LogicPath {
        LogicPathNode _startNode;

        public List<LogicPathNode> EndNodes { get; private set; }

        public int EndsCount {
            get {
                return EndNodes.Count;
            }
        }

        public int TotalLevels {
            get; private set;
        }

        public LogicPath(PlayerState playerState) {
            _startNode = new LogicPathNode(playerState, null);
        }
        
        public void analize(PathPoint target) {
            LogicPathNode currentNode = _startNode;
            LogicPathNode nextNode;

            Queue<LogicPathNode> pending = new Queue<LogicPathNode>();
            pending.Enqueue(_startNode);
            
            int totalEnds = 0;

            while(pending.Count > 0) {
                currentNode = pending.Dequeue();

                do {
                    nextNode = currentNode.createChild(target);
                    Debug.log(nextNode);
                    if(nextNode != null) {
                        pending.Enqueue(nextNode);
                    }

                } while(nextNode != null);
            }

            /*while(currentNode != null) {
                
                do {
                    nextNode = currentNode.createChild(target);
                    if(nextNode == null) {
                        if(currentNode.ChildCount == 0) {
                            totalEnds++;
                        }
                        if(currentNode.Parents.Count > 0) {
                            currentNode = currentNode.Parents[0];
                        } else {
                            currentNode = null;
                        }
                    } else if(nextNode.State.CurrentPoint == target) {
                        nextNode = null;
                        totalEnds++;
                    }

                } while(nextNode == null && currentNode != null);

                currentNode = nextNode;

                if(totalEnds == 25) {
                    Debug.log("MAX TOTAL ENDS");
                    break;
                }
            }*/

            EndNodes = _startNode.getEnds(new HashSet<LogicPathNode>(), target).ToList();
            TotalLevels = _startNode.getMaxEndLevel(target);
            Debug.log($"END ALL ITERATIONS, Total: {EndsCount} - > {totalEnds}");
        }

        public void destroy() {

        }

        public void drawDebugPath() {
            EndNodes[0].drawPathToParent();
        }
    }
}
