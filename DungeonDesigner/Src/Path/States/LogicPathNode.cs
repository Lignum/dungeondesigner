﻿using DungeonDesigner.Src.Path.Render;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Path.States {
    public class LogicPathNode {

        public PlayerState State { get; private set; }

        public List<LogicPathNode> Parents { get; private set; }

        public List<LogicPathNode> Childs { get; private set; }
        public int ChildCount {
            get {
                return Childs.Count;
            }
        }

        public int Level { get; private set; }

        public GraphicPathNode GraphicNode { get; private set; }

        public LogicPathNode(PlayerState state, LogicPathNode parent) {

            GraphicNode = new GraphicPathNode(this);

            State = state;
            Parents = new List<LogicPathNode>();
            if(parent != null) {
                Parents.Add(parent);
            }

            Childs = new List<LogicPathNode>();

            calculateLevel();
        }

        void calculateLevel() {
            Level = 0;

            if(Parents.Count > 0) {
                int minLevel = Parents[0].Level;

                foreach(var item in Parents) {
                    if(item.Level < minLevel) {
                        minLevel = item.Level;
                    }
                }

                Level = minLevel + 1;
            }

            //Debug.log(Level);
        }

        public LogicPathNode createChild(PathPoint target) {
            PlayerState nextState;
                
            LogicPathNode child = null;

            do {
                nextState = State.getNext(target);
                if(nextState != null) {

                    child = getHead().findDuplicated(this, nextState, new HashSet<LogicPathNode>());

                    if(child != null) {
                        Childs.Add(child);
                    }
                }
            } while(child != null && nextState != null);

            if(nextState != null) {
                child = new LogicPathNode(nextState, this);
                Childs.Add(child);
                return child;
            }

            return null;
                
        }
        
        

        public int getEndsCount() {

            int count = 0;

            foreach(var child in Childs) {
                if(child.Childs.Count == 0) {
                    count++;
                } else {
                    count += child.getEndsCount();
                }
            }

            return count;
        }

        public LogicPathNode getHead() {
            if(Parents.Count != 0) {
                return Parents[0].getHead();
            } else {
                return this;
            }
        }

        public LogicPathNode findDuplicated(LogicPathNode original, PlayerState state, HashSet<LogicPathNode> previous) {

            if(this.State == state && this != original) {             
                return this;
            } else {
                LogicPathNode dup;
                foreach(var child in Childs) {
                    if(previous.Contains(child)) continue;
                    previous.Add(child);

                    dup = child.findDuplicated(original, state, previous);
                    
                    if(dup != null && dup != original) return dup;
                }
            }
            
            return null;
        }

        public HashSet<LogicPathNode> getEnds(HashSet<LogicPathNode> check, PathPoint target) {
            HashSet<LogicPathNode> ends = new HashSet<LogicPathNode>();

            HashSet<LogicPathNode> childEnds;

            foreach(var child in Childs) {

                if(check.Contains(child)) continue;
                check.Add(child);

                if(child.Childs.Count == 0) {
                    ends.Add(child);
                } else {

                    if(child.State.CurrentPoint == target) {
                        ends.Add(child);
                    }

                    childEnds = child.getEnds(check, target);

                    foreach(var item in childEnds) {
                        ends.Add(item);
                    }
                    //ends.AddRange(child.getEnds());
                }
            }

            return ends;//.Distinct().ToList();
        }

        public List<LogicPathNode> getPath() {
            List<LogicPathNode> path = new List<LogicPathNode>();

            LogicPathNode current = this;
            while(current != null ) {
                path.Add(current);
                if(current.Parents.Count > 0) {
                    current = current.Parents[0];
                } else {
                    current = null;
                }
            }

            path.Reverse();

            return path;
        }

        public int getMaxEndLevel(PathPoint target) {
            HashSet<LogicPathNode> ends = getEnds(new HashSet<LogicPathNode>(), target);
            int maxLevel = 0;

            foreach(var item in ends) {
                if(item.Level > maxLevel) {
                    maxLevel = item.Level;
                }
            }

            return maxLevel;
        }
        

        public void drawPathToParent() {

            LogicPathNode parent = null;
            if(Parents.Count > 0) parent = Parents[0];

            LogicPathNode child = this;

            while(parent != null) {
                Debug.drawLine(child.State.CurrentPoint.Parent.Center, parent.State.CurrentPoint.Parent.Center, Color.Red, 10);

                child = parent;
                if(child.Parents.Count > 0) {
                    parent = child.Parents[0];
                } else {
                    parent = null;
                }
            }
        }

        public void drawDebug(int end) {

            LogicPathNode child = Childs[end];
            LogicPathNode preChild = this;

            while(child != null) {
                Debug.drawLine(preChild.State.CurrentPoint.Parent.Center, child.State.CurrentPoint.Parent.Center, Color.Red, 10);

                preChild = child;
                if(child.Childs != null && child.Childs.Count > end) {
                    child = child.Childs[end];
                } else {
                    break;
                }
            }

        }

    }
}
