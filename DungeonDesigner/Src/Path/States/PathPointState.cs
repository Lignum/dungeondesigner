﻿using DungeonDesigner.Src.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Path.States {

    public enum OpenState { Open, Block, Picked}

    public class PathPointState {

        public PathPoint Reference { get; private set; }

        public OpenState OpenState { get; private set; }
        public bool interactable { get; private set; }

        public PathPointState(PathPoint reference) {
            Reference = reference;

            interactable = reference.Parent.InteractionType != InteractionType.None;

            if(Reference.UnlockItem == null) {
                OpenState = OpenState.Open;
            } else {
                OpenState = OpenState.Block;
            }
        }

        public PathPointState(PathPointState original) {
            Reference = original.Reference;

            interactable = original.interactable;

            OpenState = original.OpenState;
        }

        public void interact(PlayerState player) {

            if(OpenState == OpenState.Block) {
                if(unlock(player)) {
                    OpenState = OpenState.Open;
                }
            }

            if(OpenState == OpenState.Open) {
                if(interactable) {
                    switch(Reference.Parent.InteractionType) {
                        case InteractionType.Pick:
                            OpenState = OpenState.Picked;
                            player.addItem(Reference.Parent.Name);
                            break;
                        case InteractionType.SwitchValue:
                            break;
                        case InteractionType.Permanent:
                            break;
                    }
                }
            }

        }

        public bool canBeInteracted(PlayerState player) {
            if(OpenState == OpenState.Open) return true;

            if(OpenState == OpenState.Block) {
                return player.hasItem(Reference.UnlockItem);
            }

            return false;
        }

        bool unlock(PlayerState player) {
            return player.useItem(Reference.UnlockItem);
        }


        public override bool Equals(object obj) {

            if(ReferenceEquals(obj, null)) {
                return false;
            }

            PathPointState p2 = obj as PathPointState;
            //if(p2 == null) return false;
            if(Reference != p2.Reference) return false;
            if(OpenState != p2.OpenState) return false;
            if(interactable != p2.interactable) return false;

            return true;
        }

        public static bool operator ==(PathPointState p1, PathPointState p2) {
            if(ReferenceEquals(p1, null)) {
                return ReferenceEquals(p2, null);
            }

            return p1.Equals(p2);
        }
        public static bool operator !=(PathPointState p1, PathPointState p2) {
            if(ReferenceEquals(p1, null)) {
                return !ReferenceEquals(p2, null);
            }
            return !p1.Equals(p2);
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }

        public override string ToString() {
            return string.Format("{0,-10} | {1,-30} | {2,-10} |", Reference.Parent.Name, Reference.Parent.Center, OpenState);
            //return $"PathPointState[{Reference.Parent.Name}\t\t{Reference.Parent.Center}\t\t, State: {State}]";
        }

    }
}
