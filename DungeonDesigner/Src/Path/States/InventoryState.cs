﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Path.States {
    public class InventoryState {
        
        Dictionary<string, int> _consumableItems;

        public InventoryState() {
            _consumableItems = new Dictionary<string, int>();
        }

        public InventoryState(InventoryState original) {
            _consumableItems = new Dictionary<string, int>(original._consumableItems);
        }

        public void addItem(string itemName) {
            if(_consumableItems.ContainsKey(itemName)) {
                _consumableItems[itemName] += 1;
            } else {
                _consumableItems.Add(itemName, 1);
            }
        }

        public bool hasItem(string itemName) {
            if(_consumableItems.ContainsKey(itemName)) {
                return _consumableItems[itemName] > 0;
            }
            return false;
        }

        public bool useItem(string itemName) {

            if(_consumableItems.TryGetValue(itemName, out int count)){
                if(count > 0) {
                    _consumableItems[itemName] -= 1;
                    return true;
                }
            }

            return false;
        }

        public override string ToString() {
            string ss = "Inventory:\n";

            foreach(var item in _consumableItems) {
                ss += string.Format("\t{0,-10} | {1,-30} |\n", item.Key, item.Value);
            }
            return ss;
        }
    }
}
