﻿using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Path.States {
    public class PlayerState {

        public PathPoint CurrentPoint { get; private set; }
        public List<PathPoint> PosConnections { get; private set; }
        public Dictionary<PathPoint, PathPointState> pointStates;

        InventoryState _inventoryState;

        public PlayerState (ICollection<PathPoint> points, PathPoint current, PathPoint target) {

            CurrentPoint = current;
            pointStates = new Dictionary<PathPoint, PathPointState>();

            foreach(var point in points) {
                pointStates.Add(point, new PathPointState(point));
            }

            _inventoryState = new InventoryState();

            calculateConnections(target);
        }

        PlayerState(PlayerState original, PathPoint current, PathPoint target) : this(original) {

            CurrentPoint = current;

            getPointState(CurrentPoint).interact(this);
            calculateConnections(target);
        }

        private PlayerState(PlayerState original) {
            CurrentPoint = original.CurrentPoint;
            pointStates = new Dictionary<PathPoint, PathPointState>();
            foreach(var states in original.pointStates) {
                pointStates.Add(states.Key, new PathPointState(states.Value));
            }

            _inventoryState = new InventoryState(original._inventoryState);
        }

        public PlayerState getCopy() {
            PlayerState copy = new PlayerState(this);
            copy.PosConnections = new List<PathPoint>(PosConnections);
            return copy;
        }

        public PlayerState getNext(PathPoint target) {
            PathPoint nextPoint = getNextPathPoint();
            if(nextPoint == null) return null;

            PlayerState nextState = new PlayerState(this, nextPoint, target);

            return nextState;
        }

        PathPointState getPointState(PathPoint point) {
            return pointStates[point];
        }

        PathPoint getNextPathPoint() {
            if(PosConnections.Count == 0) return null;
            PathPoint next = PosConnections.ElementAt(0);
            PosConnections.Remove(next);

            return next;
        }

        void calculateConnections(PathPoint targetPoint) {

            if(CurrentPoint == targetPoint) {
                PosConnections = new List<PathPoint>(0);
                return;
            }

            HashSet<PathPoint> posCons = new HashSet<PathPoint>();

            posCons.Add(CurrentPoint);

            List<PathPoint> openCons;
            PathPointState currentState;
            PathPoint current;

            for(int i = 0; i < posCons.Count; i++) {

                current = posCons.ElementAt(i);
                currentState = getPointState(current);
                
                if(currentState.OpenState == OpenState.Block) continue;

                openCons = current.getConnections();

                foreach(var open in openCons) {
                    currentState = getPointState(open);

                    if(currentState.canBeInteracted(this)) {
                        posCons.Add(open);
                    }
                }
            }

            posCons.Remove(CurrentPoint);

            List<PathPoint> removePoints = new List<PathPoint>();

            foreach(var point in posCons) {

                if(point == targetPoint) continue;

                currentState = getPointState(point);

                if(currentState.OpenState == OpenState.Open && !currentState.interactable) {
                    removePoints.Add(point);
                }
                if(currentState.OpenState == OpenState.Picked) {
                    removePoints.Add(point);
                }
            }

            foreach(var item in removePoints) {
                posCons.Remove(item);
            }

            //posConnections = posCons;
            PosConnections = shorConnections(posCons);
        }

        List<PathPoint> shorConnections(HashSet<PathPoint> connections) {
            SortedList<float, PathPoint> distantPoints = new SortedList<float, PathPoint>();
            Vector2 center = CurrentPoint.Parent.Center;

            foreach(var con in connections) {
                distantPoints.Add(Vector2.Distance(center, con.Parent.Center), con);
            }

            List<PathPoint> cons = new List<PathPoint>();
            foreach(var item in distantPoints) {
                cons.Add(item.Value);
            }

            return cons;
        }

        public bool hasItem(string itemName) {
            return _inventoryState.hasItem(itemName);
        }

        public bool useItem(string itemName) {
            return _inventoryState.useItem(itemName);
        }
        public void addItem(string itemName) {
            _inventoryState.addItem(itemName);
        }

        public override bool Equals(object obj) {

            if(ReferenceEquals(obj, null)) {
                return false;
            }

            PlayerState p2 = obj as PlayerState;
            //if(p2 == null) return false;

            //if(CurrentPoint != p2.CurrentPoint) return false;

            if(pointStates.Count != p2.pointStates.Count) return false;

            foreach(var item in pointStates) {

                if(item.Value != p2.pointStates[item.Key]) {
                    return false;
                }

            }

            return true;
        }

        public static bool operator ==(PlayerState p1, PlayerState p2) {
            if(ReferenceEquals(p1, null)) {
                return ReferenceEquals(p2, null);
            }

            return p1.Equals(p2);
        }
        public static bool operator !=(PlayerState p1, PlayerState p2) {
            if(ReferenceEquals(p1, null)) {
                return !ReferenceEquals(p2, null);
            }
            return !p1.Equals(p2);
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }

        public override string ToString() {

            string s = $"Current: {getPointState(CurrentPoint)}\n";
            s += _inventoryState.ToString() +"\n";
            foreach(var item in pointStates) {
                s += item.ToString() + "\n";
            }
            s += "\n-----------CONNECTIONS-----------\n";
            foreach(var item in PosConnections) {
                s += item + "\n";
            }
            
            return s;
        }

    }
}
