﻿using DungeonDesigner.Src.Path.Render;
using DungeonDesigner.Src.Path.States;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DungeonDesigner.Src.Path {
    public static class PathSolver {

        static LogicPath _path;

        static Action<LogicPath> _onCalculationEnd;

        public static void solvePath(PlayerState playerState, PathPoint target) {
            
            GraphicPath.clear();

            _path = new LogicPath(playerState);

            _path.analize(target);

            _onCalculationEnd?.Invoke(_path);
            GraphicPath.reorderNodes(_path);


            if(_path.EndsCount == 0) {
                MessageBox.Show("There are no possible paths");
            }

            Debug.log($"ENDs FOUND: {_path.EndsCount}");

        }

        public static void addOnCalculationEnd(Action<LogicPath> action) {
            _onCalculationEnd += action;

        }
        

    }
}
