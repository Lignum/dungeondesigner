﻿using DungeonDesigner.Engine2D.DebugSrc.RenderDebug;
using DungeonDesigner.Src.Path.States;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Path {
    public class PathPoint {

        public I_PathElement Parent { get; private set; }
        List<PathPoint> _connections = new List<PathPoint>();

        public string UnlockItem { get; set; }

        public PathPoint(I_PathElement parent) {
            Parent = parent;
        }

        public void addConnection(PathPoint point) {
            _connections.Add(point);
            point._connections.Add(this);
        }

        public void clearConnections() {

            foreach(var point in _connections) {
                point._connections.Remove(this);
            }

            _connections.Clear();
        }

        public List<PathPoint> getConnections() {
            return new List<PathPoint>(_connections);
        }


        public void drawDebug() {

            DebugDrawer.Color = Color.Blue;
            DebugDrawer.Alpha = .1f;

            for(int i = 0; i < _connections.Count; i++) {
                DebugDrawer.drawLine(Parent.Center, _connections[i].Parent.Center);
            }

        }
    }
}
