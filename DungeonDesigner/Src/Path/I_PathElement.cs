﻿using DungeonDesigner.Src.Items;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Path {
    public interface I_PathElement {
        Vector2 Center { get; }
        string Name { get; }
        InteractionType InteractionType { get; }
    }
}
