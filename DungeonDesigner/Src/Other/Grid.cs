﻿using DungeonDesigner.Engine2D;
using DungeonDesigner.Engine2D.Logic;
using DungeonDesigner.Engine2D.Pools;
using DungeonDesigner.Engine2D.Renderer;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Other {
    public class Grid: I_Updatable {

        const int GRID_INCREMENT = 25;

        public int Width { get; private set; }
        public int Height { get; private set; }

        SimplePool<GridLine> _linesPool = new SimplePool<GridLine>();
        Vector3 _startPoint;
        
        public readonly RenderSystem RenderSystem;

        public Grid(string rendererName, int width, int height) {

            RenderSystem = RenderMng.getRenderer(rendererName);

            Width = width;
            Height = height;

            ObjectUpdateMng.addObject(rendererName, this);
            //createLines();
        }

        public void update() {

            _linesPool.despawnAll();
            calculateStartPoint();
            createLines();
        }

        public void lateUpdate() { }

        void createLines() {
            
            Vector3[] points = new Vector3[2];

            points[0] = _startPoint;
            points[1] = points[0] + (Vector3.Up * -RenderSystem.MainCamera.ViewRectangle.Height) - Vector3.UnitY * GRID_INCREMENT;
            SimpleLineRenderer line;

            int totalWLines = (int)(RenderSystem.MainCamera.ViewRectangle.Width / GRID_INCREMENT);

            for(int i = 0; i <= totalWLines; i++) {

                points[0] += Vector3.UnitX * GRID_INCREMENT;
                points[1] += Vector3.UnitX * GRID_INCREMENT;

                line = _linesPool.spawn();
                if(line.RenderSystem == null) line.setRenderer(RenderSystem);
                line.setPoints(points);
            }

            points[0] = _startPoint;
            points[1] = points[0] + Vector3.Right * RenderSystem.MainCamera.ViewRectangle.Width + Vector3.Right * GRID_INCREMENT;

            totalWLines = (int)(RenderSystem.MainCamera.ViewRectangle.Height / GRID_INCREMENT);
            for(int i = 0; i <= totalWLines; i++) {

                points[0] -= Vector3.UnitY * GRID_INCREMENT;
                points[1] -= Vector3.UnitY * GRID_INCREMENT;

                line = _linesPool.spawn();
                if(line.RenderSystem == null) line.setRenderer(RenderSystem);
                line.setPoints(points);
            }
        }

        void calculateStartPoint() {

            _startPoint = new Vector3(RenderSystem.MainCamera.ViewRectangle.Position, 0);
            
            _startPoint = Vector3.Floor(_startPoint / GRID_INCREMENT) * GRID_INCREMENT;
            _startPoint += Vector3.UnitY * GRID_INCREMENT;
        }
    }
}
