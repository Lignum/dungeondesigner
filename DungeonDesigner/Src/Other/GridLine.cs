﻿using DungeonDesigner.Engine2D.Pools;
using DungeonDesigner.Engine2D.Renderer;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Other {
    class GridLine : SimpleLineRenderer, I_Poolable {

        static Color GRID_COLOR = new Color(1, 1, 1, .2f);

        public GridLine() : base(null, GRID_COLOR, false) {

        }

        public GridLine(string rendererName) : base(rendererName, GRID_COLOR, false) {

        }

        public void onDeSpawn() {
            Active = false;
        }

        public void onSpawn() {
            Active = true;
        }
    }
}
