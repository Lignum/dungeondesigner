﻿using DungeonDesigner.Engine2D.Renderer;
using DungeonDesigner.Src.Items;
using DungeonDesigner.Src.Managers;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DungeonDesigner.Src.Other {
    public abstract class SelectableObject : SpriteMeshRenderObject {
        public enum PlaceState { Selected, Building, Fixed, Waiting, Blocked }

        PlaceState _state;
        protected Color _defaultColor = Color.White;

        public bool CanBePlaced {
            get {
                return _state != PlaceState.Blocked;
            }
        }

        public SelectableObject(string[] rendererName, string textureNanme, Vector2 center, int layer) : base(rendererName, textureNanme, center, layer) {
            _state = PlaceState.Building;
        }

        public SelectableObject(string rendererName, string textureNanme, Vector2 center, int layer) : this(new string[] { rendererName }, textureNanme, center, layer) {}

        public void onSelected(bool selected) {
            if(selected) {
                setState(PlaceState.Selected);
            } else {
                setState(PlaceState.Fixed);
            }
        }

        public void onMoveState() {
            setState(checkPosition());
        }

        public void onFixPosition() {
            setState(PlaceState.Fixed);
        }

        void setState(PlaceState state) {
            _state = state;
            paintState();
        }

        void paintState() {
            paint(getStateColor());
        }

        Color getStateColor() {
            switch(_state) {
                case PlaceState.Building:
                    return GlobalData.ITEM_CREATING_COLOR;
                case PlaceState.Blocked:
                    return GlobalData.ITEM_BLOCKED_COLOR;
                case PlaceState.Selected:
                    return GlobalData.ITEM_SELECTED_COLOR;
                default:
                    return _defaultColor;
            }
        }

        public void paint(Color paintColor) {
            if(paintColor == Color.Transparent) {
                color = getStateColor();
            } else {
                color = paintColor;
            }
        }

        protected abstract PlaceState checkPosition();

    }
}
